#!/usr/bin/python

# Student Name: Pongsakorn Surasarang
# Student ID  : a1697114
# Course Name : Artificial Intelligence
# Assignment #: 1

# Defining imports
# -------------- #
from __future__ import print_function

import os
import re
import sys
import string

# Testing purpose
debug = False

# Defines the object of how Path Cost for each individual node / point would be
class pathCost:
  def __init__(self, gx, fx, sequence):
    self.gx = gx
    self.fx = fx
    self.sequence = sequence

# Returns an string input line with a single white space
def readFromFile(line):
    pat = re.compile(r'\s+')
    return(pat.sub(' ', line))

# Remove extra white spaces
def translateInput(filename):
    file = open(filename, "r")
    current_lineNumber = 0

    # Defines mazes for current, g(x) Mazes and h(x) Mazes
    maze = []
    hMaze = []
    gMaze = []

    for line in file:
        if(current_lineNumber > 0):
            current_line = readFromFile(line)
            current_line = re.sub("0", "-", current_line)
            current_line = re.sub("1", "#", current_line)

            maze.append(current_line.split())
        else:
            width, height = readFromFile(line).split()
            hMaze = [[str("0") for x in range(int(width))] for y in range(int(height))] 
            gMaze = [[str("0") for x in range(int(width))] for y in range(int(height))] 

        current_lineNumber = current_lineNumber + 1

    return maze, gMaze, hMaze

# For debugging purposes to print the map of it's travel
def printMaze(currentMaze, path=""):
    i = int(sys.argv[2])
    j = int(sys.argv[3])

    pos = set()
    for move in path:
        if move == "L":
            i -= 1
        elif move == "R":
            i += 1
        elif move == "U":
            j -= 1
        elif move == "D":
            j += 1
        pos.add((j, i))
    
    for j, row in enumerate(currentMaze):
        for i, col in enumerate(row):
            if (j, i) in pos:
                print("+ ", end="")
            else:
                print(col + " ", end="")
        print()

# Insert a value to a maze (maze or gMaze or hMaze) current poisition of x and y
def insertValue(currentMaze, position_x, position_y, value):
    # if(currentMaze[position_x][position_y] == "0"):
    currentMaze[position_x][position_y] = value

# Inputs the coordinate of any point, and display the current position it should be at
def displayCoordinate(sequences, i, j):
    for x in range(len(sequences)):
        if sequences[x] == "L":
            i -= 1
        elif sequences[x] == "R":
            i += 1
        elif sequences[x] == "U":
            j -= 1
        elif sequences[x] == "D":
            j += 1

    return i, j

# A function that does the checking Ups and Downs of the current position that it is travelling on the 
# starting x position of the maze where the robot is pre-defined
def checkUpDown(currentMaze, currentSequence, i, j, current_number):
    if(j == len(currentMaze) -1):
        while(1):
            co_x, co_y = displayCoordinate(currentSequence, i, j)

            if(co_y == len(currentMaze) or co_y < 0):
                break

            insertValue(currentMaze, co_y, co_x, str(current_number))
            current_number += + 1
            i, j = displayCoordinate(currentSequence, i, j)
    else:
        while((0 <= j < len(currentMaze)-1)):
            co_x, co_y = displayCoordinate(currentSequence, i, j)
            insertValue(currentMaze, co_y, co_x, str(current_number))
            current_number += + 1
            i, j = displayCoordinate(currentSequence, i, j)

# Own function to fill in the h(x) for any values on a 2D cell / grid
# Summary: Does the current position by U and D then scanning throught L and R to find h(x) cost
def fillSquare(currentMaze, origin_i, origin_j):
    current_number = 0

    # First sequence is Up and Down
    for currentSequence in ["U", "D"]:
        i, j = displayCoordinate(currentSequence, origin_i, origin_j)

        if(j != len(currentMaze)):
            current_number = int(currentMaze[j][i]) + 1
            while(0 < j < len(currentMaze)-1):
                co_x, co_y = displayCoordinate(currentSequence, i, j)
                insertValue(currentMaze, co_y, co_x, str(current_number))
                current_number += + 1
                i, j = displayCoordinate(currentSequence, i, j)

    # Second set of sequence is Left and Right
    # ---------------------------------------- #
    # While doing left and right, it will grab the current position value then it will repeat 
    # when checking for going Up and Down
    for currentOrder in ["L","R"]:
        i, j = displayCoordinate(currentOrder, origin_i, origin_j)
        previousCurrentNumber = 0
        currentPath = ""

        # Does the checking for Left turn
        if(currentOrder == "L"):
            while((i > -1)):
                currentPath += currentOrder
                i, j = displayCoordinate(currentPath, origin_i, origin_j)

                # Indicates that it is out of maze size
                if(i == -1):
                    break

                if(currentMaze[j][i] != str(previousCurrentNumber)):
                    currentMaze[j][i] = str(previousCurrentNumber + 1)
                    previousCurrentNumber += 1

                for currentSequence in ["U", "D"]:
                    i, j = displayCoordinate(currentPath, origin_i, origin_j)
                    current_number = int(currentMaze[j][i]) + 1
                    checkUpDown(currentMaze, currentSequence, i, j, current_number)

        # Does the checking for Right turn
        if(currentOrder == "R"):
            while((i < len(currentMaze[0]))):
                currentPath += currentOrder
                i, j = displayCoordinate(currentPath, origin_i, origin_j)

                # Indicates that it is out of maze size
                if(i == len(currentMaze[0])):
                    break

                if(currentMaze[j][i] != str(previousCurrentNumber)):
                    currentMaze[j][i] = str(previousCurrentNumber + 1)
                    previousCurrentNumber += 1

                for currentSequence in ["U", "D"]:
                    i, j = displayCoordinate(currentPath, origin_i, origin_j)
                    current_number = int(currentMaze[j][i]) + 1
                    checkUpDown(currentMaze, currentSequence, i, j, current_number)

# Gets the current openNode for where this is the list of next other nodes to check
# And then checking to see where to put the next node at (either before or after)
def addToNode(openNode, currentCost, pathCostValue):
    isBreak = False
    
    if(len(openNode) == 0):
        openNode.append(currentCost)
        pathCostValue += 1
    else:
        for items in range(len(openNode)):
            if(openNode[items].fx > currentCost.fx):
                openNode.insert(items, currentCost)
                pathCostValue += 1
                isBreak = True
                break

        if(isBreak == False):
            openNode.append(currentCost)
            pathCostValue += 1
    
    return pathCostValue

# Main function to do A* algorithm to find the shorest path
def findPath(original_maze, g_maze, h_maze):
    # Pre-Defined local variables
    isContinue = True
    pathStart = True
    currentF = 1
    pathCostValue = 1

    # Defines the array of nodes where openNode is the next list of nodes to be checked for 
    # and closeNode is teh path it took when finding the smallest path cost in order
    openNode = []

    while(isContinue):
        # Initial setup, checks for iterations of the robot for where the iteration is equal to 1
        if(pathStart == True):
            for sequenceF in ["L", "R", "U", "D"]:
                co_y, co_x = displayCoordinate(sequenceF, int(sys.argv[2]), int(sys.argv[3]))

                # Inserting possible sequence to travel with g(x), and f(x)
                if (co_y != len(g_maze[0])) and (co_x != len(g_maze)):
                    if(co_y != -1) and (co_x != -1):
                        if(original_maze[co_x][co_y] == "-"):
                            insertValue(g_maze, co_x, co_y, str(currentF))
                            insertValue(original_maze, co_x, co_y, "x")

                            # f(x) = g(x) + h(x)
                            currentCost = pathCost(currentF, currentF + int(h_maze[co_x][co_y]), sequenceF)
                            pathCostValue = addToNode(openNode, currentCost, pathCostValue)
            pathStart = False

        # Checks for iterations of the robot for where the iteration is greater than 1
        else :
            # The robot has not been able to find any valid point
            if(len(openNode) == 0):
                print("{} 0".format(pathCostValue))
                print("X")
                isContinue = False
                continue

            # At the start of every file, it will clear off the first node on list but will store current Node values
            currentNode = openNode[0]
            openNode.pop(0)

            for k in ["L", "R", "U", "D"]:
                sequenceF = currentNode.sequence + k
                co_y, co_x = displayCoordinate(sequenceF, int(sys.argv[2]), int(sys.argv[3]))

                # Inserting possible sequence to travel with g(x), and f(x)
                if (co_y != len(original_maze[0])) and (co_x != len(original_maze)):
                    if(co_y != -1) and (co_x != -1):
                        if(original_maze[co_x][co_y] == "-"):
                            insertValue(original_maze, co_x, co_y, "x")
                            insertValue(g_maze, co_x, co_y, str(currentNode.fx))

                            # End of path. Program has successfully found a valid point
                            if(h_maze[co_x][co_y] == "*"):
                                pValue = len(sequenceF)
                                sequenceOrder = sequenceF
                                sequenceF = re.sub("", " ", sequenceF)
                                print("{} {}\n{}".format(pathCostValue, pValue, sequenceF[1:]))
                                isContinue = False

                                if(debug):
                                    return sequenceOrder

                                continue

                            # f(x) = g(x) + h(x)
                            currentCost = pathCost(currentNode.fx, currentNode.fx + int(h_maze[co_x][co_y]), sequenceF)
                            pathCostValue = addToNode(openNode, currentCost, pathCostValue)

# Main Function that is used to run A* Algorithm Path Finding
def main():
    # Pass in a file to remove extra white spaces
    maze, gMaze, hMaze = translateInput(sys.argv[1])

    # Start point for where the robot should be at and the end point of where the robot should be at
    hMaze[int(sys.argv[5])][int(sys.argv[4])] = str("*")
    gMaze[int(sys.argv[3])][int(sys.argv[2])] = str("*")

    # No valid move, since the start position is at a wall
    if(maze[int(sys.argv[3])][int(sys.argv[2])] == "#"):
        print("0 0")
        print("X")
        return

    # Does the initialization of calculating valid area from the current position.
    for sequence in ["L", "R", "U", "D"]:
        co_y, co_x = displayCoordinate(sequence, int(sys.argv[4]), int(sys.argv[5]))

        if (co_y != len(hMaze[0])) and (co_x != len(hMaze)):
            if(co_y != -1) and (co_x != -1):
                insertValue(hMaze, co_x, co_y, "1")

    fillSquare(hMaze, int(sys.argv[4]), int(sys.argv[5]))
    
    if(debug):
        sequence = findPath(maze, gMaze, hMaze)
    else:
        findPath(maze, gMaze, hMaze)

    # For debugging purposes
    if(debug == True):
        maze[int(sys.argv[5])][int(sys.argv[4])] = str("X")
        maze[int(sys.argv[3])][int(sys.argv[2])] = str("O")
        printMaze(maze, sequence[:-1])
    return 0

# When a program start, it will do a simple check at start and then executes main()
if __name__ == '__main__':
    if(len(sys.argv) < 6):
        print("Input arguments are not met, please ensure you have all the inputs")
        print("i.e. ./a.out inputFile start-x start-y goal-x goal-y")
        sys.exit()

    main()