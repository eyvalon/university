#include<iostream>
using namespace std;

/* General instructions: Please modify this file and submit the modified one
   via websubmission (under the entry "Assignment 7"). No design documents or
   header files are needed. You only need to submit one file, which is this one.

   Please note, you should use recursion in answering both questions.
*/

/* Task 1: Implement the following function for calculating the number of different k-combinations from n.
*/

int count=0;
int num;
int total1;
int total2;
int ccout=1;
int n_original;
int k_original;
int numComb(int n, int k) {

    if(count==0){
        total1=1;
        total2=1;
        n_original=n;
        k_original=k;
        count=1;
        numComb(n ,k );
    }
    else if(count==1){
        
        if (n>0){
            total1*=n;
            numComb(n-1, k);
        }
        
        if (k>0){
            total2*=k;
            numComb(n, k-1);
        }
            return n;
        
    }
}

/* Task 2: Look at the following numTrucks function, which computes 
   the number of trucks with certain loadSize needed to load some 
   numCrates of crates. Along the process, a sequence of 1's and 0's
   is printed.

   Given two positive integers numCrates and loadSize, the function 
   will print out 1 when numCrates < loadSize. Your task is to find 
   out how many 1's are there in the output with a certain pair of 
   numCrates and loadSize
  
   Please implement the function int count1s(int numCrates, int loadSize)
*/


// You are not allowed to modify the following numTrucks function

int numTrucks(int numCrates, int loadSize){

  //base cases
  if(numCrates < loadSize){
    cout << 1;
    return 1;
  }

  if(numCrates == loadSize){
    cout << 0;
    return 1;
  }
  
    int pile1 = numCrates/2;
    int pile2 = numCrates - pile1;

    return numTrucks(pile1, loadSize) + numTrucks(pile2, loadSize);   
  
}


int count1s(int numCrates, int loadSize) {

  
}

// You are not supposed to change the main function
int main() {
	int test;
	cin >> test;
	// Below are the test cases. If you pass x cases, then your mark is x/6.
	// The numbers commentted are the expected output
	switch (test) {
		case 1: cout << numComb(4, 2); break; // 6
		case 2: cout << numComb(20, 5); break; // 15504
	        case 3: cout << numComb(10, 3); break; // 120
      	        case 4: cout << count1s(10, 2); break; // 2
		case 5: cout << count1s(100, 21); break; // 8
		case 6: cout << count1s(150, 4); break; // 44
	}	
}

