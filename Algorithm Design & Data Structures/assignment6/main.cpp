#include <iostream>
#include <string>
#include <sstream>

int binary;
int length;
int count=0;
int maxx=0;
int store=0;
int times;

// Creates a structure for Node that will be used for creating linked list
struct Node {
	int data;
	Node* link;
};

// This functions does the adding of 0's for a given binary string length
Node* add(Node* oldHead, int next) {
	Node* newNode = new Node;
	newNode->data = next;
	newNode->link = oldHead;
	return newNode;
}

// This function does the changing to the necessarily index value of the Node to the correct value for the binary
void compare(Node* head, int binary, int size) {
	Node* current = head;
    int cccount=0;
    int array[size];
    
    // This function just adds to array, the value of the the binary
    for (int k=0; k<size; k++){
        array[k] = binary%10;
        binary /= 10;
    }
    
    // Compare the linked list, to see if the value is more than 0. If it is, change the value to 1 otherwise, do nothing
	while (current != 0) {
        cccount++;
		if (array[size-cccount] >= 0)
            current->data = array[size-cccount];
        current = current->link;
	}
}

// This functions does the calculating for BinaryFlip
void BinaryFlip(Node* head, int inx, int size) {
	Node* current = head;
    
    count=1;
    while (current != NULL) {
        
            if( count == inx )
                if(current->data==0)
                    std::cout << "1" << "";
                else
                    std::cout << "0" << "";
            else
                std::cout << current->data << "";
            
            current = current->link;
            count++;
            
    }      
    std::cout << " ";
    
}

// This functions does the calculating for BinaryFlip
void Rearrange(Node* head, int inx, int size) {
	Node* current = head;

    count=1;
    while (current != NULL) {
        if( count >= inx && count <= size+inx-1){
            if (((current->data) == 1)){
                store++;
            }
            if(current->data == 0){
                if((store>=maxx))
                    maxx=store;
                store=0;
            }
            std::cout << current->data << "";

        }
        
        current = current->link;
        count++;
    }
    
    // If the times is equal to 1, meaning that it's the second time it runs, so doing from start, output max as well
    if(times==1){
        if((store>=maxx))
            maxx=store;
        std::cout << " " << maxx;
    }
}

// Assigns a set of 0, given the size of binary string
Node* list(int size) {
	Node* head = 0;
	for (int i = size; i >= 1; i--) {
		head = add(head, false);
	}
	return head;
}

int main() {
    // Create a linked list with head
    Node* head;
    
    // Declaration of variables
    std::string value1,value2;
    int array_n[2];
	int k1,k2;
    int index;
    
    std::cin >> value1;
	std::cin >> k1;
	std::cin >> value2;
	std::cin >> k2;

    // Stores the first binary as int, which will then be passed to compare the binary with created linked list of 0
    std::istringstream iss ( value1 ); 
    iss >> binary;
    array_n[0] = binary;;
    
    length = value1.length();
    index = k1;
    
    // Helps to determine which values to start and stop
    if(index > length)
        while(index > length)
            index = index - length;
        
    // Creates a linked list of 0's
    // Compare the binary to make sure that it match
    // Finally do BinaryFlip on the position of where the start and stop positions are
    head = list(length);
    compare(head, array_n[0], length);
    BinaryFlip(head, index, length);
    
    // Stores the second binary as int, which will then be passed to compare the binary with created linked list of 0
    std::istringstream isss ( value2 ); 
    isss >> binary;
    array_n[1] = binary;;
    
    length = value2.length();
    index = k2;
    
    // Helps to determine which values to start and stop
    if(index > length)
        while(index > length)
            index = index - length;
        
    // Creates a linked list of 0's
    // Compare the binary to make sure that it match
    // Finally, do rearrange one from start and stop position, and then set times to 1, which will then be used to loop in front again but this time from 0 to the start value that we found earlier
    head = list(length);
    compare(head, array_n[1], length);
    Rearrange(head, index, length);
    times=1;
    Rearrange(head, 0, index);

}
