#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <iterator>

#include "players.h"
#include "human.h"
#include "computer.h"
#include "referee.h"

#include "toolbox.h"
#include "avalanche.h"
#include "bureaucrat.h"
#include "fistfullODollars.h"
#include "crescendo.h"
#include "paperDoll.h"
#include "tournament.h"

using namespace std;

int main() {
    
    // Declaration of class calling for future use.
    referee rplayer;
    toolbox tplayer;
    avalanche aplayer;
    bureaucrat bplayer;
    fistfullODollars fplayer;
    paperDoll pplayer;
    crescendo cplayer;
    tournament tournament_set;
    
    // Declaration of variables
    int inc=0;
    vector<string> user_choice;
    char temp1;
    char temp2;
    int result;

    // Get's the user input
    string value;
    getline(cin, value);
    
    // Define a vector array where this will be used to define what xplayer inputs are
    char aplayer_choice[5];
    char bplayer_choice[5];
    char cplayer_choice[5];
    char pplayer_choice[5];
    char fplayer_choice[5];
    char tplayer_choice[5];
    
    // Make vector array of size 5, such that the max possible play will be 5 in case there is a possibility of tie at the end
    for(int v=0; v<5; v++)
    {
        if(inc==3)
            inc=0;
        
        aplayer_choice[v] = 'R';
        bplayer_choice[v] = 'P';
        tplayer_choice[v] = 'S';
        
        if (inc == 0){
            cplayer_choice[v] = 'P';
            pplayer_choice[v] = 'P';
            fplayer_choice[v] = 'R';
        }else if(inc == 1){
            cplayer_choice[v] = 'S';
            pplayer_choice[v] = 'S';
            fplayer_choice[v] = 'P';
        }else if(inc == 2){        
            cplayer_choice[v] = 'R';
            pplayer_choice[v] = 'S';
            fplayer_choice[v] = 'P';
        }
        inc++;
    }
    
    // Split up the user input into array by whitespace, and then storing it into a vector called tokens
    istringstream iss(value);
    while (iss) {
        string word;
        iss >> word;
        user_choice.push_back(word);
    }
    
    // Sets the array to match Avalanche Bureaucrat Crescendo FistfullODollars PaperDoll and Toolbox so that it can be used for future reference
    string sArray[6] = {"Avalanche", "Bureaucrat", "Crescendo", "FistfullODollars", "PaperDoll", "Toolbox"};
    vector<string> sVector;
    sVector.assign(sArray, sArray+6);
    
    // Will be used for tournament - used for displaying which informations to pass through
    vector<string> competiting_list;
    vector<string> winner_list_1;
    vector<string> winner_list_2;
    vector<string> winner_list_3;
    
    // Compares the input string with the vector array used earlier, to see if it matches with one of the index, assign the index value to competiting_list vector array to be used for tournament
    for (int k=0; k<8; k++)
    {
        if(user_choice[k] == sVector[0]){
            aplayer.setInputs(aplayer_choice);
            competiting_list.push_back("0");
        }
        else if(user_choice[k] == sVector[1]){
            bplayer.setInputs(bplayer_choice);
            competiting_list.push_back("1");
        }
        else if(user_choice[k] == sVector[2]){
            cplayer.setInputs(cplayer_choice);
            competiting_list.push_back("2");
        }
        else if(user_choice[k] == sVector[3]){
            fplayer.setInputs(fplayer_choice);
            competiting_list.push_back("3");
        }
        else if(user_choice[k] == sVector[4]){
            pplayer.setInputs(pplayer_choice);
            competiting_list.push_back("4");
        }
        else if(user_choice[k] == sVector[5]){
            tplayer.setInputs(tplayer_choice);
            competiting_list.push_back("5");
        }
    }
    
    // For each set, determine between 1st and 2nd to see who wins and store that int the vector array winner_list_1
    for (int a=0; a<4; a++)
    {
        for(int d=0; d<5; d++){
        // First, compare to see what the list from input string index, is to the value "0 - 5". Once it is within 0 - 5, assign temp1 or temp2 to that value where this will be used for competing against each other
            if(!competiting_list[a*2].compare("0")){
                aplayer.setInputs(&aplayer_choice[d]);
                temp1 = aplayer.getInputs();
            }else if(!competiting_list[a*2].compare("1")){
                bplayer.setInputs(&bplayer_choice[d]);
                temp1 = bplayer.getInputs();
            }else if(!competiting_list[a*2].compare("2")){
                cplayer.setInputs(&cplayer_choice[d]);
                temp1 = cplayer.getInputs();
            }else if(!competiting_list[a*2].compare("3")){
                fplayer.setInputs(&fplayer_choice[d]);
                temp1 = fplayer.getInputs();
            }else if(!competiting_list[a*2].compare("4")){
                pplayer.setInputs(&pplayer_choice[d]);
                temp1 = pplayer.getInputs();
            }else if(!competiting_list[a*2].compare("5")){
                tplayer.setInputs(&tplayer_choice[d]);
                temp1 = tplayer.getInputs();
            };
            
            if(!competiting_list[a*2+1].compare("0")){
                aplayer.setInputs(&aplayer_choice[d]);
                temp2 = aplayer.getInputs();
            }else if(!competiting_list[a*2+1].compare("1")){
                bplayer.setInputs(&bplayer_choice[d]);
                temp2 = bplayer.getInputs();
            }else if(!competiting_list[a*2+1].compare("2")){
                cplayer.setInputs(&cplayer_choice[d]);
                temp2 = cplayer.getInputs();
            }else if(!competiting_list[a*2+1].compare("3")){
                fplayer.setInputs(&fplayer_choice[d]);
                temp2 = fplayer.getInputs();
            }else if(!competiting_list[a*2+1].compare("4")){
                pplayer.setInputs(&pplayer_choice[d]);
                temp2 = pplayer.getInputs();
            }else if(!competiting_list[a*2+1].compare("5")){
                tplayer.setInputs(&tplayer_choice[d]);
                temp2 = tplayer.getInputs();
            };
            
            rplayer.get_choice1(&temp1);
            rplayer.get_choice2(&temp2);
            rplayer.compare_choice();
            
            if(rplayer.get_total() == 3 || ((rplayer.get_total() > 0) && d==4)){
                winner_list_1.push_back(competiting_list[a*2]);
                rplayer.reset();
                break;
            }
            
            if(rplayer.get_total() == 0){
                winner_list_1.push_back(competiting_list[a*2]);
                rplayer.reset();
                break;
            }
            
            if(rplayer.get_total() == -3 || ((rplayer.get_total() < 0) && d==4)){
                winner_list_1.push_back(competiting_list[a*2+1]);
                rplayer.reset();
                break;
            }    
        }
    }
    
    // For each set, determine between 1st and 2nd to see who wins and store that int the vector array winner_list_2
    for (int a=0; a<2; a++)
    {
        for(int d=0; d<5; d++){
        // First, compare to see what the list from input string index, is to the value "0 - 5". Once it is within 0 - 5, assign temp1 or temp2 to that value where this will be used for competing against each other
            if(!winner_list_1[a*2].compare("0")){
                aplayer.setInputs(&aplayer_choice[d]);
                temp1 = aplayer.getInputs();
            }else if(!winner_list_1[a*2].compare("1")){
                bplayer.setInputs(&bplayer_choice[d]);
                temp1 = bplayer.getInputs();
            }else if(!winner_list_1[a*2].compare("2")){
                cplayer.setInputs(&cplayer_choice[d]);
                temp1 = cplayer.getInputs();
            }else if(!winner_list_1[a*2].compare("3")){
                fplayer.setInputs(&fplayer_choice[d]);
                temp1 = fplayer.getInputs();
            }else if(!winner_list_1[a*2].compare("4")){
                pplayer.setInputs(&pplayer_choice[d]);
                temp1 = pplayer.getInputs();
            }else if(!winner_list_1[a*2].compare("5")){
                tplayer.setInputs(&tplayer_choice[d]);
                temp1 = tplayer.getInputs();
            };
            
            if(!winner_list_1[a*2+1].compare("0")){
                aplayer.setInputs(&aplayer_choice[d]);
                temp2 = aplayer.getInputs();
            }else if(!winner_list_1[a*2+1].compare("1")){
                bplayer.setInputs(&bplayer_choice[d]);
                temp2 = bplayer.getInputs();
            }else if(!winner_list_1[a*2+1].compare("2")){
                cplayer.setInputs(&cplayer_choice[d]);
                temp2 = cplayer.getInputs();
            }else if(!winner_list_1[a*2+1].compare("3")){
                fplayer.setInputs(&fplayer_choice[d]);
                temp2 = fplayer.getInputs();
            }else if(!winner_list_1[a*2+1].compare("4")){
                pplayer.setInputs(&pplayer_choice[d]);
                temp2 = pplayer.getInputs();
            }else if(!winner_list_1[a*2+1].compare("5")){
                tplayer.setInputs(&tplayer_choice[d]);
                temp2 = tplayer.getInputs();
            };
            
            rplayer.get_choice1(&temp1);
            rplayer.get_choice2(&temp2);
            rplayer.compare_choice();
            
            if(rplayer.get_total() == 3 || ((rplayer.get_total() > 0) && d==4)){
                winner_list_2.push_back(winner_list_1[a*2]);
                rplayer.reset();
                break;
            }
            
            if(rplayer.get_total() == 0){
                winner_list_2.push_back(winner_list_1[a*2]);
                rplayer.reset();
                break;
            }
            
            if(rplayer.get_total() == -3 || ((rplayer.get_total() < 0) && d==4)){
                winner_list_2.push_back(winner_list_1[a*2+1]);
                rplayer.reset();
                break;
            }    
        }
    }
    
    // For each set, determine between 1st and 2nd to see who wins and store that int the vector array winner_list_3
    for (int a=0; a<1; a++)
    {
        for(int d=0; d<5; d++){
        // First, compare to see what the list from input string index, is to the value "0 - 5". Once it is within 0 - 5, assign temp1 or temp2 to that value where this will be used for competing against each other
            if(!winner_list_2[a*2].compare("0")){
                aplayer.setInputs(&aplayer_choice[d]);
                temp1 = aplayer.getInputs();
            }else if(!winner_list_2[a*2].compare("1")){
                bplayer.setInputs(&bplayer_choice[d]);
                temp1 = bplayer.getInputs();
            }else if(!winner_list_2[a*2].compare("2")){
                cplayer.setInputs(&cplayer_choice[d]);
                temp1 = cplayer.getInputs();
            }else if(!winner_list_2[a*2].compare("3")){
                fplayer.setInputs(&fplayer_choice[d]);
                temp1 = fplayer.getInputs();
            }else if(!winner_list_2[a*2].compare("4")){
                pplayer.setInputs(&pplayer_choice[d]);
                temp1 = pplayer.getInputs();
            }else if(!winner_list_2[a*2].compare("5")){
                tplayer.setInputs(&tplayer_choice[d]);
                temp1 = tplayer.getInputs();
            };
            
            if(!winner_list_2[a*2+1].compare("0")){
                aplayer.setInputs(&aplayer_choice[d]);
                temp2 = aplayer.getInputs();
            }else if(!winner_list_2[a*2+1].compare("1")){
                bplayer.setInputs(&bplayer_choice[d]);
                temp2 = bplayer.getInputs();
            }else if(!winner_list_2[a*2+1].compare("2")){
                cplayer.setInputs(&cplayer_choice[d]);
                temp2 = cplayer.getInputs();
            }else if(!winner_list_2[a*2+1].compare("3")){
                fplayer.setInputs(&fplayer_choice[d]);
                temp2 = fplayer.getInputs();
            }else if(!winner_list_2[a*2+1].compare("4")){
                pplayer.setInputs(&pplayer_choice[d]);
                temp2 = pplayer.getInputs();
            }else if(!winner_list_2[a*2+1].compare("5")){
                tplayer.setInputs(&tplayer_choice[d]);
                temp2 = tplayer.getInputs();
            };
                        
            rplayer.get_choice1(&temp1);
            rplayer.get_choice2(&temp2);
            rplayer.compare_choice();
            
            if(rplayer.get_total() == 3 || ((rplayer.get_total() > 0) && d==4)){
                winner_list_3.push_back(winner_list_2[a*2]);
                break;
            }
            
            if(rplayer.get_total() == 0){
                winner_list_3.push_back(winner_list_2[a*2]);
                break;
            }
            
            if(rplayer.get_total() == -3 || ((rplayer.get_total() < 0) && d==4)){
                winner_list_3.push_back(winner_list_2[a*2+1]);
                break;
            }    
        }
    };
    
    // Converts the final output (string) to an int so that it can output in the array sArray index name
    stringstream(winner_list_3[0]) >> result;
    
    // Returns what the result would be after comparing all the possible matches
    cout << sArray[result] << endl;
    
    return 0;
}


