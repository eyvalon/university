#ifndef PLAYERDOLL_H
#define PLAYERDOLL_H

#include "players.h"

using namespace std;

// Declaration of base class that is used to let computer class and human class to get the methods
class paperDoll : public players
{   
private:
    
public:
    paperDoll();
};

#endif
