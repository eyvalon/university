#ifndef TOOLBOX_H
#define TOOLBOX_H

#include "players.h"

using namespace std;

// Declaration of base class that is used to let computer class and human class to get the methods
class toolbox : public players
{
private:
    
public:
    toolbox();
    
};

#endif
