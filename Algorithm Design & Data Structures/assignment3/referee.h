#ifndef REFEREE_H
#define REFEREE_H

#include "players.h"

using namespace std;

// Declaration of base class that is used to let computer class and human class to get the methods
class referee : public players
{   
private:
    // Declaration of variables that only this class can access
    char player1;
    char player2;
    char results;
    int total;
    
public:
    // Declaration of methods that will be used to compare human and computer Rock Paper Sister
    referee();
    
    void compare_choice();
    void get_choice1(char*);
    void get_choice2(char*);
    char getResult();
    int get_total();
    int reset();

};

#endif
