#include <iostream>
#include <sstream>
#include <string>

#include "referee.h"

using namespace std;

// Methods for Referee
referee::referee(){
    
};

void referee::compare_choice() 
{
    // List out all the probability of what the result would be by comparing with the computer input, Rock 'R'
    if((player1 == 'P') & (player2 == 'P'))
        results = 'T';
    else if((player1 == 'R') & (player2 == 'P')){
        results = 'L';
        total--;
    }else if((player1 == 'S') & (player2 == 'P')){
        results = 'W';
        total++;
    }
    
    if((player1 == 'P') & (player2 == 'R')){
        results = 'W';
        total++;
    }else if((player1 == 'R') & (player2 == 'R'))
        results = 'T';
    else if((player1 == 'S') & (player2 == 'R')){
        results = 'L';
        total--;
    }
    
    if((player1 == 'P') & (player2 == 'S')){
        results = 'L';
        total--;
    }else if((player1 == 'R') & (player2 == 'S')){
        results = 'W';
        total++;
    }else if((player1 == 'S') & (player2 == 'S'))
        results = 'T';
    
};

// Gets the input for human from iterating in the for loop in the main.cpp
void referee::get_choice1(char* choices)
{
    player1 = *choices;
};

// Gets the input for computer from iterating in the for loop in the main.cpp
void referee::get_choice2(char* choices)
{
    player2 = *choices;
};

// Returns the results, used for printing out in main.cpp
char referee::getResult()
{
    return results;
};

int referee::get_total()
{
    return total;
};

int referee::reset()
{
    total=0;
};
