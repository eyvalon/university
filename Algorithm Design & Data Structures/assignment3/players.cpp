#include <iostream>
#include <sstream>
#include <string>

#include "players.h"

using namespace std;

// Methods for players
players::players()
{
    
}

// From the input, get all the inputs entered and store in the variable picked as pointers
void players::setInputs(char* chosen_letter)
{
    picked = *chosen_letter;
}

// Get what the input is and returning so that it can be used to pass to other class
char players::getInputs()
{
    return picked;
}
