#ifndef BUREAUCRAT_H
#define BUREAUCRAT_H

#include "players.h"

using namespace std;

// Declaration of base class that is used to let computer class and human class to get the methods
class bureaucrat : public players
{
private:
    
public:
    bureaucrat();
};

#endif
