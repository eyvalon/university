#ifndef PLAYERS_H
#define PLAYERS_H

#include <string>

using namespace std;

// Declaration of base class that is used to let computer class and human class to get the methods
class players
{   
    private:
        // Declaration of variable, that is used to see what the input for computer and humans are later
        char picked;

    public:
        // Declaration of methods
        players();
        void setInputs(char*);
        char getInputs(); 
};

#endif
