#ifndef FISTFULLODOLLARS_H
#define FISTFULLODOLLARS_H

#include "players.h"

using namespace std;

// Declaration of base class that is used to let computer class and human class to get the methods
class fistfullODollars : public players
{   
private:
    
public:
    fistfullODollars();
};

#endif
