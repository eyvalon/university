#include<iostream>
using namespace std;

/* General instructions: Please modify this file and submit the modified one
   via svn and then websubmission (under the entry "pracExam"). No design
   documents or header files are needed. You only need to submit one file, 
   which is this one.

   This exam is marked out of 10 marks and is worth 20% of your overall marks.
   You need at least 4/10 to pass the hurdle requirement.
*/

struct Node {
    int data;
    Node* next;
};

/* Task 1: Implement the following function for adding a new node to the 
   front of the given list.

   input   Nodes* head: a head pointer to a list
           int val: an integer that represent the value of the new
                    node
   return  the head pointer of the new list after the insertion

   Example: add a node of value 9
            before  HEAD->1->2->3->NULL
            after   HEAD->9->1->2->3->NULL

   3 marks(1 for style, 2 for functionality)

*/

Node* add(Node* head, int val){

  Node* newNode = new Node;
  newNode->data = val;
  newNode->next = head;
  return newNode;

}

/* Task 2: Implement the following function for printing the elements
   in odd position of the given list. Assume the head node is defined 
   to be in position 1.

   input   Nodes* head: a head pointer to a list

   return  void
          
   output  The elements in odd position of the given list

   Example: input    HEAD->1->2->3->9->12->NULL
            output   HEAD->1->NA->3->NA->12->NULL

   3 marks(1 for style, 2 for functionality)
*/

void printOdd(Node* head){

  Node* current = head;
  cout << "HEAD->";
  while (current != NULL) {
    cout << current->data << "->";
    current = current->next;
  }
  cout << "NULL" << endl;

}

/* Task 3: Implement the following function for deleting the nodes in
   between Node n1 and Node n2.

   input   Nodes* n1: a pointer to a node in a list
           Nodes* n2: a pointer to a node in the same list

   return  void

   Example: input   n1 points to the 1st node 
                    n2 points to the 4th node

            before  HEAD->1->2->3->9->12->NULL

            after   HEAD->1->9->12->NULL

   4 marks(1 for style, 3 for functionality)
*/

// Function to delete certain node in the list.
void deleteNode(Node* current_node){

   Node* temp = current_node->next;
   current_node->data = temp->data;
   current_node->next = temp->next;
   delete temp;

}

void clear(Node* n1, Node* n2){

  // Declaration of variables.
  Node* current;
  Node* tempp;
  // Assumes that p2 is always higher than p1.
  current = n2->next;
  
  while(current!=NULL){

    if(current==n1)
      break;

    deleteNode(current);
  }

}

// You are not supposed to change the main function
int main() {

    Node* head = NULL;
    Node *p4, *p7;
    int test;
    cin >> test;

    for(int i = 1; i < 10; i++) {
        head = add(head, i);
        if (i==4) p4 = head;
        if (i==7) p7 = head;
    }
    // at this point, we have created the following list: HEAD->9->8->7->6->5->4->3->2->1->NULL
    // p4 now points to node 4 (the node containing 4); p7 now points to node 7 (the node containing 7)

    clear(p4, p7);
    // between p4 and p7, there are 2 nodes. Delete both of them.
    // The resulting list is HEAD->9->8->7->4->3->2->1->NULL

    //You can uncomment this line to test.
    //the output should be: HEAD->9->NA->7->NA->3->NA->1->NULL
    //Please remember to comment this line out before submitting
    //printOdd(head);
    

    head = add(head, 12);
    head = add(head, 15);
    // at this point, the list is: HEAD->15->12->9->8->7->4->3->2->1->NULL

    clear(head, p7);
    // between p7 and head, there are 3 nodes (12, 9 and 8). Remove them all.
    // The resulting list is HEAD->15->7->4->3->2->1->NULL
    printOdd(head);
    // the output should be: HEAD->15->NA->4->NA->2->NA->NULL


    // free all nodes
    Node* tmp;
    while (head) {
        tmp = head;
        head = head->next;
        delete tmp;
    }
    return 0;
  
}
