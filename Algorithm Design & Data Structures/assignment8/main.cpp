#include <cstring>
#include <algorithm>
#include <string>
#include <sstream>
#include <iostream>

#include "Sort.h"
#include "BubbleSort.h"
#include "QuickSort.h"
#include "RecursiveBinarySearch.h"

int main() {
	BubbleSort* bubble_sort = new BubbleSort;
	QuickSort* quick_sort = new QuickSort;
	RecursiveBinarySearch* binary_search = new RecursiveBinarySearch;
    
    std::vector<int> input_array;
    int current_digit;
    std::string data;
    std::getline(std::cin, data);
    std::istringstream iss(data);
    
    // Get each value separated by spaces.
    // Converts each values separated by spaces, from string to int.
    // Store the converted int to the vector called input_array.
    while (!iss.eof()) {
        std::string input;
        iss >> input;
        
        // Break out of the loop if the last input is a space.
        if(input.empty())
            break;
        
        std::istringstream ss(input);
        ss >> current_digit;
        
        input_array.push_back(current_digit);
    }

    // Sort the array using bubble_sort.
    // Check for true / false with binary_search.
    std::vector<int> bubble_sortVector = bubble_sort->sortVector(input_array);
    int tail = bubble_sortVector.size();
    int head = 0;
    bool check = binary_search->searchVector(bubble_sortVector, 1, head, tail);
    if(check == 1) {
        std::cout << "true ";
    } else {
        std::cout << "false ";
    } 

    // Sort the array using quick_sort.
    std::vector<int> quick_sortVector = quick_sort->sortVector(input_array);
    for(unsigned int i = 0; i < quick_sortVector.size(); i++) {
        if(i == quick_sortVector.size()-1) {
            std::cout << quick_sortVector[i] << std::endl;
        } else {
            std::cout << quick_sortVector[i] << " ";
        }
    }

}
