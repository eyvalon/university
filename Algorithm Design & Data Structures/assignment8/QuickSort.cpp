#include "QuickSort.h"

QuickSort::QuickSort(){

}

// Recursively calls for the function when the functions are not yet fully Sorted
std::vector<int> QuickSort::recursionStep(std::vector<int>& vector, int p, int q){
	// Declaration of variables
    int i = p, j = q;
    int temp;
    int pivot = vector[(p + q) / 2];
 
    // Partition 
    while (i <= j) {
        while (vector[i] < pivot) {
            i++;
        }
        while (vector[j] > pivot) {
            j--;
        }
        if (i <= j) {
            temp = vector[i];
            vector[i] = vector[j];
            vector[j] = temp;
            i++;
            j--;
        }
    };
 
//  Recursion 
    if (p < j){
        recursionStep(vector, p, j);
    }
    if (i < q){
        recursionStep(vector, i, q);
    }
    return vector;
}

// Pass the given vector, to the recursionStep by telling which values are needed in the function -> recursionStep(vector, 0, size-1)
std::vector<int> QuickSort::sortVector(std::vector<int> vec){
	int p = 0;
	unsigned int q = vec.size()-1;

	return recursionStep(vec, p, q);
}

QuickSort::~QuickSort(){

}
