#include "RecursiveBinarySearch.h"

RecursiveBinarySearch::RecursiveBinarySearch(){

}

// Given a vector, recursively check for the value "1" by recursively calling the function again until it either finds a value "1" or not.
// If it does, return true, otherwise return false.
bool RecursiveBinarySearch::searchVector(std::vector<int>& vector, int n, int head, int tail){
    int middle = (head + tail) / 2;
    if(head >= tail) {
        return false;
    } else {
        if(n == vector[middle]) {
            return true;
        } else if(n < vector[middle]) {
            return searchVector(vector, n, head, middle);
        } else if(n > vector[middle]) {
            return searchVector(vector, n, (middle+1), tail);
        }
    }
    return false;
}

RecursiveBinarySearch::~RecursiveBinarySearch(){

}
