#ifndef BINSEARCH_H
#define BINSEARCH_H

#include <vector>

class RecursiveBinarySearch {
public:
    RecursiveBinarySearch();
    bool searchVector(std::vector<int>& vector, int n, int head, int tail);
    ~RecursiveBinarySearch();
};

#endif //BINSEARCH_H
