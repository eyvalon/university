#ifndef QUICKSORT_H
#define QUICKSORT_H

#include "Sort.h"

class QuickSort : public Sort {
public:
    QuickSort();
    std::vector<int> recursionStep(std::vector<int>& vector, int x, int y);
    std::vector<int> sortVector(std::vector<int> vector);
    ~QuickSort();
};

#endif //QUICKSORT_H
