#include "BubbleSort.h"

BubbleSort::BubbleSort(){

}

// Given a vector, swap the 2 index for which are needed to be swapped. Once swapped, continue until there are no more swapping needed to be done.
std::vector<int> BubbleSort::sortVector(std::vector<int> vector){
    std::vector<int> new_vector = vector;
    bool check = true;
    while(check) {
        check = false;
        for (unsigned int i = 0; i < new_vector.size()-1; i++) {
            if (new_vector[i]>new_vector[i+1] ){
                new_vector[i] += new_vector[i+1];
                new_vector[i+1] = new_vector[i] - new_vector[i+1];
                new_vector[i] -=new_vector[i+1];
                check = true;
            }
        }
    }
    return new_vector;
}

BubbleSort::~BubbleSort(){

}
