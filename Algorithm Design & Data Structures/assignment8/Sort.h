#ifndef SORT_H
#define SORT_H

#include <vector>

class Sort {
private:
    virtual std::vector<int> sortVector(std::vector<int> vector) = 0;

public:
    Sort();
    ~Sort();
};

#endif //SORT_H
