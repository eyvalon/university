#include <string>
#include <iostream>

using namespace std;

#include "palindrome.h"

int main()
{
    string data;
    Palindrome *pao;
    cout << "";
    getline (cin,data);
    
    // Parse the input value to the Palindrome class function to calculates for Palindrome
    pao = new Palindrome(data);         
    
    // Change the input string to lowercase
    pao->lowerCase();
    
    // Remove all non-alphabetical letters
    pao->removeNonLetters();        
    
    // Check to see what the result of Palindrome is
    pao->isPalindrome();            
    
    // Prints out whether Palindrome is yes or no.
    cout << pao->answer << endl;    
    
    // Deletes the pointer when finished with the program
    delete pao;                     
}
