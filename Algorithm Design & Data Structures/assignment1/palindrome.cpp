#include <algorithm>
#include <string>
#include <iostream>
#include <math.h> 

#include "palindrome.h"

using namespace std;

// Set the default answer to Yes
string Palindrome::answer = "Yes"; 

// Parse the input string to a variable called input where other functions can access it. the variable input is in public attribute so it will be a global
Palindrome::Palindrome(string u)    
{
    input = u;
}

// Changes the input string to lower case
void Palindrome::lowerCase()
{
    transform(input.begin(), input.end(), input.begin(),(int (*)(int))tolower);    
}

// Checks that all special characters and white spaces have been removed successfully
void Palindrome::removeNonLetters()
{
    while (input.find_first_not_of("abcdefghijklmnopqrstuvwxyz") != std::string::npos)    
    {
        for(unsigned int i=0; i<input.length(); i++)
        {
            if(!isalpha(input[i])) input.erase(input.begin() + i);
        }
    }
}

void Palindrome::isPalindrome()
{ 
    // Gets the length of string after removing all the special characters and numbers and store in the variable length_of_string
    length_of_string = input.length(); 
    
    // Find the mid-point and floor the value (if it is odd) otherwise the midpoint value so that later on, it can compare the starting with the ending values in the floor loop
    if (length_of_string%2 == 1)
    {
        half = floor((length_of_string /2));
    }
    else
        half = (length_of_string/2);
    
    for (int k=0; k<half; k++)
    {
        // If the iterate checks that the letter that it is comparing to doesn't match with one another at any point, it will tell the program that the answer is no and therefore break the program instantly
        if(input[k] != input[length_of_string-1-k])
        {
            answer = "No";
            break;
        } 
    }
}
