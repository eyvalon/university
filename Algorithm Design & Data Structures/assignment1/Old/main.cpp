#include <string>
#include <iostream>

using namespace std;

#include "palindrome.h"

int main()
{
    string data;
    Palindrome *pao;
    cout << "";
    getline (cin,data);
    
    pao = new Palindrome(data);     //Parse the input value to the Palindrome class function to calculates for Palindrome
    cout << pao->answer << endl;    //Print's out whether it is a Palindrome or not
}
