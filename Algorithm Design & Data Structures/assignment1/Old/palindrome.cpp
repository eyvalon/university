#include <algorithm>
#include <string>
#include <iostream>
#include <math.h> 

#include "palindrome.h"

using namespace std;

string Palindrome::answer = "Yes"; //set's the default answer to Yes

//Checks whether it is a Palindrome or not
Palindrome::Palindrome(string u)    
{
    // Changes the string to lower case
    transform(u.begin(), u.end(), u.begin(),(int (*)(int))tolower);    
    
    // Checks that all special characters and white spaces have been removed successfully
    while (u.find_first_not_of("abcdefghijklmnopqrstuvwxyz") != std::string::npos)    
    {
        for(unsigned int i=0; i<u.length(); i++)
        {
            if(!isalpha(u[i])) u.erase(u.begin() + i);
        }
    }
    
    // Gets the length of string after removing all the special characters and numbers and store in the variable length_of_string
    length_of_string = u.length(); 
    
    // Find the mid-point and floor the value (if it is odd) otherwise the midpoint value so that later on, it can compare the starting with the ending values in the floor loop
    if (length_of_string%2 == 1)
    {
        half = floor((length_of_string /2));
    }
    else
        half = (length_of_string/2);
    
    for (int k=0; k<half; k++)
    {
        // If the iterate checks that the letter that it is comparing to doesn't match with one another at any point, it will tell the program that the answer is no and therefore break the program instantly
        if(u[k] != u[length_of_string-1-k])
        {
            answer = "No";
            break;
        } 
    }
}
