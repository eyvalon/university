#include <string>
#include <iostream>

#ifndef ASSIGNMENT_1_H
#define ASSIGNMENT_1_H

using namespace std;

class Palindrome
{
public:  
    // This is where the input data will be used to set string input
    Palindrome(string z);
    
    string input;
    
    // Creates a function where the input data will be used to check against it 
    void removeNonLetters();
    void lowerCase();
    void isPalindrome();

    // Calculates what the length of the string after removing all the non-alphabetical letters is as well as the half of that length 
    int half;
    int length_of_string;
        
    // If the Palindrome is a no, the answer will be printed as no
    static string answer;

};

#endif //ASSIGNMENT_1_H
