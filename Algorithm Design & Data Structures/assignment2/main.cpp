#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "players.h"
#include "human.h"
#include "computer.h"
#include "referee.h"

using namespace std;
int main() {
    
    // Declaration of class calling for future use.
    human hplayer;
    computer cplayer;
    referee rplayer;
    
    // Declaration of variables
    char choice;
    int inc = 0;
    int value;
    char cplayer_choice = 'R';
    char temp1;
    char temp2;
    
    // Asks the user for input
    cin >> value;
    
    // Declare an array of fixed size with the first input 
    char user_choice[value];

    // Passes the R P S to the array for future uses
    while (cin.get(choice) ){
        if (choice == 'R' || choice == 'P' || choice == 'S') {
            user_choice[inc] = choice;
            inc ++;
        }
    }
    
    // Sets what the user input is for the human, and assigning computer to have "R"
    hplayer.setInputs(user_choice);
    cplayer.setInputs(&cplayer_choice);
    
    // Iterate one at a time and then comparing to see what the result would be
    for (int i = 0; i < value; i++) {
    
        hplayer.setInputs(&user_choice[i]);
        temp1 = hplayer.getInputs() ;
        temp2 = cplayer.getInputs();
        
        rplayer.getComputer_choice(&temp2);
        rplayer.getHuman_choice(&temp1);
        rplayer.compare_choice();
        
        // Once you get the result from the previous calls, print it out
        cout << rplayer.getResult() << " ";
      
    };
    
    return 0;
}
