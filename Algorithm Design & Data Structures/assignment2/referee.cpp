#include <iostream>
#include <sstream>
#include <string>

#include "referee.h"

using namespace std;

// Methods for Referee
referee::referee(){
    
};

void referee::compare_choice() 
{
    // List out all the probability of what the result would be by comparing with the computer input, Rock 'R'
    if((human == 'P') & (computer == 'R'))
        results = 'W';
    else if((human == 'R') & (computer == 'R'))
        results = 'T';
    else if((human == 'S') & (computer == 'R'))
        results = 'L';
};

// Gets the input for human from iterating in the for loop in the main.cpp
void referee::getHuman_choice(char* choices)
{
    human = *choices;
};

// Gets the input for computer from iterating in the for loop in the main.cpp
void referee::getComputer_choice(char* choices)
{
    computer = *choices;
};

// Returns the results, used for printing out in main.cpp
char referee::getResult()
{
    return results;
};
