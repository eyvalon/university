#ifndef REFEREE_H
#define REFEREE_H

#include "players.h"

using namespace std;

class referee : public players
{   
private:
    // Declaration of variables that only this class can access
    char computer;
    char human;
    char results;
    
public:
    // Declaration of methods that will be used to compare human and computer Rock Paper Sister
    referee();
    
    void compare_choice();
    void getComputer_choice(char*);
    void getHuman_choice(char*);
    char getResult();
};

#endif
