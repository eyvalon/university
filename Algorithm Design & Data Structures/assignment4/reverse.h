#ifndef REVERSE_H
#define REVERSE_H

class reverse
{
private:
    // Declaring variables
    std::stringstream ss;
    
    int length;
    int result;
    int count;
    int size;
    
    std::string temp;
    
    std::vector<int> data;

public:
    
    // Functions used for reversing
    int reverseDigit(int value);
    std::string reverseString(std::string letters);
    std::string getString(std::string name);
    std::string remove_zero(std::string y, int x);
};

#endif
 
