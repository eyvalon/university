#ifndef FIBONAACI_H
#define FIBONAACI_H

class fibonacci
{
private:
    // Declaring variables
    std::stringstream ss;
    
    int count;
    int size;
    int osize;
    int original;
    
    std::vector<int> data;

public:
    
    // Functions used for finding fibonacci
    int getValue(int value, std::string x);
    int returnValue();
    int getRealSize(int x);
    int fib(int z);
};

#endif
 
