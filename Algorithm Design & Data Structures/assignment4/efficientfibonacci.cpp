#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "efficientfibonacci.h"

int efficientfibonacci::getValue(int value, std::string x)
{
    // Declaring variables
    ocount=0;
    count = 0;
    first=1;
    second=1;
    original = value;
    
    // Returns -1, so that it can be changed to ERROR output
    if(value<0 && size==1)
        return size=-1;
    
    if(x.find('0') != std::string::npos)
        return 0;
    
    // Finds the number of digits for a given number
    while (value > 0)
    {
        value/=10;
        count++;
    }
    

    // Returns -1, so that it can be changed to ERROR output
    if(count != osize)
        return size=-1;
    
    size = count;
    return 1;
};

// Returns what the size of the 4th input number is
int efficientfibonacci::returnValue()
{
    return size;
};

// Gets the int value
int efficientfibonacci::getRealSize(int x)
{
    osize = x;
    return 1;
};

// Functions for finding the efficient fibonacci
int efficientfibonacci::fib(int n)
{
    if(n == 2 && ocount>0)
        return second;
    if(n < 2 && ocount==0)
        return n;

    if(n > 2)
    {
        current = first + second;
        first = second;
        second = current;
        ocount++;
        fib(n-1);
    }
};
