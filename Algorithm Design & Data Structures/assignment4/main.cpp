#include <iostream>
#include <string>
#include <vector>
#include <sstream>

#include "reverse.h"
#include "fibonacci.h"
#include "efficientfibonacci.h"

int main()
{
    reverse rman;
    fibonacci fibo;
    efficientfibonacci fibo1;
    
    // Declaration of variables
    std::stringstream ss;
    std::stringstream ss1;
    std::stringstream ss2;

    int digit;
    int length;
    int digit_value;
    int digit_size;
    int temp_value;
    
    std::string data;
    std::string new_one;
    std::string new1;
    std::string temp;
    std::string error="ERROR";
    
    // Declaration of vectors
    std::vector<std::string> new_data_array;
    std::vector<std::string> new_entry;
    std::vector<std::string> result;

    // Asks the user for 4 inputs
    getline(std::cin, data);
    std::istringstream iss(data);
    
    // From user input, store the inputs into 4 different vector array called new_data_array
    while (iss) 
    {
        std::string input;
        iss >> input;
        new_data_array.push_back(input);
    }
    
    // Checks to see if the integer position has any error or not
    // While checking, push the new value to the vector new_one
    length = new_data_array[0].length();
    new_one = rman.remove_zero(new_data_array[0], length);

    new_entry.push_back(new_one);
 
    ss << new_entry[0];
    ss >> digit;
    
    rman.getString(new_entry[0]);
    temp_value = rman.reverseDigit(digit);
    
    ss.clear();
    ss.str("");
    
    ss << temp_value;
    ss >> temp;
    
    result.push_back(temp);
    
    // clears off the stringstream ss
    ss.clear();
    ss.str("");
    
    // Assign the result[0] to digit and compare to see if it equals to -1
    ss << result[0];
    ss >> digit;
    
    // Compare the output of digit to see if it equals to -1 or not. If it does, change the resul[0] to ERROR
    if(digit==-1)
        result[0] = error;

    // Check whether the 3rd and 4th index has any error in it or not    
    new_entry.push_back(new_data_array[1]);

    // Reverses the string and push back to the vector array, result
    result.push_back(rman.reverseString(new_entry[1]));
    
    // Gets value of what the 3rd input is
    ss1 << new_data_array[2];
    ss1 >> digit_value; 

    // Gets the size of what the 3rd input is
    ss2 << new_data_array[2].size();
    ss2 >> digit_size; 
        
    // Determine what the actual size of the value for 3rd index is. This will be used to determine if it will be ERROR or not
    fibo.getRealSize(digit_size);
    fibo.getValue(digit_value, new_data_array[2]);
    
    // Checks to see if it contains any error or not, if it is (-1), the 3rd result will be ERROR
    // If it doesn't, it will go into the fibonacci fib function where this calculates what the value of f(n) is, putting it's answer to the vector result
    if(fibo.returnValue()==-1) 
        result.push_back(error);
    else
    {
        digit = (fibo.fib(digit_value));
        
        // clears off the stringstream ss
        ss.clear();
        ss.str("");
    
        // Assign the int digit from fibonacci to turn into string new_one, where this would then be used to push to vector new_one
        ss << digit;
        ss >> new_one;
        result.push_back(new_one);
    }
    
    // clears the stringstream so that the same function can be used to overwrite without having to declare a new one
    ss1.clear();
    ss1.str("");
    ss2.clear();
    ss2.str("");
    
    // Gets value of what the 4th input input is
    ss1 << new_data_array[3];
    ss1 >> digit_value; 

    // Gets the size of what the 4th input value is
    ss2 << new_data_array[3].size();
    ss2 >> digit_size; 
        
    // Determine what the actual size of the value for 4th index is. This will be used to determine if it will be ERROR or not
    fibo1.getRealSize(digit_size);
    fibo1.getValue(digit_value, new_data_array[3]);
    
    // Checks to see if it contains any error or not, if it is (-1), the 4th result will be ERROR
    // If it doesn't, it will go into the fibonacci fib function where this calculates what the value of f(n) is, putting it's answer to the vector result
    if(fibo1.returnValue()==-1) 
        result.push_back(error);
    else
    {
        digit =  fibo1.fib(digit_value);
        // clears off the stringstream ss
        ss.clear();
        ss.str("");
    
        // Assign the int digit from fibonacci to turn into string new_one, where this would then be used to push to vector new_one
        ss << digit;
        ss >> new1;
        result.push_back(new1);
    }

    // Prints out the answer
    for(int aa=0; aa<4; aa++)
        std::cout << result[aa] << " ";
     std::cout << std::endl;
}
