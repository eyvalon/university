#ifndef EFIBONAACI_H
#define EFIBONAACI_H

class efficientfibonacci
{
private:
    // Declaring variables
    std::stringstream ss;
    
    int count;
    int size;
    int osize;
    int original;
    int ocount;
    
    int current;
    int first;
    int second;
    
    std::vector<int> data;

public:
    
    // Functions used for finding efficient fibonacci
    int getValue(int value, std::string x);
    int returnValue();
    int getRealSize(int x);
    int fib(int z);
};

#endif
 
 
