#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "fibonacci.h"

int fibonacci::getValue(int value, std::string x)
{
    // Declaring variables
    count=0;
    original = value;
    
    // Returns -1, so that it can be changed to ERROR output
    if(value<0 && size==1)
        return size=-1;
    
    if(x.find('0') != std::string::npos)
        return 0;
    
    // Finds the number of digits for a given number
    while (value > 0)
    {
        value/=10;
        count++;
    }
    

    // Returns -1, so that it can be changed to ERROR output
    if(count != osize)
        return size=-1;
    
    size = count;
    return 1;
};

// Returns what the size of the 4th input number is
int fibonacci::returnValue()
{
    return size;
};

// Gets the int value
int fibonacci::getRealSize(int x)
{
    osize = x;
    return 1;
};

// fibonacci in recursion
int fibonacci::fib(int n)
{
    if(n==0)
        return 0;
    if(n==1)
        return 1;
    else if(n==2)
        return fib(n-1) + fib(n-2);
    if(n>=3)
        return fib(n-1) + fib(n-2);
    else
        return 0;
};
