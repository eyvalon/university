#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "reverse.h"

// Reverses the digit
int reverse::reverseDigit(int value)
{
    // Used for outputing Error 
    if(value <0)
        return -1;
    if(value == 0) 
    {
        if((size==1) && (count==0) && (temp[count] != '0'))
            return -1;
        
        if((size==1) && (count==0))
            return value;
        
        if((size == count+1))
        {
            // If the first digit is a zero, add it to the end of the vector array
            data.push_back(0);
            ss << data[count];
            return ss >> result ? result : 0;
        }
        
        // Used for outputing Error 
        else if((count<size) && (temp[count] != '0'))
            return -1;

        else
            // Returns the array to an int, used for printing
            return ss >> result ? result : 0;
    } else 
    {
        // Adds the last digit of the int, to an array. Used for returning later
        data.push_back(value % 10);
        ss << data[count];
        count++;
        return reverseDigit((value/10)); 
    }
};

// Reverses the string
std::string reverse::reverseString(std::string letters)
{
    if (letters.length() == 1)
        return letters;
    else 
        return reverseString(letters.substr(1,letters.length())) + letters.at(0);
};

// Define private variables and declaring count=0
std::string reverse::getString(std::string name)
{
    count=0;
    temp = name;
    size = name.size();
    return "1";
};


// Removes any 0 starting from the end of the string first input
std::string reverse::remove_zero(std::string y, int x)
{
    // Deletes off the last part of the integer that is zero
    for (int b=x-1; b>0; b--)
    {
        if(y[b] == '0'){
            y.erase (y.begin()+b);
        }
        else
            break;
    }
    return y;
};
