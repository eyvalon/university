/*
 * =====================================================================================
 *	Operating Systems Assignment 2
 *  Filename:  memsim.c
 *
 *	Student1 Name: Pongsakorn Surasarang
 *  Student1 ID  : a1697114
 *
 *	Student2 Name: Steven Loi
 *  Student2 ID  : a1704489
 *
 *  Command: gcc -std=c11 *.c -o memsim
 *  Command:  ./memsim input_file.trace PAGESIZE NUMPAGES ALGORITHM INTERVAL
 *
 * =====================================================================================
 */

#include <ctype.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_SIZE 1000

// Defines node structure for LinkedList
typedef struct node {
  char *bits;
  char *readWrite;
  int number;

  struct node *next;
} node_t;

// Declaration of LinkedLists
node_t *head = NULL;
node_t *head_tail = NULL;

// Define global variables
int traces = 0;
int reads = 0;
int writes = 0;

int totalSize = 0;
int init = 0;

bool debug = false;
bool maxSize = false;
bool first = true;

// Checks if a LinkedList is empty
bool isEmpty(node_t *the_node) { return the_node == NULL; }

// Prints out the final output after doing all operations
void printOutput(int traces, int reads, int writes) {
  printf("events in trace:    %d\n", traces);
  printf("total disk reads:   %d\n", reads);
  printf("total disk writes:  %d\n", writes);
}

// Prints out HIT message for DEBUGGING purposes
void printHIT(int hit) {
  if (debug)
    printf("HIT:\t page %d\n", hit);
}

// Prints out MISS message for DEBUGGING purposes
void printMISS(int miss) {
  if (debug)
    printf("MISS:\t page %d\n", miss);
}

// Prints out REPLACE message for DEBUGGING purposes
void printREPLACE(int replace) {
  if (debug)
    printf("REPLACE: page %d\n", replace);
}

// Prints out REPLACE message with dirty for DEBUGGING purposes
void printREPLACEFault(int replace) {
  if (debug)
    printf("REPLACE: page %d  (DIRTY)\n", replace);
}

// Uses a delimiter to split any part of the string (i.e. spaces comma .etc)
void split(char *txt, char delim, char ***tokens) {
  int *tklen, *t, count = 1;
  char **arr, *p = (char *)txt;

  while (*p != '\0')
    if (*p++ == delim)
      count += 1;
  t = tklen = calloc(count, sizeof(int));
  for (p = (char *)txt; *p != '\0'; p++)
    *p == delim ? *t++ : (*t)++;
  *tokens = arr = malloc(count * sizeof(char *));
  t = tklen;
  p = *arr++ = calloc(*(t++) + 1, sizeof(char *));
  while (*txt != '\0') {
    if (*txt == delim) {
      p = *arr++ = calloc(*(t++) + 1, sizeof(char *));
      txt++;
    } else
      *p++ = *txt++;
  }
  free(tklen);
}

// Converts given HEX to int
unsigned long hex2int(char *a, unsigned int len) {
  int i;
  unsigned long val = 0;

  for (i = 0; i < len; i++)
    if (a[i] <= 57)
      val += (a[i] - 48) * (1 << (4 * (len - 1 - i)));
    else if (a[i] >= 97 && a[i] <= 102)
      val += (a[i] - 93) * (1 << (4 * (len - 1 - i)));
    else
      val += (a[i] - 55) * (1 << (4 * (len - 1 - i)));

  return val;
}

// Used for printing out x bits frame for testing
void printBits(int *ref, int *frame, int *modBit, int size) {
  if (debug) {
    for (int k = 0; k < size; k++) {
      printf("%d: %d - %d\n", ref[k], modBit[k], frame[k]);
    }
  }
}

// Second Chance paging algorithm
int secondChance(node_t *linked, int size, int pageSize) {
  node_t *ptr = linked;

  int ref[size];
  int frame[size];
  int modBit[size];
  int countx = 0;

  // Initialise reference bits, frame and modBit
  memset(ref, '0', size);
  memset(frame, '0', size);
  memset(modBit, '0', size);

  while (ptr != NULL) {
    int flag = 0;
    if (totalSize != 0) {
      // In the first loop, we want to make sure that all frames are filled with
      // page number (converted from addresses given from the input file)
      for (int i = 0; i < totalSize; i++)
        if (ptr->number == frame[i]) {
          printHIT(frame[i]);
          ref[i] = 1;

          // When the address is a write, we add one to write counter
          if (strcmp(ptr->readWrite, "W") == 0) {
            if (modBit[i] == 0) {
              modBit[i] = 1;
              writes++;
            }
          }

          flag = 1;
          break;
        }
    }

    // If there are no repeated page number in the current frame
    if (flag == 0) {
      if (init == 0 && frame[size - 1] != -1) {
        for (int k = 0; k < size; k++) {
          if (ref[k] == 1)
            ref[k] = 0;
        }

        init = 0;
      } else {
        for (int k = 0; k < size; k++) {
          if (ref[k] == 0) {
            init = k;
            break;
          }
        }
      }

      // In the first loop, we want to set reference bit to 1 and modBit to 0
      // initially
      if (totalSize != size) {
        ref[totalSize] = 1;
        modBit[totalSize] = 0;

        // Whenever there are a write operation, we want to change modBit to 1
        // and make write counter plus 1
        if (strcmp(ptr->readWrite, "W") == 0) {
          if (modBit[totalSize] == 0) {
            modBit[totalSize] = 1;
            writes++;
          }
        }

        frame[totalSize++] = ptr->number;
        init = totalSize;
        init = init % size;
      } else {
        int temp = frame[init];
        frame[init] = ptr->number;
        ref[init] = 1;

        // We check to see if the current page is a pagefault (where there are
        // write operation or not)
        if (modBit[init] == 1) {
          modBit[init] = 0;
          printREPLACEFault(temp);
        } else {
          printREPLACE(temp);
        }

        init++;
        init = init % size;
      }

      printMISS(ptr->number);
      reads++;
    }

    printBits(ref, frame, modBit, size);
    ptr = ptr->next;
  }
}

// Enhanced Second Chance paging algorithm
int enhancedSecondChance(node_t *linked, int size, int pageSize) {
  node_t *ptr = linked;

  int ref[size];
  int frame[size];
  int modBit[size];
  int numOfTimes = 0;

  // Initialise reference bits, frame and modBit
  memset(ref, '0', size);
  memset(frame, '0', size);
  memset(modBit, '0', size);

  while (ptr != NULL) {
    int flag = 0;
    if (totalSize != 0) {
      // Checks to see if there exists any page number in the current frame
      // If there exists, we check if the page is a write operation
      // If it is, we want to change modBit to 1
      for (int i = 0; i < totalSize; i++)
        if (ptr->number == frame[i]) {
          printHIT(frame[i]);
          ref[i] = 1;

          bool isReplaced = false;
          if (strcmp(ptr->readWrite, "W") == 0) {
            if (modBit[i] == 0)
              isReplaced = true;
            else if (modBit[i] == 2)
              reads++;
          }

          if (isReplaced) {
            modBit[i] = 1;
          } else {
            modBit[i] = 0;
          }

          flag = 1;
          break;
        }
    }

    if (flag == 0) {
      // We check to see if the operation has been run for page size number of
      // times
      if (init == 0 && frame[size - 1] != 0) {
        numOfTimes++;
        if (numOfTimes >= 2) {
          int storeArray[size];
          int pos = 0;

          // We set a check to tell the program that, there exists a (1, 1) in
          // second chance for us to work with which will allow us to see if we
          // can replace the existing page
          for (int i = 0; i < totalSize; i++) {
            if (modBit[i] == 2) {
              storeArray[pos] = i;
              pos++;
            }
          }

          // For the current page, we are  going to replace the frame with the
          // new page number
          // We also want to reset modBit to 0, and reference bit to 1
          for (int i = 0; i < pos; i++) {
            if (frame[storeArray[i]] != ptr->number) {
              printMISS(ptr->number);
              printREPLACEFault(frame[storeArray[i]]);
              frame[storeArray[i]] = ptr->number;
              modBit[storeArray[i]] = 0;
              ref[storeArray[i]] = 1;

              reads++;
              writes++;

              printBits(ref, frame, modBit, size);
              ptr = ptr->next;
            }

            if (ptr == NULL) {
              return 0;
            }
          }
        }

        // We check to see how we see the second pass (1,1 0,0 0,1 1,0)
        // When we see (1,1) we want to set it to 2 and do the operation when
        // everything has been done
        for (int k = 0; k < size; k++) {
          if (ref[k] == 1 && modBit[k] == 0)
            ref[k] = 0;

          if (ref[k] == 1 && modBit[k] == 1) {
            modBit[k] = 2;
            ref[k] = 0;
          }
        }

        init = 0;
      } else {
        // Check for the first reference bit where it is equal to 0 and modBit
        // equal to 0
        for (int k = 0; k < size; k++) {
          if (ref[k] == 0 && modBit[k] == 0) {
            init = k;
            break;
          }

          init = k;
        }
      }

      // While the frame size is not equal to the page size
      if (totalSize != size) {
        ref[totalSize] = 1;
        modBit[totalSize] = 0;

        // When the operation is a read, we change the modBit to 1
        if (strcmp(ptr->readWrite, "W") == 0) {
          if (modBit[totalSize] == 0) {
            modBit[totalSize] = 1;
          }
        }

        frame[totalSize++] = ptr->number;
        init = totalSize;
        init = init % size;
      } else {
        int temp = frame[init];
        frame[init] = ptr->number;
        ref[init] = 1;

        // We check if we need to replace an existing page with write operation
        // or not
        if (modBit[init] == 1) {
          modBit[init] = 0;
          printREPLACEFault(temp);
        } else {
          printREPLACE(temp);
        }

        init++;
        init = init % size;
      }

      printMISS(ptr->number);
      reads++;
    }

    printBits(ref, frame, modBit, size);
    ptr = ptr->next;
  }
}

// Function that shifts the bit (removing the end and adding 1 to the beginning)
void shiftBit(char *str, char *ref) {
  str[7] = '\0';
  char new_str[8];
  strcpy(new_str, "1");
  strcat(new_str, str);
  printf("%s\n", new_str);

  str = new_str;
}

// ARB paging algorithm
// Follows the logic of LRU where we replace the longest frame that has been in
// the frame the longest
int ARB(node_t *linked, int size, int pageSize, int interval) {
  node_t *ptr = linked;

  // Initialise of local variables
  bool skip = false;

  int inputs[size][2];
  int counter = 0;
  int isFull = 0;
  int ref[size];

  memset(ref, '0', size);
  char *refBits[size][1];

  // Set the referenceBits to 00000000
  for (int i = 0; i < 8; ++i) {
    refBits[i][1] = "00000000";
  }

  while (ptr != NULL) {
    skip = false;
    int lowest = 0;
    int position = 0;

    // We first check to see if the frame has been filled up (in the first loop)
    // We also want to check if the page number that we are filling up, is
    // inside the current frame lsit
    // If it is, we want to continue
    if (isFull != size) {
      for (int b = 0; b < isFull; b++) {
        if (inputs[b][1] == ptr->number) {
          inputs[b][0] = counter++;

          // If there is a write operation
          if (strcmp(ptr->readWrite, "W") == 0) {
            ref[b] = 1;
          }

          skip = true;
          break;
        }
      }
    } else {

      // We want to find the lowest counter in the second loop and then we will
      // use that frame to replace with the new page number
      lowest = inputs[0][0];
      for (int b = 0; b < isFull; b++) {
        if (inputs[b][1] == ptr->number) {
          inputs[b][0] = counter++;
          skip = true;
          break;
        }

        // Once it finds the lowest value, get the position, ready to be replace
        if (lowest > inputs[b][0]) {
          lowest = inputs[b][0];
          position = b;
        }
      }
    }

    // Prints for debug
    if (skip == false) {
      printMISS(ptr->number);
    } else {
      printHIT(ptr->number);
    }

    // While in the first loop, we check to see if there exists a write
    // operation
    // We also need to want to increment a counter that alllows us to give us
    // the longest period of the time that the frame has been in for
    if (isFull != size) {
      if (!skip) {
        inputs[isFull][0] = counter++;
        inputs[isFull][1] = ptr->number;

        if (strcmp(ptr->readWrite, "W") == 0) {
          ref[isFull] = 1;
        } else {
          ref[isFull] = 0;
        }

        if (isFull < size) {
          isFull++;
        }

        reads++;
      }
      // We check to see if the frame we are placing is from a write operation
      // or not
      // We also want to update with the new page number
    } else if (isFull == size) {
      if (!skip) {
        if (ref[position] == 1) {
          writes++;
          ref[position] = 0;
          printREPLACEFault(inputs[position][1]);
        } else {
          printREPLACE(inputs[position][1]);
        }

        inputs[position][0] = counter++;
        inputs[position][1] = ptr->number;

        if (strcmp(ptr->readWrite, "W") == 0) {
          ref[position] = 1;
        } else {
          ref[position] = 0;
        }

        if (strcmp(ptr->readWrite, "R") == 0)
          reads++;
      }
    }

    // Shifts the bits
    // counter++;
    // if(counter % interval == 0){
    //    counter = 0;
    //    char new_str[8];
    //    strcpy(new_str, refBits[7][1]);
    //    shiftBit(new_str);
    // }

    ptr = ptr->next;
  }
}

// EARB paging algorithm
int EARB(node_t *linked, int size, int pageSize, int interval) {
  node_t *ptr = linked;

  // Initialise of variables
  bool skip = false;

  int inputs[size][2];
  int counter = 0;
  int isFull = 0;
  int ref[size];

  memset(ref, '0', size);

  init = -1;
  int numOfTimes = 0;

  // First loop, that fills the first set of array size
  // Uses LRU implementation where page replacement follows the lowest page
  // number first
  while (ptr != NULL) {
    skip = false;
    int lowest = -1;
    int position = 0;

    // We first check to see if the frame has been filled up (in the first loop)
    // We also want to check if the page number that we are filling up, is
    // inside the current frame lsit
    // If it is, we want to continue
    if (numOfTimes == 0) {
      for (int b = 0; b < isFull; b++) {
        if (inputs[b][1] == ptr->number) {
          inputs[b][0] = counter++;

          if (strcmp(ptr->readWrite, "W") == 0) {
            ref[b] = 1;
          }

          skip = true;
          break;
        }
      }
    } else if (numOfTimes >= 1) {

      if (init == -1) {
        init = 0;
      }

      lowest = inputs[0][0];
      bool isFound = false;

      // If the frame number that we have currently is a write and has been in
      // the existing frame for 2 times, we want to make a check in the next
      // function that tells us that we are
      for (int b = 0; b < isFull; b++) {
        if (inputs[b][1] == ptr->number && ref[b] == 2) {
          inputs[b][0] = counter++;
          skip = true;
          reads++;
          isFound = true;
          break;
        }
      }

      // We want to find the lowest counter in the second loop and then we will
      // use that frame to replace with the new page number
      if (!isFound) {
        for (int b = 0; b < isFull; b++) {
          if (inputs[b][1] == ptr->number && ref[b] == 0) {
            inputs[b][0] = counter++;
            skip = true;
            break;
          }

          // Once it finds the lowest value, get the position, ready to be
          // replace
          if ((lowest > inputs[b][0])) {
            lowest = inputs[b][0];
            position = b;
          }
        }
      }
    }

    // Used for debug
    if (skip == false) {
      printMISS(ptr->number);
    } else {
      printHIT(ptr->number);
    }

    // We check if we are in the first loop
    // If we are, we want to check if the current position contains write
    // operation or not
    // If it is, we want to set the reference bit to 1
    // We also want to increment the total size that we have come across (till
    // we reach the pagesize)
    if (numOfTimes == 0) {
      if (!skip) {
        inputs[isFull][0] = counter++;
        inputs[isFull][1] = ptr->number;

        if (strcmp(ptr->readWrite, "W") == 0) {
          ref[isFull] = 1;
        } else {
          ref[isFull] = 0;
        }

        if (isFull < size) {
          isFull++;
        }

        reads++;
      }
    } else if (numOfTimes >= 1) {

      // We check if we need to replace the frame at the current position or we
      // need to replace the frame where it's not at the current position but is
      // still replaceable
      if (!skip) {
        if (ref[position] == 1) {
          ref[position] = 2;
          inputs[position][0] = counter++;

          for (int k = position; k < size; k++) {
            if (ref[k] == 0) {
              position = k;
              break;
            }
            position = k;
          }

          inputs[position][0] = counter++;
          printREPLACE(inputs[position][1]);

        } else {
          printREPLACE(inputs[position][1]);

          inputs[position][0] = counter++;
          inputs[position][1] = ptr->number;

          if (strcmp(ptr->readWrite, "R") == 0)
            reads++;
        }
      }

      init++;
      init = init % size;

      if (init == 0) {
        numOfTimes++;
      }
    }

    if (debug) {
      for (int k = 0; k < size; k++) {
        printf("%d: %d - %d\n", ref[k], inputs[k][0], inputs[k][1]);
      }
    }

    ptr = ptr->next;

    if (isFull == size && numOfTimes == 0) {
      numOfTimes++;
    }
  }
}

// Insert link to the Head LinkedList
void insertHead(char *bits, char *readWrite, int pageSize) {
  node_t *link = (struct node *)malloc(sizeof(node_t));

  link->bits = bits;
  link->readWrite = readWrite;
  link->number = hex2int(bits, 8) / pageSize;

  if (isEmpty(head)) {
    head = link;
    head->next = NULL;
    head_tail = head;
    return;
  }

  head_tail->next = link;
  head_tail = link;
}

// Function that reads data from file
// Reads from the file and then store to linkedList
char *readFile(char *fileName, int pageSize) {
  FILE *fp;
  char buf[MAX_SIZE];

  if ((fp = fopen(fileName, "r")) == NULL) {
    perror("fopen source-file");
    // return 1;
  }

  while (fgets(buf, sizeof(buf), fp) != NULL) {
    char **arr = NULL;
    buf[strlen(buf) - 1] = '\0';
    int c = 0;

    split(buf, ' ', &arr);
    char *readWrite = arr[1];
    char *bits = arr[0];

    // Checks for removal of commented lines
    if (buf[0] == 'R' || buf[0] == 'W') {
      insertHead(readWrite, bits, pageSize);
      traces++;
    }
  }

  fclose(fp);
}

// Main driven code
int main(int argc, char *argv[]) {
  // Defines the input given arguments
  char *PAGESIZE = argv[2];
  char *NUMPAGES = argv[3];
  char *ALGORITHM = argv[4];
  char *INTERVAL = argv[5];

  // Parse input files and store into LinkedList
  readFile(argv[1], atoi(PAGESIZE));

  // Depending on the user's ALGORITHM input, each function will have it's own
  // implementation
  node_t *ptr = head;
  if (strcmp(ALGORITHM, "SC") == 0) {
    secondChance(ptr, atoi(NUMPAGES), atoi(PAGESIZE));
  } else if (strcmp(ALGORITHM, "ESC") == 0) {
    enhancedSecondChance(ptr, atoi(NUMPAGES), atoi(PAGESIZE));
  } else if (strcmp(ALGORITHM, "ARB") == 0) {
    ARB(ptr, atoi(NUMPAGES), atoi(PAGESIZE), atoi(INTERVAL));
  } else if (strcmp(ALGORITHM, "EARB") == 0) {
    EARB(ptr, atoi(NUMPAGES), atoi(PAGESIZE), atoi(INTERVAL));
  }

  // Prints the final output
  printOutput(traces, reads, writes);

  return 0;
}