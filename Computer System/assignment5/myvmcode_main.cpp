// Student Name: Pongsakorn Surasarang
// Student Number: a1697114
// Description: Translates the main.jack to VM codes

#include <iostream>
#include <stdlib.h>
#include "jacktokens.h"
#include "jackxml.h"
#include "jacksymbols.h"
#include "myall_Map.h"
#include <string>
#include <vector>

using namespace std;

//Declaration of variables
jacktokens *tokeniser = NULL;
string token = "?";
string tokenclass = "?";
string tokenvalue = "?";
string previous_token = "?";
string previous_tokenvalue = "?";

string keyword_string;
string type_string;
string varname_string;
string tokenvalue_tostore;
string classname_string;
string function_type_string;
string function_name_string;
string function_full_name;
string temp_let;
string local_let;
string after_equal;

vector<string> final_list; 
vector<string> function_name_list;

bool within_while = false;
bool within_if = false;
bool in_while = false;
bool in_if = false;
bool in_else = false;
bool global_add = false;
bool local_add = false;
bool ismethod = false;

int counter_token_global = 0;
int counter_token_local = 0;
int counter_token_this = 0;
int while_count = 0;
int if_count = 0;
int call_count = 0;
int function_count = 0;
int this_counter = 0;

void parseProgram();
void parseClass();
void parseClassVarDec();
void parseType();
void parseSubRoutineDec();
void parseParameterList();
void parseSubRoutineBody();
void parseVarDec();
void parseClassName();
void parseSubRoutineName();
void parseVarName();
void parseStatements();
void parserStatement();
void parseLetStatement();
void parseIfStatement();
void parseWhileStatement();
void parseDoStatement();
void parseReturnStatement();
void parseExpression();
void parseTerm();
void parseExpressionList();
void parseOp();
void parseUnaryOp();
void parseKeywordConstant();

void add_into_map_global();
void add_into_map_local();

// Declaration of pre-defined map, which are defined in myMap.h file. This gives access to store variables to the blank lookup table.
std::map<std::string, std::string> global (global_values, global_values + global_values_size);
std::map<std::string, std::string> local (local_values, local_values + local_values_size);
std::map<std::string, std::string> type (type_values, type_values + type_values_size);
std::map<std::string, std::string> type_count (type_count_values, type_count_values + type_count_values_size);
std::map<std::string, std::string> function_name (function_name_values, function_name_values + function_name_values_size);

// Kept it as a reference to working on which part is used to print out certain section of the program
void print_xml_after() {

}

// Gets the next tokenvalue
void nextch() {
    token = tokeniser->next_token();
    tokenclass = tokeniser->token_class();
    tokenvalue = tokeniser->token_value();
}

// Checks to see if the input string contains only string or number
bool is_digit(std::string str){
    return (str.find_first_not_of( "0123456789" ) == std::string::npos);
};

// Function that helps to store into local and global lookup table.
void add_into_map_global(){
    if(global.find(previous_tokenvalue) != global.end())
        exit(0);
    else
        global.insert(std::pair<std::string , std::string> (previous_tokenvalue, tokenvalue_tostore));
}

void add_into_map_local(){
    if(local.find(previous_tokenvalue) != local.end())
        exit(0);
    else
        local.insert(std::pair<std::string , std::string> (previous_tokenvalue, tokenvalue_tostore));
}

// Pre-defined functions that are necessarily for this jack program
void mustbe(string expected) {
    if (expected != token) {
        jackxml::erase_buffer();
        exit(0) ;
    }
    
    previous_token = tokenclass;
    previous_tokenvalue = tokenvalue;
    nextch();
}

bool have(string expected) {
    if (expected != token)
        return false;
    
    previous_token = tokenclass;
    previous_tokenvalue = tokenvalue;	
    nextch();
    return true;
}

void parseProgram() {
    if (have("class"))     
        parseClass();
}

void parseClass() {
    print_xml_after();
    parseClassName();
    classname_string = previous_tokenvalue;
    mustbe("{");
    print_xml_after();
    
    // Gets global variables which is used to store in the global lookup table
    parseClassVarDec();
    
    // Has functions that will be used for calculating the VM code
    parseSubRoutineDec();
    mustbe("}");
    print_xml_after();
}

// Global variable section, which is used to add into the global lookup table with the correct order of code structure (i.e. aa,int,static,0 )
void parseClassVarDec() {
    while (have("static") || have("field")) {
        print_xml_after();    
        
        if(previous_tokenvalue == "static")
            keyword_string = "static";
        else
            keyword_string = "this";
        
        parseType();
        type_string = previous_tokenvalue;
        global_add = true;
        parseVarName();
        global_add = false;
        
        if(keyword_string == "static")
            counter_token_global++;
        else{
            counter_token_this++;
            this_counter++;
        }
        
        // If the global variable consists of more than one
        while (have(",")) {
            print_xml_after();
            global_add = true;
            parseVarName();
            global_add = false;
            
            // Adds a counter for local / global counter respectively to what the tokenvalue is
            if(keyword_string == "static")
                counter_token_global++;
            else{
                counter_token_this++;
                this_counter++;
            }
        }
            
        type_string = "";
        keyword_string = "";
        varname_string = "";
            
        mustbe(";");
        print_xml_after();
    }
}

// Checks of what type it is, and depending on the type, parse it to the correct function
void parseType() {
    if ((token == "int") || (token == "char") || (token == "boolean") || (token == "identifier")) {        
        if (have("int") || have("char") || have("boolean"))
            print_xml_after();
        else if (token == "identifier")
            parseClassName();
    }
}

// For each and every function there exists, check if it is a method, if it does, change counter_token_local value to 1 instead of 0 (which is reset at the end of each loop), where this will be used for
// printing later
void parseSubRoutineDec() {
    while (have("constructor") || have("function") || have("method")) {
        function_type_string = "function";
                
        bool isvoid = false;
        string void_name;
        
        // Depending on the function type (function, method, or constructor), do their part respectively where later, it will be used to determine what to do by IF statement
        if(previous_tokenvalue=="constructor"){
            print_xml_after();
            nextch();
        }else{
            if(previous_tokenvalue=="method"){
                ismethod = true;
                counter_token_local=1;
            }
            print_xml_after();
            if (have("void")){
                print_xml_after();
                void_name = tokenvalue;
                isvoid = true;
            }else if ((token == "int") || (token == "char") || (token == "boolean") || (token == "identifier"))
                parseType();
        }
                
        parseSubRoutineName();
        mustbe("(");
        print_xml_after();
        parseParameterList();
        mustbe(")");
        print_xml_after();
        parseSubRoutineBody();
        
        // Decides on whether the given function contains a void before { (i.e. function void main() )
        if(isvoid==false){
            function_full_name = function_type_string + " " + classname_string+ "." + function_name_string + " " + to_string(counter_token_local);
            if(function_full_name != ""){
                final_list.push_back("---");
                function_name_list.push_back(function_full_name);
                function_full_name = "";
            }
        }else if(isvoid){
            function_name_list.push_back(function_type_string + " " + classname_string+ "." + void_name + " " + to_string(counter_token_local));
        }

        function_type_string="";
        function_name_string="";
        counter_token_local=0;
        ismethod = false;
    }
}

void parseParameterList() {
    parseType();

    if(previous_tokenvalue!="("){
        keyword_string = "argument";
        type_string = previous_tokenvalue;
        local_add = true;
        parseVarName();
        local_add = false;
    }else
        parseVarName();

    // For arguments that appears more than one
    while (have(",")) {
        counter_token_local++;
        print_xml_after();
        parseType();
        type_string = previous_tokenvalue;
        local_add = true;
        parseVarName();
        local_add = false;
    }

    // Resets the variable to zero so that it doesn't conflict with one another when called later
    type_string = "";
    keyword_string = "";
    varname_string = "";
}

void parseSubRoutineBody() {
    if (have("{")) {
        counter_token_local=0;
        print_xml_after();
        parseVarDec();
        parseStatements();
        mustbe("}");
        print_xml_after();
        local.clear();
    }
}

void parseVarDec() {
    while (have("var")) {
        print_xml_after();
        keyword_string = "local";
        parseType();
        type_string = previous_tokenvalue;
        local_add = true;
        parseVarName();
        local_add = false;
        counter_token_local++;
        
        // For arguments that appears more than one
        while (have(",")) {
            print_xml_after();
            local_add = true;
            parseVarName();
            local_add = false;
            counter_token_local++;
        }
        
        // Resets the variable to zero so that it doesn't conflict with one another when called later
        type_string = "";
        keyword_string = "";
        varname_string = "";

        mustbe(";");
        print_xml_after();
    }
}

void parseClassName() {
    if (have("identifier")) {
        print_xml_after();
    }
}

void parseSubRoutineName() {
    if (have("identifier")) {
        print_xml_after();
        function_name_string = previous_tokenvalue;
        function_name.insert(std::pair<std::string , std::string> (function_name_string, to_string(function_count) ))  ;
        function_count++;
    }
}

void parseVarName() {
    if (have("identifier")) {
        varname_string = previous_tokenvalue;
        
        // Helps to add to the lookup table - Global / Local. This is determined by seeing whether the tokenvalue is static / field as well as whether global_add or
        // local_add is called where this determines to see if it needs to add to the global / local lookup.
        if(global_add){
            if(keyword_string == "static"){
                tokenvalue_tostore = varname_string + "," + type_string + "," + keyword_string + "," + to_string(counter_token_global);
                type_count.insert(std::pair<std::string , std::string> (varname_string, to_string(counter_token_global)));
            }else{
                tokenvalue_tostore = varname_string + "," + type_string + "," + keyword_string + "," + to_string(counter_token_this);
                type_count.insert(std::pair<std::string , std::string> (varname_string, to_string(counter_token_this)));
            }
            add_into_map_global();
            type.insert(std::pair<std::string , std::string> (varname_string, keyword_string));
        }
        
        if(local_add && previous_tokenvalue!="("){
            tokenvalue_tostore = varname_string + "," + type_string + "," + keyword_string + "," + to_string(counter_token_local);
            add_into_map_local();
        }

        print_xml_after();
        
    }
}

// Type of statements that is needed for functions (let, if, while, do and return)
void parseStatements() {
    parserStatement();
}

void parserStatement() {
    while ((token == "let") || (token == "if") || (token == "while") || (token == "do") || (token == "return")) {
        parseLetStatement();
        parseIfStatement();
        parseWhileStatement();
        parseDoStatement();
        parseReturnStatement();	
    }	
}

void parseLetStatement() {
    while (have("let")) {
        print_xml_after();
        parseVarName();
        
        if (have("[")) {
            print_xml_after();
            parseExpression();
            mustbe("]");
            print_xml_after();
        }
        
        temp_let = previous_tokenvalue;
        mustbe("=");
        string temp_symbol = previous_tokenvalue;
        print_xml_after();
        parseExpression();
        mustbe(";");
        print_xml_after();
        
        // Prints out pop static / this
        if(temp_symbol == "="){
            if(type.find(temp_let) != type.end())
                final_list.push_back("pop " + type.find(temp_let)->second + " " + type_count.find(temp_let)->second);
        }
    }
    
    // Resets what the local variable name is
    local_let = "";
}

void parseIfStatement() {
    while (have("if")) {
        
        print_xml_after();
        int temp_num = if_count;
        in_if = true;
        final_list.push_back("push constant 0");
        mustbe("(");
        print_xml_after();
        parseExpression();	
        
        if(in_if)
            within_if = true;
        
        // IFSTATEMENT that is in the if / nested if loop. This helps to print the correct jackxml::buffer_line for if loop it is in
        if(within_if){
            if(previous_tokenvalue == "true"){
                final_list.push_back("not"); 
            }
            final_list.push_back("if-goto IF_TRUE" + to_string(if_count));
            final_list.push_back("goto IF_FALSE" + to_string(if_count));
            final_list.push_back("label IF_TRUE" + to_string(if_count));
            if_count++;
        }
        
        mustbe(")");
        print_xml_after();
        mustbe("{");
        print_xml_after();
        parseStatements();
        mustbe("}");
        print_xml_after();

        // IFSTATEMENT that is in the else part of the IF statement. This checks to see what to do when it is inside the else, by printing the necessarily jackxml::buffer_line()
        if (have("else")) {
            in_else = true;
            print_xml_after();
            mustbe("{");
            print_xml_after();
            parseStatements();
            mustbe("}");
            
            if(within_if == true){
                print_xml_after();
                
                final_list.push_back("goto IF_END" + to_string(temp_num)); 
                final_list.push_back("label IF_FALSE" + to_string(temp_num)); 
                final_list.push_back("label IF_END" + to_string(temp_num)); 
                
                within_while == false;
                
            }else{
            
                print_xml_after();
                final_list.push_back("if-goto IF_END" + to_string(if_count)); 
                
                if(while_count == temp_num){
                    final_list.push_back("goto IF_END" + to_string(temp_num+1)); 
                    final_list.push_back("label IF_FALSE" + to_string(temp_num)); 
                    final_list.push_back("label IF_END" + to_string(temp_num+1)); 
                }else{
                final_list.push_back("goto IF_END" + to_string(temp_num)); 
                final_list.push_back("label IF_FALSE" + to_string(temp_num)); 
                final_list.push_back("label IF_END" + to_string(temp_num)); 
                }
                
                if_count=temp_num+1;
            }
            in_else = false;
        }else{
            final_list.push_back("label IF_FALSE" + to_string(temp_num));
        }
    }
    in_if = false;
}

void parseWhileStatement() {
    while (have("while")) {
        
        int temp_num = while_count;
        in_while = true;
        final_list.push_back("label WHILE_EXP" + to_string(temp_num)); 
        final_list.push_back("push constant 0"); 
        
        print_xml_after();
        mustbe("(");
        print_xml_after();
        parseExpression();
        
        // Checks to see what to do when inside the parseParameterList, is true or false and from that, print the correct jackxml::buffer_line() at that position
        if(previous_tokenvalue == "true" || previous_tokenvalue == "false"){
            
            if(in_while)
                within_while = true;
            
            if(within_while){
                final_list.push_back("not"); 
                if(previous_tokenvalue == "true"){
                    final_list.push_back("not"); 
                }
                
                final_list.push_back("if-goto WHILE_END" + to_string(while_count)); 
                while_count++;
            }
        }
        
        mustbe(")");
        print_xml_after();
        mustbe("{");
        print_xml_after();
        
        if (within_while == false)
            while_count++;
        
        parseStatements();
        mustbe("}");
        
        // WHILESTATEMENT that is in the nested while loop. This helps to print the correct jackxml::buffer_line for which loop it is in
        if(within_while == true){
            
            print_xml_after();
            final_list.push_back("goto WHILE_EXP" + to_string(temp_num)); 
            final_list.push_back("label WHILE_END" + to_string(temp_num)); 
            within_while == false;
            
        }else{
        
            print_xml_after();
            final_list.push_back("if-goto WHILE_END" + to_string(while_count)); 
            
            if(while_count == temp_num){
                final_list.push_back("goto WHILE_END" + to_string(temp_num+1)); 
                final_list.push_back("label WHILE_END" + to_string(temp_num+1)); 
            }else{
                final_list.push_back("goto WHILE_END" + to_string(temp_num)); 
                final_list.push_back("label WHILE_END" + to_string(temp_num)); 
            }
            
            while_count=temp_num+1;
        }
    }
    in_while=false;
}

void parseDoStatement() {
    while (have("do")) {
        print_xml_after();
        
        if (token == "identifier") {
            string tmpTokenclass = tokenclass;
            string tmpTokenvalue = tokenvalue;	
            nextch();
            
            if (have(".")) {
                print_xml_after();		
                
            }else {
            }
        }
        
        parseSubRoutineName();
        mustbe("(");
        print_xml_after();
        parseExpressionList();
        mustbe(")");
        print_xml_after();
        mustbe(";");
        print_xml_after();
    }
}

void parseReturnStatement() {
    if (have("return")) {
        print_xml_after();
        
        // Hardcoded jackxml::buffer_line() that is needed for printing when it sees a return. Ideally, this would be in some function that I can't seem to know which one it is in
        if(type.find(tokenvalue) != type.end()){
            if(ismethod){
                final_list.push_back("push argument 0"); 
                final_list.push_back("pop pointer 0");
            }
            final_list.push_back("push " + type.find(tokenvalue)->second + " " + type_count.find(tokenvalue)->second);
            final_list.push_back(previous_tokenvalue); 
        }else{
            if(tokenvalue == "this"){
                final_list.push_back("push constant " + to_string(this_counter)); 
                final_list.push_back("call Memory.alloc 1"); 
                final_list.push_back("pop pointer 0"); 
                final_list.push_back("push pointer 0"); 
            }else{
                if(ismethod){
                    final_list.push_back("push argument 0"); 
                    final_list.push_back("pop pointer 0"); 
                    final_list.push_back("push pointer 0"); 
                    final_list.push_back("push argument 1"); 
                    final_list.push_back("push argument 1"); 
                    final_list.push_back("call " + classname_string + "." + tokenvalue + " 3");

                }
                else
                    final_list.push_back("push constant 0");
                
            }
            final_list.push_back(previous_tokenvalue); 
        }

        if (have(";"))
            print_xml_after();
        else {
            parseExpression();
            mustbe(";");
            print_xml_after();
        }
    }
}

// Handles expression operation
void parseExpression() {
    
    parseTerm();
    string temp_symbol;
    string condition_check;
    
    // Determines if the token symbol is an "add", if it is, store add into temp_symbol, to be used for jackxml::buffer_line later
    if(tokenvalue == "+"){
        condition_check = "add";
        temp_symbol = tokenvalue;
    }
        
    while ((token == "+") || (token == "-") || (token == "*") || (token == "/") || (token == "&") 
        || (token == "|") || (token == "<") || (token == ">") || (token == "=")) {
        parseOp();
        parseTerm();
    }
    
    // Stores the necessarily condition_check for which symbols relates to (+ is add)
    if((temp_symbol == "+") || (temp_symbol == "-") || (temp_symbol == "*") || (temp_symbol == "/") || (temp_symbol == "&") 
        || (temp_symbol == "|") || (temp_symbol == "<") || (temp_symbol == ">") || (temp_symbol == "="))
        final_list.push_back(condition_check);
}

// Handles how terms are passed inside each parse of token when called from other functions
void parseTerm() {
    if ((token == "integerConstant") || (token == "stringConstant") || (token == "(") || (token == "-")
        || (token == "~") || (token == "true") || (token == "false") || (token == "null") || (token == "this") 
        || (token == "identifier") || (token =="constructor") || (token == "function") || (token == "method")) {
        bool printout = false;
        if(tokenvalue == classname_string)
            printout = true;

        if ((have("integerConstant")) || (have("stringConstant"))) {
            print_xml_after();
            
            if(is_digit(previous_tokenvalue))
                final_list.push_back("push constant " + previous_tokenvalue);
            else
                final_list.push_back("push constant " + to_string(previous_tokenvalue.length()));
            
            if(local_let!=""){
                final_list.push_back("call " + classname_string + "." + local_let + " " + to_string(call_count));
                final_list.push_back("pop local 1");
            }
            
        }else if ((token == "true") || (token == "false") || (token == "null") || (token == "this"))
            parseKeywordConstant();	
        else if ((token == "identifier")) {
            jackxml::flush_output();
            string tmpTokenclass = tokenclass;
            string tmpTokenvalue = tokenvalue;
            nextch();
            
            after_equal = tokenvalue;
            
            if(previous_tokenvalue=="="){
                if(type.find(tmpTokenvalue) != type.end()){
                    final_list.push_back("push " + type.find(tmpTokenvalue)->second + " " + type_count.find(tmpTokenvalue)->second);
                }
            }
            
            // Checks for if tokenvalue contains " '[' / '.' / '(' "
            if (have("[")) {
                print_xml_after();
                parseExpression();
                mustbe("]");
                print_xml_after();	
                
            }else if (have(".")) {
                
                local_let = tokenvalue; // Used to determine if 
                if(global.find(temp_let) != global.end() || printout){
                    final_list.push_back("call " + classname_string + "." + tokenvalue + " " + to_string(call_count) );
                }
                
                if(function_name.find(tokenvalue) != function_name.end() && printout && global.find(temp_let) == global.end()){
                    final_list.push_back("pop local 0");
                    final_list.push_back("push local 0");
                }
                call_count++;
                
                print_xml_after();
                parseSubRoutineName();
                mustbe("(");
                print_xml_after();
                parseExpressionList();
                mustbe(")");
                print_xml_after();
                
            }else if (have("(")) {
                print_xml_after();
                parseExpressionList();
                mustbe(")");
                print_xml_after();
            }else {

            }
            
        // Checks for tokenvalue that contains "("
        }else if (have("(")) {
            print_xml_after();
            parseExpression();
            mustbe(")");
            print_xml_after();
            
        // Checks for tokenvalue that contains " '-' / '~' "
        }else if ((token == "-") || (token == "~")) {
            parseUnaryOp();
            parseTerm();
        }
    }
}

// Checking of expressions while in statements
void parseExpressionList() {

    if ((token == "integerConstant") || (token == "stringConstant") || (token == "(") || (token == "-")
        || (token == "~") || (token == "true") || (token == "false") || (token == "null") || (token == "this") 
        || (token == "identifier") || (token =="constructor") || (token == "function") || (token == "method")) {
        parseExpression();
    
        // For arguments that appears more than one
        while (have(",")) {
            print_xml_after();
            if ((token == "integerConstant") || (token == "stringConstant") || (token == "(") || (token == "-")
                || (token == "~") || (token == "true") || (token == "false") || (token == "null") || (token == "this") 
                || (token == "identifier") || (token =="constructor") || (token == "function") || (token == "method")) {
                parseExpression();
            }
        }
    }

}

// Checks for operations
void parseOp() {
    if (have("+") || have("-") || have("*") || have("/") || have("&") 
        || have("|") || have("<") || have(">") || have("=")) {
        print_xml_after();
    }	
}

// Checks for unaryop
void parseUnaryOp() {
    if (have("-") || have("~")) {
        print_xml_after();
    } 
}

// Checks for keyword
void parseKeywordConstant() {
    if ( have("true") || have("false") || have("null") || have("this") ) {
        print_xml_after();
    }
}

int main(){
    tokeniser = new jacktokens();
    nextch();
    parseProgram();
    int xcounter = 0;
    
    // Prints out using jackxml::buffer_line(). Where "---" appears in the vector "final_list", replace it with the next function name (from Main.jack perspective)
    for(int z=0; z<final_list.size(); z++){
        if(z==0){
            jackxml::buffer_line(function_name_list[0]);
            xcounter++;
        }
        
        if(final_list[z] == "---"){
            if(!function_name_list[xcounter].empty()){
                jackxml::buffer_line(function_name_list[xcounter]);
                xcounter++;
            }
        }else 
            jackxml::buffer_line(final_list[z]);
    }
    
    jackxml::flush_output();
    mustbe("?");
}
