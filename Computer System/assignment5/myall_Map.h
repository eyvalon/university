// Student Name: Pongsakorn Surasarang
// Student Number: a1697114
// Description: Defines empty lookup tables that is needed for parsing to the main.cpp which is used to store tokenvalues accordingly to their type (global / local / function name / type)

#ifndef MYMAP_H
#define MYMAP_H
#include <map>

// Declaration of map variables
typedef std::pair<std::string, std::string> global_map;
typedef std::pair<std::string, std::string> local_map;
typedef std::pair<std::string, std::string> type_map;
typedef std::pair<std::string, std::string> type_count_map;
typedef std::pair<std::string, std::string> function_name_map;

// Jump lookup table pre-defined.
const global_map global_values[] = {
};

// Destination lookup table pre-defined.
const local_map local_values[] = {
};

// Comp lookup table pre-defined when a=0 and a=1.
const type_map type_values[] = {
};

// Comp lookup table pre-defined when a=0 and a=1.
const type_count_map type_count_values[] = {
};

// Comp lookup table pre-defined when a=0 and a=1.
const function_name_map function_name_values[] = {
};

// Declaration for map size, used for getting the size of map.
const int global_values_size = sizeof(global_values) / sizeof(global_values[0]);
const int local_values_size = sizeof(local_values) / sizeof(local_values[0]);
const int type_values_size = sizeof(type_values) / sizeof(type_values[0]);
const int type_count_values_size = sizeof(type_count_values) / sizeof(type_count_values[0]);
const int function_name_values_size = sizeof(function_name_values) / sizeof(function_name_values[0]);

// Declaration to be passed to extern.
extern std::map <std::string, std::string> global;
extern std::map <std::string, std::string> local;
extern std::map <std::string, std::string> type;
extern std::map <std::string, std::string> type_count;
extern std::map <std::string, std::string> function_name;

#endif //MYMAP_H

