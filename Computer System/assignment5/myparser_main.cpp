// Student Name: Pongsakorn Surasarang
// Student Number: a1697114
// Description: Translates the main.jack to XML format

#include <iostream>
#include <stdlib.h>
#include "jacktokens.h"
#include "jackxml.h"

using namespace std;

jacktokens *tokeniser = NULL;
string token = "?";
string tokenclass = "?";
string tokenvalue = "?";
string previous_token = "?";
string previous_tokenvalue = "?";

void parseProgram();
void parseClass();
void parseClassVarDec();
void parseType();
void parseSubRoutineDec();
void parseParameterList();
void parseSubRoutineBody();
void parseVarDec();
void parseClassName();
void parseSubRoutineName();
void parseVarName();
void parseStatements();
void parserStatement();
void parseLetStatement();
void parseIfStatement();
void parseWhileStatement();
void parseDoStatement();
void parseReturnStatement();
void parseExpression();
void parseTerm();
void parseExpressionList();
void parseOp();
void parseUnaryOp();
void parseKeywordConstant();

// Used for printing out what the symbols are at certain part of the program when called later in the program
void print_xml_after() {
	jackxml::open_node(previous_token);
	jackxml::node_text(previous_tokenvalue);
	jackxml::close_node(previous_token);	
}

// Gets the next tokenvalue
void nextch() {
    token = tokeniser->next_token();
    tokenclass = tokeniser->token_class();
    tokenvalue = tokeniser->token_value();
}

// Pre-defined functions that are necessarily for this jack program
void mustbe(string expected) {
    if (expected != token) {
        jackxml::erase_buffer();
        exit(0) ;
    }
    
    previous_token = tokenclass;
    previous_tokenvalue = tokenvalue;
    nextch();
}

bool have(string expected) {
    if (expected != token)
        return false;
    
    previous_token = tokenclass;
    previous_tokenvalue = tokenvalue;	
    nextch();
    return true;
}

// Pre-defined functions that are necessarily for this jack program
void parseProgram() {
	if (have("class")) {
		jackxml::open_node("class");
		parseClass();
		jackxml::close_node("class");
	}
}

void parseClass() {
    print_xml_after();
    parseClassName();
    mustbe("{");
    print_xml_after();
    
    // Gets global variables which is used to store in the global lookup table
    parseClassVarDec();
    
    // Has functions that will be used for calculating the symbols code
    parseSubRoutineDec();
    mustbe("}");
    print_xml_after();
}

// Global variable section, which is used to add into the global lookup table with the correct order of code structure (i.e. aa,int,static,0 )
void parseClassVarDec() {
	while (have("static") || have("field")) {
		jackxml::open_node("classVarDec");
        
		print_xml_after();
		parseType();
		parseVarName();
        
        // If the global variable consists of more than one
		while (have(",")) {
			print_xml_after();
			parseVarName();
		}
		mustbe(";");
		print_xml_after();
        
		jackxml::close_node("classVarDec");
	}
}

// Checks of what type it is, and depending on the type, parse it to the correct function
void parseType() {
	if ((token == "int") || (token == "char") || (token == "boolean") || (token == "identifier")) {
		jackxml::open_node("type");
        
		if (have("int") || have("char") || have("boolean")) {
			print_xml_after();
		}else if (token == "identifier") {
			parseClassName();
		}
		
		jackxml::close_node("type");
	}
}

// For each and every function there exists, check if it is a method, if it does, print the correct XML for tokenvalue
void parseSubRoutineDec() {
	while (have("constructor") || have("function") || have("method")) {
		jackxml::open_node("subroutineDec");
        
		print_xml_after();
		if (have("void")) {
			print_xml_after();
		}else if ((token == "int") || (token == "char") || (token == "boolean") || (token == "identifier")) {
			parseType();
		}	
		parseSubRoutineName();
		mustbe("(");
		print_xml_after();
		parseParameterList();
		mustbe(")");
		print_xml_after();
		parseSubRoutineBody();
        
		jackxml::close_node("subroutineDec");
	}
}

// Checks for parameterList for which is used for printing out how many arguments there are + keeping count of number of arguments
void parseParameterList() {
	jackxml::open_node("parameterList");
    
	parseType();
	parseVarName();
    
    // For arguments that appears more than one
	while (have(",")) {
		print_xml_after();
		parseType();
		parseVarName();
	}
	
	jackxml::close_node("parameterList");
}

void parseSubRoutineBody() {
	if (have("{")) {
		jackxml::open_node("subroutineBody");		
        
		print_xml_after();
		parseVarDec();
		parseStatements();
		mustbe("}");
		print_xml_after();
        
		jackxml::close_node("subroutineBody");
	}
}

void parseVarDec() {
	while (have("var")) {
		jackxml::open_node("varDec");
        print_xml_after();
		parseType();
		parseVarName();
        
        // For arguments that appears more than one
		while (have(",")) {
			print_xml_after();
			parseVarName();
		}
		
		mustbe(";");
		print_xml_after();
		jackxml::close_node("varDec");
	}
}

void parseClassName() {
	if (have("identifier")) {
		jackxml::open_node("className");
		print_xml_after();
		jackxml::close_node("className");		
	}
}

void parseSubRoutineName() {
	if (have("identifier")) {
		jackxml::open_node("subroutineName");
		print_xml_after();
		jackxml::close_node("subroutineName");		
	}
}

void parseVarName() {
	if (have("identifier")) {
		jackxml::open_node("varName");
		print_xml_after();
		jackxml::close_node("varName");		
	}	
}

// Type of statements that is needed for functions (let, if, while, do and return)
void parseStatements() {
	jackxml::open_node("statements");
	parserStatement();
	jackxml::close_node("statements");
}

void parserStatement() {
	while ((token == "let") || (token == "if") || (token == "while") || (token == "do") || (token == "return")) {
		parseLetStatement();
		parseIfStatement();
		parseWhileStatement();
		parseDoStatement();
		parseReturnStatement();	
	}	
}

void parseLetStatement() {
	while (have("let")) {
		jackxml::open_node("statement");
		jackxml::open_node("letStatement");	
		print_xml_after();
		parseVarName();
		if (have("[")) {
			print_xml_after();
			parseExpression();
			mustbe("]");
			print_xml_after();
		}
		mustbe("=");
		print_xml_after();
		parseExpression();
		mustbe(";");
		print_xml_after();
		jackxml::close_node("letStatement");
		jackxml::close_node("statement");				
	}
}

void parseIfStatement() {
	while (have("if")) {
		jackxml::open_node("statement");		
		jackxml::open_node("ifStatement");	
		print_xml_after();
		mustbe("(");
		print_xml_after();
		parseExpression();	
		mustbe(")");
		print_xml_after();
		mustbe("{");
		print_xml_after();
		parseStatements();
		mustbe("}");
		print_xml_after();
		if (have("else")) {
			print_xml_after();
			mustbe("{");
			print_xml_after();
			parseStatements();
			mustbe("}");
			print_xml_after();			
		}
		jackxml::close_node("ifStatement");
		jackxml::close_node("statement");			
	}
}

void parseWhileStatement() {
	while (have("while")) {
		jackxml::open_node("statement");	
		jackxml::open_node("whileStatement");
		print_xml_after();
		mustbe("(");
		print_xml_after();
		parseExpression();
		mustbe(")");
		print_xml_after();
		mustbe("{");
		print_xml_after();
		parseStatements();
		mustbe("}");
		print_xml_after();
		jackxml::close_node("whileStatement");
		jackxml::close_node("statement");
	}
}

void parseDoStatement() {
	while (have("do")) {
		jackxml::open_node("statement");	
		jackxml::open_node("doStatement");
		print_xml_after();
		if (token == "identifier") {
			string tmpTokenclass = tokenclass;
			string tmpTokenvalue = tokenvalue;	
			nextch();
			if (have(".")) {
				jackxml::open_node("className");
				jackxml::open_node(tmpTokenclass);
				jackxml::node_text(tmpTokenvalue);
				jackxml::close_node(tmpTokenclass);
				jackxml::close_node("className");	
				print_xml_after();			
			}else {
				jackxml::open_node("subroutineName");
				jackxml::open_node(tmpTokenclass);
				jackxml::node_text(tmpTokenvalue);
				jackxml::close_node(tmpTokenclass);
				jackxml::close_node("subroutineName");
			}		
		}	
		parseSubRoutineName();
		mustbe("(");
		print_xml_after();
		parseExpressionList();
		mustbe(")");
		print_xml_after();
		mustbe(";");
		print_xml_after();
		jackxml::close_node("doStatement");
		jackxml::close_node("statement");			
	}
}

void parseReturnStatement() {
	if (have("return")) {
		jackxml::open_node("statement");	
		jackxml::open_node("returnStatement");
        
		print_xml_after();
		if (have(";")) {
			print_xml_after();
		}else {
			parseExpression();
			mustbe(";");
			print_xml_after();
		}
		
		jackxml::close_node("returnStatement");
		jackxml::close_node("statement");
	}
}

// Handles expression operation
void parseExpression() {
	jackxml::open_node("expression");
    
	parseTerm();
	while ((token == "+") || (token == "-") || (token == "*") || (token == "/") || (token == "&") 
		|| (token == "|") || (token == "<") || (token == ">") || (token == "=")) {
		parseOp();
		parseTerm();
	}
	
	jackxml::close_node("expression");
}

// Handles how terms are passed inside each parse of token when called from other functions
void parseTerm() {
	if ((token == "integerConstant") || (token == "stringConstant") || (token == "(") || (token == "-")
		|| (token == "~") || (token == "true") || (token == "false") || (token == "null") || (token == "this") 
		|| (token == "identifier") || (token =="constructor") || (token == "function") || (token == "method")) {
		if ((have("integerConstant")) || (have("stringConstant"))) {
			jackxml::open_node("term");
			print_xml_after();
			jackxml::close_node("term");
		}else if ((token == "true") || (token == "false") || (token == "null") || (token == "this")) {
			parseKeywordConstant();	
		}else if ((token == "identifier")) {
			jackxml::flush_output();
			string tmpTokenclass = tokenclass;
			string tmpTokenvalue = tokenvalue;
			nextch();
			jackxml::open_node("term");
			if (have("[")) {
				jackxml::open_node("varName");
				jackxml::open_node(tmpTokenclass);
				jackxml::node_text(tmpTokenvalue);
				jackxml::close_node(tmpTokenclass);
				jackxml::close_node("varName");	
				print_xml_after();
				parseExpression();
				mustbe("]");
				print_xml_after();				
			}else if (have(".")) {
				jackxml::open_node("className");
				jackxml::open_node(tmpTokenclass);
				jackxml::node_text(tmpTokenvalue);
				jackxml::close_node(tmpTokenclass);
				jackxml::close_node("className");
				print_xml_after();
				parseSubRoutineName();
				mustbe("(");
				print_xml_after();
				parseExpressionList();
				mustbe(")");
				print_xml_after();
			}else if (have("(")) {
				jackxml::open_node("subroutineName");
				jackxml::open_node(tmpTokenclass);
				jackxml::node_text(tmpTokenvalue);
				jackxml::close_node(tmpTokenclass);
				jackxml::close_node("subroutineName");
				print_xml_after();
				parseExpressionList();
				mustbe(")");
				print_xml_after();				
			}else {
				jackxml::open_node("varName");
				jackxml::open_node(tmpTokenclass);
				jackxml::node_text(tmpTokenvalue);
				jackxml::close_node(tmpTokenclass);
				jackxml::close_node("varName");				
			}
			jackxml::close_node("term");
		}else if (have("(")) {
			jackxml::open_node("term");
			print_xml_after();
			parseExpression();
			mustbe(")");
			print_xml_after();
			jackxml::close_node("term");
		}else if ((token == "-") || (token == "~")) {
			jackxml::open_node("term");
			parseUnaryOp();
			parseTerm();
			jackxml::close_node("term");
		}
	}
}

// Checking of expressions while in statements
void parseExpressionList() {
	jackxml::open_node("expressionList");
    
	if ((token == "integerConstant") || (token == "stringConstant") || (token == "(") || (token == "-")
		|| (token == "~") || (token == "true") || (token == "false") || (token == "null") || (token == "this") 
		|| (token == "identifier") || (token =="constructor") || (token == "function") || (token == "method")) {
		parseExpression();
		while (have(",")) {
			print_xml_after();
			if ((token == "integerConstant") || (token == "stringConstant") || (token == "(") || (token == "-")
				|| (token == "~") || (token == "true") || (token == "false") || (token == "null") || (token == "this") 
				|| (token == "identifier") || (token =="constructor") || (token == "function") || (token == "method")) {
				parseExpression();
			}
		}
	}
	
	jackxml::close_node("expressionList");
}

// Checks for operations
void parseOp() {
	if (have("+") || have("-") || have("*") || have("/") || have("&") 
		|| have("|") || have("<") || have(">") || have("=")) {
		print_xml_after();
	}	
}

// Checks for unaryop
void parseUnaryOp() {
	if (have("-") || have("~")) {
		print_xml_after();
	} 
}

// Checks for keyword
void parseKeywordConstant() {
	if ( have("true") || have("false") || have("null") || have("this") ) {
		jackxml::open_node( "term" );
		print_xml_after();
		jackxml::close_node( "term" );
	}
}

int main(){
	tokeniser = new jacktokens();
	nextch();
	parseProgram();
	jackxml::flush_output();
	mustbe("?");
}
