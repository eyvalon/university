// Student Name: Pongsakorn Surasarang
// Student Number: a1697114
// Description: Translates the main.jack to symbols like form

#include <iostream>
#include <stdlib.h>
#include "jacktokens.h"
#include "jackxml.h"
#include "jacksymbols.h"
#include "myall_Map.h"
#include <string>

using namespace std;

//Declaration of variables
jacktokens *tokeniser = NULL;
string token = "?";
string tokenclass = "?";
string tokenvalue = "?";
string previous_token = "?";
string previous_tokenvalue = "?";

string keyword_string;
string type_string;
string varname_string;
string tokenvalue_tostore;

int counter_token_global = 0;
int counter_token_local = 0;
int counter_token_this = 0;

bool global_add = false;
bool local_add = false;

void parseProgram();
void parseClass();
void parseClassVarDec();
void parseType();
void parseSubRoutineDec();
void parseParameterList();
void parseSubRoutineBody();
void parseVarDec();
void parseClassName();
void parseSubRoutineName();
void parseVarName();
void parseStatements();
void parserStatement();
void parseLetStatement();
void parseIfStatement();
void parseWhileStatement();
void parseDoStatement();
void parseReturnStatement();
void parseExpression();
void parseTerm();
void parseExpressionList();
void parseOp();
void parseUnaryOp();
void parseKeywordConstant();

void add_into_map_global();
void add_into_map_local();

// Declaration of pre-defined map, which are defined in myMap.h file. This gives access to store variables to the blank lookup table.
std::map<std::string, std::string> global (global_values, global_values + global_values_size);
std::map<std::string, std::string> local (local_values, local_values + local_values_size);

// Used for printing out what the symbols are at certain part of the program when called later in the program
void print_xml_after() {
    jackxml::open_node(previous_token); 
    
    if(global.find(previous_tokenvalue) != global.end())
        jackxml::node_text(global.find(previous_tokenvalue)->second);
    else if(local.find(previous_tokenvalue) != local.end())
        jackxml::node_text(local.find(previous_tokenvalue)->second);
    else
        jackxml::node_text(previous_tokenvalue);
    
    jackxml::close_node(previous_token);	
}

// Gets the next tokenvalue
void nextch() {
    token = tokeniser->next_token();
    tokenclass = tokeniser->token_class();
    tokenvalue = tokeniser->token_value();
}

// Function that helps to store into local and global lookup table.
void add_into_map_global(){
    if(global.find(previous_tokenvalue) != global.end())
        exit(0);
    else
        global.insert(std::pair<std::string , std::string> (previous_tokenvalue, tokenvalue_tostore));
}

void add_into_map_local(){
    if(local.find(previous_tokenvalue) != local.end())
        exit(0);
    else
        local.insert(std::pair<std::string , std::string> (previous_tokenvalue, tokenvalue_tostore));
}

// Pre-defined functions that are necessarily for this jack program
void mustbe(string expected) {
    if (expected != token) {
        jackxml::erase_buffer();
        exit(0) ;
    }
    
    previous_token = tokenclass;
    previous_tokenvalue = tokenvalue;
    nextch();
}

bool have(string expected) {
    if (expected != token)
        return false;
    
    previous_token = tokenclass;
    previous_tokenvalue = tokenvalue;	
    nextch();
    return true;
}

// Pre-defined functions that are necessarily for this jack program
void parseProgram() {
    if (have("class")) {
        jackxml::open_node("class");
        parseClass();
        jackxml::close_node("class");
    }
}

void parseClass() {
    print_xml_after();
    parseClassName();
    mustbe("{");
    print_xml_after();
    
    // Gets global variables which is used to store in the global lookup table
    parseClassVarDec();
    
    // Has functions that will be used for calculating the symbols code
    parseSubRoutineDec();
    mustbe("}");
    print_xml_after();
}

// Global variable section, which is used to add into the global lookup table with the correct order of code structure (i.e. aa,int,static,0 )
void parseClassVarDec() {
    while (have("static") || have("field")) {
        jackxml::open_node("classVarDec");
        print_xml_after();    
        
        if(previous_tokenvalue == "static")
            keyword_string = "static";
        else
            keyword_string = "this";
        
        parseType();
        type_string = previous_tokenvalue;
        global_add = true;
        parseVarName();
        global_add = false;
        
        if(keyword_string == "static")
            counter_token_global++;
        else
            counter_token_this++;
        
        // If the global variable consists of more than one
        while (have(",")) {
            print_xml_after();
            global_add = true;
            parseVarName();
            global_add = false;
            
            // Adds a counter for local / global counter respectively to what the tokenvalue is
            if(keyword_string == "static")
                counter_token_global++;
            else
                counter_token_this++;
        }
            
        type_string = "";
        keyword_string = "";
        varname_string = "";
            
        mustbe(";");
        print_xml_after();
        
        jackxml::close_node("classVarDec");
    }
}

// Checks of what type it is, and depending on the type, parse it to the correct function
void parseType() {
    if ((token == "int") || (token == "char") || (token == "boolean") || (token == "identifier")) {
        jackxml::open_node("type");
        
        if (have("int") || have("char") || have("boolean"))
            print_xml_after();
        else if (token == "identifier")
            parseClassName();
        
        jackxml::close_node("type");
    }
}

// For each and every function there exists, check if it is a method, if it does, change counter_token_local value to 1 instead of 0 (which is reset at the end of each loop), where this will be used for
// printing later
void parseSubRoutineDec() {
    while (have("constructor") || have("function") || have("method")) {
        jackxml::open_node("subroutineDec");
        
        // Depending on the function type (function, method, or constructor), do their part respectively where later, it will be used to determine what to do by IF statement
        if(previous_tokenvalue=="constructor"){
            print_xml_after();
            nextch();
        }else{
            if(previous_tokenvalue=="method")
                counter_token_local=1;
            print_xml_after();
            if (have("void"))
                print_xml_after();
            else if ((token == "int") || (token == "char") || (token == "boolean") || (token == "identifier"))
                parseType();
        }
        
        parseSubRoutineName();
        mustbe("(");
        print_xml_after();
        parseParameterList();
        mustbe(")");
        print_xml_after();
        parseSubRoutineBody();
        counter_token_local=0;
        jackxml::close_node("subroutineDec");
        
    }
}

// Checks for parameterList for which is used for printing out how many arguments there are + keeping count of number of arguments
void parseParameterList() {
    jackxml::open_node("parameterList");
    parseType();

    if(previous_tokenvalue!="("){
        keyword_string = "argument";
        type_string = previous_tokenvalue;
        local_add = true;
        parseVarName();
        local_add = false;
    }else
        parseVarName();

    // For arguments that appears more than one
    while (have(",")) {
        counter_token_local++;
        print_xml_after();
        parseType();
        type_string = previous_tokenvalue;
        local_add = true;
        parseVarName();
        local_add = false;
    }

    // Resets the variable to zero so that it doesn't conflict with one another when called later
    type_string = "";
    keyword_string = "";
    varname_string = "";

    jackxml::close_node("parameterList");
}

void parseSubRoutineBody() {
    if (have("{")) {
        jackxml::open_node("subroutineBody");		
        counter_token_local=0;
        print_xml_after();
        parseVarDec();
        parseStatements();
        mustbe("}");
        print_xml_after();
        local.clear();
        jackxml::close_node("subroutineBody");
    }
}

void parseVarDec() {
    while (have("var")) {
        jackxml::open_node("varDec");
        print_xml_after();
        keyword_string = "local";
        parseType();
        type_string = previous_tokenvalue;
        local_add = true;
        parseVarName();
        local_add = false;
        counter_token_local++;
        
        // For arguments that appears more than one
        while (have(",")) {
            print_xml_after();
            local_add = true;
            parseVarName();
            local_add = false;
            counter_token_local++;
        }
        
        // Resets the variable to zero so that it doesn't conflict with one another when called later
        type_string = "";
        keyword_string = "";
        varname_string = "";

        mustbe(";");
        print_xml_after();
        jackxml::close_node("varDec");
    }
}

void parseClassName() {
    if (have("identifier")) {
        jackxml::open_node("className");
        print_xml_after();
        jackxml::close_node("className");		
    }
}

void parseSubRoutineName() {
    if (have("identifier")) {
        jackxml::open_node("subroutineName");
        print_xml_after();
        jackxml::close_node("subroutineName");		
    }
}

void parseVarName() {
    if (have("identifier")) {
        jackxml::open_node("varName");
        varname_string = previous_tokenvalue;
        
        // Helps to add to the lookup table - Global / Local. This is determined by seeing whether the tokenvalue is static / field as well as whether global_add or
        // local_add is called where this determines to see if it needs to add to the global / local lookup.
        if(global_add){
            if(keyword_string == "static")
                tokenvalue_tostore = varname_string + "," + type_string + "," + keyword_string + "," + to_string(counter_token_global);
            else
                tokenvalue_tostore = varname_string + "," + type_string + "," + keyword_string + "," + to_string(counter_token_this);
            add_into_map_global();
        }
        
        if(local_add && previous_tokenvalue!="("){
            tokenvalue_tostore = varname_string + "," + type_string + "," + keyword_string + "," + to_string(counter_token_local);
            add_into_map_local();
        }

        print_xml_after();
        jackxml::close_node("varName");
    }
}

// Type of statements that is needed for functions (let, if, while, do and return)
void parseStatements() {
    jackxml::open_node("statements");
    parserStatement();
    jackxml::close_node("statements");
}

void parserStatement() {
    while ((token == "let") || (token == "if") || (token == "while") || (token == "do") || (token == "return")) {
        parseLetStatement();
        parseIfStatement();
        parseWhileStatement();
        parseDoStatement();
        parseReturnStatement();	
    }	
}

void parseLetStatement() {
    while (have("let")) {
        jackxml::open_node("statement");
        jackxml::open_node("letStatement");	
        print_xml_after();
        parseVarName();
        
        if (have("[")) {
            print_xml_after();
            parseExpression();
            mustbe("]");
            print_xml_after();
        }
        
        mustbe("=");
        print_xml_after();
        parseExpression();
        mustbe(";");
        print_xml_after();
        jackxml::close_node("letStatement");
        jackxml::close_node("statement");				
    }
}

void parseIfStatement() {
    while (have("if")) {
        jackxml::open_node("statement");		
        jackxml::open_node("ifStatement");	
        print_xml_after();
        mustbe("(");
        print_xml_after();
        parseExpression();	
        mustbe(")");
        print_xml_after();
        mustbe("{");
        print_xml_after();
        parseStatements();
        mustbe("}");
        print_xml_after();
        
        if (have("else")) {
            print_xml_after();
            mustbe("{");
            print_xml_after();
            parseStatements();
            mustbe("}");
            print_xml_after();			
        }
        
        jackxml::close_node("ifStatement");
        jackxml::close_node("statement");			
    }
}

void parseWhileStatement() {
    while (have("while")) {
        jackxml::open_node("statement");	
        jackxml::open_node("whileStatement");
        print_xml_after();
        mustbe("(");
        print_xml_after();
        parseExpression();
        mustbe(")");
        print_xml_after();
        mustbe("{");
        print_xml_after();
        parseStatements();
        mustbe("}");
        print_xml_after();
        jackxml::close_node("whileStatement");
        jackxml::close_node("statement");
    }
}

void parseDoStatement() {
    while (have("do")) {
        jackxml::open_node("statement");	
        jackxml::open_node("doStatement");
        print_xml_after();
        
        if (token == "identifier") {
            string tmpTokenclass = tokenclass;
            string tmpTokenvalue = tokenvalue;	
            nextch();
            
            if (have(".")) {
                
                if(global.find(tmpTokenvalue) != global.end()){
                    jackxml::open_node("varName");
                    jackxml::open_node(tmpTokenclass);
                    jackxml::node_text(global.find(tmpTokenvalue)->second);
                    jackxml::close_node(tmpTokenclass);
                    jackxml::close_node("varName");
                }else if(local.find(tmpTokenvalue) != local.end()){
                    jackxml::open_node("varName");
                    jackxml::open_node(tmpTokenclass);
                    jackxml::node_text(local.find(tmpTokenvalue)->second);
                    jackxml::close_node(tmpTokenclass);
                    jackxml::close_node("varName");
                }else{
                    jackxml::open_node("className");
                    jackxml::open_node(tmpTokenclass);
                    jackxml::node_text(tmpTokenvalue);
                    jackxml::close_node(tmpTokenclass);
                    jackxml::close_node("className");	
                }
                print_xml_after();		
                
            }else {
                jackxml::open_node("subroutineName");
                jackxml::open_node(tmpTokenclass);
                jackxml::node_text(tmpTokenvalue);
                jackxml::close_node(tmpTokenclass);
                jackxml::close_node("subroutineName");
            }
        }
        
        parseSubRoutineName();
        mustbe("(");
        print_xml_after();
        parseExpressionList();
        mustbe(")");
        print_xml_after();
        mustbe(";");
        print_xml_after();
        jackxml::close_node("doStatement");
        jackxml::close_node("statement");			
    }
}

void parseReturnStatement() {
    if (have("return")) {
        jackxml::open_node("statement");	
        jackxml::open_node("returnStatement");
        print_xml_after();
                
        if (have(";"))
            print_xml_after();
        else {
            parseExpression();
            mustbe(";");
            print_xml_after();
        }
        
        jackxml::close_node("returnStatement");
        jackxml::close_node("statement");
    }
}

// Handles expression operation
void parseExpression() {
    jackxml::open_node("expression");

    parseTerm();
    while ((token == "+") || (token == "-") || (token == "*") || (token == "/") || (token == "&") 
        || (token == "|") || (token == "<") || (token == ">") || (token == "=")) {
        parseOp();
        parseTerm();
    }

    jackxml::close_node("expression");
}

// Handles how terms are passed inside each parse of token when called from other functions
void parseTerm() {
    if ((token == "integerConstant") || (token == "stringConstant") || (token == "(") || (token == "-")
        || (token == "~") || (token == "true") || (token == "false") || (token == "null") || (token == "this") 
        || (token == "identifier") || (token =="constructor") || (token == "function") || (token == "method")) {
        
        if ((have("integerConstant")) || (have("stringConstant"))) {
            jackxml::open_node("term");
            print_xml_after();
            jackxml::close_node("term");
        }else if ((token == "true") || (token == "false") || (token == "null") || (token == "this"))
            parseKeywordConstant();	
        else if ((token == "identifier")) {
            jackxml::flush_output();
            string tmpTokenclass = tokenclass;
            string tmpTokenvalue = tokenvalue;
            nextch();
            jackxml::open_node("term");
            
            // Checks for if tokenvalue contains " '[' / '.' / '(' "
            if (have("[")) {
                jackxml::open_node("varName");
                jackxml::open_node(tmpTokenclass);

                if(global.find(tmpTokenvalue) != global.end())
                    jackxml::node_text(global.find(tmpTokenvalue)->second);
                else if(local.find(tmpTokenvalue) != local.end())
                    jackxml::node_text(local.find(tmpTokenvalue)->second);
                else
                    jackxml::node_text(tmpTokenvalue);
                
                jackxml::close_node(tmpTokenclass);
                jackxml::close_node("varName");	
                print_xml_after();
                parseExpression();
                mustbe("]");
                print_xml_after();	
                
            }else if (have(".")) {

                if(global.find(tmpTokenvalue) != global.end()){
                    jackxml::open_node("varName");
                    jackxml::open_node(tmpTokenclass);
                    jackxml::node_text(global.find(tmpTokenvalue)->second);
                    jackxml::close_node(tmpTokenclass);
                    jackxml::close_node("varName");
                }else if(local.find(tmpTokenvalue) != local.end()){
                    jackxml::open_node("varName");
                    jackxml::open_node(tmpTokenclass);
                    jackxml::node_text(local.find(tmpTokenvalue)->second);
                    jackxml::close_node(tmpTokenclass);
                    jackxml::close_node("varName");
                }else{
                    jackxml::open_node("className");
                    jackxml::open_node(tmpTokenclass);
                    jackxml::node_text(tmpTokenvalue);
                    jackxml::close_node(tmpTokenclass);
                    jackxml::close_node("className");
                }
                    
                print_xml_after();
                parseSubRoutineName();
                mustbe("(");
                print_xml_after();
                parseExpressionList();
                mustbe(")");
                print_xml_after();
                
            }else if (have("(")) {
                jackxml::open_node("subroutineName");
                jackxml::open_node(tmpTokenclass);
                jackxml::node_text(tmpTokenvalue);
                jackxml::close_node(tmpTokenclass);
                jackxml::close_node("subroutineName");
                print_xml_after();
                parseExpressionList();
                mustbe(")");
                print_xml_after();
            }else {
                jackxml::open_node("varName");                
                jackxml::open_node(tmpTokenclass);

                if(global.find(tmpTokenvalue) != global.end())
                    jackxml::node_text(global.find(tmpTokenvalue)->second);
                else if(local.find(tmpTokenvalue) != local.end())
                    jackxml::node_text(local.find(tmpTokenvalue)->second);
                else
                    jackxml::node_text(tmpTokenvalue);
                
                jackxml::close_node(tmpTokenclass);
                jackxml::close_node("varName");				
            }
            jackxml::close_node("term");
            
        // Checks for tokenvalue that contains "("
        }else if (have("(")) {
            jackxml::open_node("term");
            print_xml_after();
            parseExpression();
            mustbe(")");
            print_xml_after();
            jackxml::close_node("term");
            
        // Checks for tokenvalue that contains " '-' / '~' "
        }else if ((token == "-") || (token == "~")) {
            jackxml::open_node("term");
            parseUnaryOp();
            parseTerm();
            jackxml::close_node("term");
        }
    }
}

// Checking of expressions while in statements
void parseExpressionList() {
    jackxml::open_node("expressionList");

    if ((token == "integerConstant") || (token == "stringConstant") || (token == "(") || (token == "-")
        || (token == "~") || (token == "true") || (token == "false") || (token == "null") || (token == "this") 
        || (token == "identifier") || (token =="constructor") || (token == "function") || (token == "method")) {
        parseExpression();
    
        // For arguments that appears more than one
        while (have(",")) {
            print_xml_after();
            if ((token == "integerConstant") || (token == "stringConstant") || (token == "(") || (token == "-")
                || (token == "~") || (token == "true") || (token == "false") || (token == "null") || (token == "this") 
                || (token == "identifier") || (token =="constructor") || (token == "function") || (token == "method")) {
                parseExpression();
            }
        }
    }

    jackxml::close_node("expressionList");
}

// Checks for operations
void parseOp() {
    if (have("+") || have("-") || have("*") || have("/") || have("&") 
        || have("|") || have("<") || have(">") || have("=")) {
        print_xml_after();
    }	
}

// Checks for unaryop
void parseUnaryOp() {
    if (have("-") || have("~")) {
        print_xml_after();
    } 
}

// Checks for keyword
void parseKeywordConstant() {
    if ( have("true") || have("false") || have("null") || have("this") ) {
        jackxml::open_node( "term" );
        print_xml_after();
        jackxml::close_node( "term" );
    }
}

int main(){
    tokeniser = new jacktokens();
    nextch();
    parseProgram();
    jackxml::flush_output();
    mustbe("?");
}
