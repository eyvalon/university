// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/01/Mux8Way16.hdl

/**
 * 8-way 16-bit multiplexor:
 * out = a if sel == 000
 *       b if sel == 001
 *       etc.
 *       h if sel == 111
 */

CHIP Mux8Way16 {
    IN a[16], b[16], c[16], d[16],
       e[16], f[16], g[16], h[16],
       sel[3];
    OUT out[16];

    PARTS:
    // Put your code here:
    
    // We can use Mux16 for this, the same way as we did for Mux4Way16
    
    // We first want to group a,b as one output and c,d as another output
    Mux16(a=a, b=b, sel=sel[0], out=s1);
    Mux16(a=c, b=d, sel=sel[0], out=s2);
    
    // After we have the previous 2 outputs from sel[0], we do Mux16 using sel[1]
    Mux16(a=s1, b=s2, sel=sel[1], out=s3);
    
    // We then make another output from e,f and g,h
    Mux16(a=e, b=f, sel=sel[0], out=s4);
    Mux16(a=g, b=h, sel=sel[0], out=s5);
    
    // After we another 2 output from sel[0], we make another Mux16 using sel[1]
    Mux16(a=s4, b=s5, sel=sel[1], out=s6);
    
    // Finally we Mux16 using the 2 sel[1] that we have with sel[2]
    Mux16(a=s3, b=s6, sel=sel[2], out=out);
}
