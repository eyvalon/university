// Student Name: Pongsakorn Surasarang
// Student Number: a1697114

#include <string>
#include <sstream>
#include <bitset>

// Declaration of variables.
std::string binary;
int n;
int c;
int k;

// Converts a given number, to binary format using the least bit significant.
std::string getBinary(int x){
    binary = "";
    for (c = 15; c > -1; c--){
        k = x >> c;
        if (k & 1)
            binary = binary + "1";
        else
            binary = binary + "0";
    }
    
    return binary;
}

// Converts from Hex to binary format from given string. The given string then gets converted to int by stringstream, and then it calculates using bitset and then gets converted back
// to string before finally returning.
std::string getHex(std::string y){
    std::stringstream sss;
    sss << std::hex << y;
    sss >> n;
    std::bitset<16> b(n);  
    std::string hex_value = b.to_string<char,std::string::traits_type,std::string::allocator_type>();
    
    return hex_value;
}

// Pre-defined what R0-R15 stores so that 0-15 can just grab the values below and add it to the end of the string. This is used to get from the lookup table of Symbol and this 
// array index will thn get the last bit of the end. From that, it is used to calculate the Hex values when putting this at the end of the Symbol lookup 'R' so the final string before 
// calculating for Hex would be 0x000 + Rs[index]
std::string Rs[16] = {
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "a",
    "b",
    "c",
    "d",
    "e",
    "f"
};

// Checks for whether the given string contains only number or not. If it does, return True otherwise False.
bool is_digit(std::string str){
    return (str.find_first_not_of( "0123456789" ) == std::string::npos);
};
