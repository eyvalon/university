// Student Name: Pongsakorn Surasarang
// Student Number: a1697114

// convert Hack assembly into binary
#include <map>
#include <sstream>
#include <iostream>
#include "tokens.h"
#include "myMap.h"

// to simplify the code
using namespace std ;

// Reads both Hack assembly file and input lines.
int main(){
    // Declaration of pre-defined maps - comp, dest, jump which are defined in myMap.h file.
    std::map<std::string, std::string> jump (jump_values, jump_values + jump_values_size);
    std::map<std::string, std::string> dest (dest_values, dest_values + dest_values_size);
    std::map<std::string, std::string> comp (comp_values, comp_values + comp_values_size);
    std::map<std::string, std::string> symbol (symbol_values, symbol_values + symbol_values_size);
    
    // Using externs.    
    extern std::string getBinary(int x);

	// create a new assembler tokeniser then read the first token
	tokens *tokeniser = new tokens(getchar) ;
	std::string token = tokeniser->next_token() ;

	// a token counters
	int tokens = 0 ;
    
    //Declaration of variables
    int countx;
    int store_value;

    std::string input_value;
    std::string input_token;
    
    while ( token != "?" )
    {
        // Helps to print a new line when it is only an aluop.
        if(countx==1)
            cout << endl;
        
        // Checks and print a new line when tokens is more than 1 so that only the second line onwards will
        // be a new line instead of having all new lines being on the new line.
        countx=0;
        if(tokens>0)
            cout << endl;
        
        // Define what input_value and input_token would be at the start of every input values. This is to ensure 
        // that they do not overlap with each other.
        input_value = "";
        input_token = "";
        
        // Checks for address value that has a number.
        if(token == "address"){
            std::istringstream ss(tokeniser->token_value());
            ss >> store_value;
            std::cout << getBinary(store_value);
        }

        // If token returns as dest-comp?, comp, null or dest, get the value from the lookup table and print out using std::cout.
        // While doing it, check whether the next value is a "semi" or "equals" and then printing out corresponding to the token.
        if(token == "dest-comp?" || token == "comp" || token == "null" || token == "dest"){
            input_value = tokeniser->token_value();
            input_token = token;
            token = tokeniser->next_token() ;
            
            if(token == "equals"){
                token = tokeniser->next_token() ;
                
                if(input_token == "null") {
                        std::cout << comp.find(tokeniser->token_value())->second;
                        std::cout << dest.find("null")->second;
                        std::cout << jump.find("null")->second;
                
                }else if(input_token == "dest" || input_token == "dest-comp?"){
                    if (token == "comp" || token == "dest-comp?"){
                        std::cout << comp.find(tokeniser->token_value())->second;
                        std::cout << dest.find(input_value)->second;
                        std::cout << jump.find("null")->second;
                    }
                }
            }else if(token == "semi") {
                std::cout << comp.find(input_value)->second;
                std::cout << dest.find("null")->second;    
                token = tokeniser->next_token() ;
                
                if (token == "jump")
                    std::cout << jump.find(tokeniser->token_value())->second;
                else if(token == "null")
                    std::cout <<jump.find(token)->second;
            
            }else if (token != "equal" || token != "semi"){
                std::cout << comp.find(input_value)->second;
                std::cout << dest.find("null")->second;  
                std::cout << jump.find("null")->second;
               
                if (token!= "semi" || token!="equals"){
                    countx=1;
                    continue;
                }
            }
        }
        // Add one to total, used to get the total number of tokens.
        tokens++ ;

        // read the next token
        token = tokeniser->next_token() ;
    }
    
    cout << endl;
    return 0 ;
}
