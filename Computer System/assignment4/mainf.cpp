// Student Name: Pongsakorn Surasarang
// Student Number: a1697114
//  Description: Convert Hack assembly into binary

#include <vector>
#include <sstream>
#include <iostream>
#include "tokens.h"
#include "myMap.h"

// To simplify the code
using namespace std;

// Reads both Hack assembly file and input lines.
int main(){
    // Declaration of pre-defined maps - comp, dest, jump which are defined in myMap.h file.
    std::map<std::string, std::string> jump (jump_values, jump_values + jump_values_size);
    std::map<std::string, std::string> dest (dest_values, dest_values + dest_values_size);
    std::map<std::string, std::string> comp (comp_values, comp_values + comp_values_size);
    std::map<std::string, std::string> symbol (symbol_values, symbol_values + symbol_values_size);
    
    // Using externs.
    extern std::string Rs[16];
    extern std::string getBinary(int x);
    extern std::string getHex(string y);
    extern bool is_digit(std::string str);

    // create a new assembler tokeniser then read the first token.
    tokens *tokeniser = new tokens(getchar);
    std::string token=tokeniser -> next_token();

    // Token counters.
    int tokens = 0;
    int countx = 0;
    int countt = 0;
    
    //Declaration of variables.
    int store_value;
    int startx = 16;
    int skip=0;
    
    std::string binary;
    std::string input_value;
    std::string input_token;
    std::string address_value;

    // Declaring vector to store the First Pass: tokens and values; so that it can perform just like calling token but with vector index.
    vector<std::string> value_list;
    vector<std::string> token_list;
    vector<std::string> final_list;
    
    // First Pass.
    while ( token != "?" ){       
        // Reset variables to be None at the start of the loop so that it doesn't conflict with one another.
        input_value = "";
        input_token = "";
        int skipped = 0;
        
        // If token returns as label, find line number, and then adding that value to the map symbol lookup table.
        if(token == "label"){
            if(symbol.find(tokeniser -> token_value()) != symbol.end())
                break;
            else
                symbol.insert(std::pair<std::string , std::string> (tokeniser -> token_value(), getBinary(tokens)));
        }
        
        // If token returns as address, store the token value as well as the token that it holds.
        if(token == "address"){
            value_list.push_back(tokeniser -> token_value());
            token_list.push_back(token);
            tokens++;
        }
        
        // If token returns as dest-comp?, comp, null or dest, store the token value as well as the token that it holds.
        // Run through the loop and see what other values it gets inside and store the token value and token to the list.
        if(token == "dest-comp?" || token == "comp" || token == "dest"){
            if(skipped == 0){
                input_value=tokeniser -> token_value();
                input_token=token;
                
                if(input_token=="null"){
                    value_list.push_back(token);
                    token_list.push_back(token);  
                    
                }else{
                    value_list.push_back(tokeniser -> token_value());
                    token_list.push_back(token);    
                }
                token = tokeniser -> next_token();
                
            }else if(skipped == 1){
                value_list.push_back(tokeniser -> token_value());
                token_list.push_back(token);
                skipped = 0;
                tokens++;
            }
            
            if(token == "dest-comp?" || token == "comp" || token == "dest")
                skipped = 1;
                        
            if(skipped == 0){
                if(token == "equals"){
                    value_list.push_back(token);
                    token_list.push_back(token);
                    token = tokeniser -> next_token();
                    
                    if (token == "comp" || token == "dest-comp?"){
                        value_list.push_back(tokeniser -> token_value());
                        token_list.push_back(input_token);  
                    }    
                    tokens++;
                }else if(token == "semi") {
                    value_list.push_back(token);
                    token_list.push_back(token); 
                    token = tokeniser -> next_token();
                    
                    if (token == "jump"){
                        value_list.push_back(tokeniser -> token_value());
                        token_list.push_back(token);  
                    }else{
                        value_list.push_back("null");
                        token_list.push_back("null"); 
                    }
                    
                    tokens++;
                }
            }
        }
        
        if(token == "semi") {
            value_list.push_back(token);
            token_list.push_back(token); 
            token = tokeniser -> next_token();
            
            if (token == "jump"){
                value_list.push_back(tokeniser -> token_value());
                token_list.push_back(token);  
            }else{
                value_list.push_back("null");
                token_list.push_back("null"); 
            }
            
            if(token_list[token_list.size()-4] != "equals")
                tokens++;
        }
        
        // Read the next token if skipped=0.
        if(skipped==0)
            token=tokeniser -> next_token();
    }
    
    // Sets the end of the vector to be "?" where this helps to identify when the last input is, it also helps to identify when to break in Second Pass so that it doesn't give segmentation fault.
    token_list.push_back("?");
    
    // Second Pass
    countx = 0;

    while(countt < value_list.size()){
    
        // Define an empty input_value and input_token string at the start of every input values. This is to ensure that they do not overlap with each other.
        // Create a string binary that gets the resulting binary from IF statements, used for printing after the IF statements.
        std::string binary = "";
        input_value = "";
        input_token = "";
        countx=0;
        
        // Checks for address value that has a number or a symbol.
        // If address contains "R" in front, check against the lookup and compare the value in front of "R" to see if it is a value. If it is, lookup in the symbol table for that value.
        // If the address with "R" in front doesn't register in the symbol table, do a getHex function.
        // For any address that doesn't start with "R" or a number, either do a lookup or add new value to symbol lookup table + print.
        if(token_list[countt] == "address"){
            input_value=value_list[countt];
                        
            // If input address is a number.
            if(is_digit(input_value) == 1){
                std::istringstream ss(input_value);
                ss >> store_value;
                binary += getBinary(store_value);
                
            // If first string character is an "R".
            }else if(input_value[0] == 'R'){
                input_value=input_value.substr(1, input_value.size());
                
                if(is_digit(input_value) == 1){
                    std::istringstream ss(input_value);
                    ss >> store_value;
                    
                    if(store_value>=0 && store_value<=15){
                        address_value=symbol.find("R")->second;
                        address_value=address_value.substr(0, address_value.size()-input_value.size());
                        address_value=address_value + Rs[store_value];
                        binary += getHex(address_value);
                    }
                }else{
                    input_value = "R" + input_value;
                    if(symbol.find(input_value) != symbol.end())
                        binary += symbol.find(input_value)->second;
                    else{
                        binary += getBinary(startx);
                        startx++;
                    }
                }
                    
            // If first string character is not an "R".
            }else if(input_value[0] != 'R'){
                if(symbol.find(input_value) != symbol.end()){
                    string temp=symbol.find(input_value)->second;
                    
                    if (temp.length() < 10){
                        binary += getHex(symbol.find(input_value)->second);
                    }else
                        binary += temp;

                }else{
                    binary += getBinary(startx);
                    symbol.insert(std::pair<std::string , std::string> (input_value, getBinary(startx)));
                    startx++;
                }
            }
        }

        // Pass the token to this IF statement if input contains dest-comp, comp, null, or dest.
        // Checks for how to handle printing at certain part of the token if contains semi, equals or neither.
        // There are 3 IF statements that handles if it is multiple aluops, contains a "semi" and finally, an "equal".
        if(token_list[countt] == "dest-comp?" || token_list[countt] == "comp" || token_list[countt] == "null" || token_list[countt] == "dest"){
            input_value=value_list[countt];
            input_token=token_list[countt];
            
            // This checks to see whether an input contains multiple consecutive aluop or not.
            if(token_list[countt+1] == "comp" || token_list[countt+1] == "dest-comp?" || token_list[countt+1] == "?")
                if (token_list[countt] == "comp" || token_list[countt] == "dest-comp?"){
                    binary += comp.find(input_value)->second;
                    binary += dest.find("null")->second;  
                    binary += jump.find("null")->second;
                    countx=1;
                }
            
            if(countx == 0){
                // While the next input doesn't contain "?", check for binary output in each IF statements and store to the string variable, binary.
                if(token_list[countt+1] != "?")
                    countt++;
                
                if(token_list[countt] == "equals"){
                    countt++;
                    if(input_token == "null") {
                        binary += comp.find(value_list[countt])->second;
                        binary += dest.find("null")->second;
                        
                    }else if(input_token == "dest" || input_token == "dest-comp?"){
                        if (token_list[countt] == "comp" || token_list[countt] == "dest-comp?"){
                            binary += comp.find(value_list[countt])->second;
                            binary += dest.find(input_value)->second;
                        }else if (token_list[countt] == "dest"){
                            binary += comp.find(value_list[countt])->second;
                            binary += dest.find(input_value)->second;
                        }
                        
                        if(token_list[countt+1] == "semi") {
                            countt=countt+2;

                                if (token_list[countt] == "jump")
                                    binary += jump.find(value_list[countt])->second;
                                else if(token_list[countt] == "null")
                                    binary += jump.find("null")->second;

                        }else
                            binary += jump.find("null")->second;
                    }
                // Check for what to print out if input contains a semi token.
                }else if(token_list[countt] == "semi") {
                    
                    binary += comp.find(input_value)->second;
                    binary += dest.find("null")->second;    
                    countt++;

                    if (token_list[countt] == "jump")
                        binary += jump.find(value_list[countt])->second;
                    else if(token_list[countt] == "null")
                        binary += jump.find(token_list[countt])->second;
                }
            }
        }
        
        // Read the next token and store the binary to the vector "final_list".
        countt++;
        
        if(binary !=  "" && binary.length() == 16)
            final_list.push_back(binary);
    }
    
    // Only print if the number of tokens input, is equal to the number of lines that assembly was able to get passed.
    if(final_list.size() == tokens)
        for(int zz=0; zz<final_list.size(); zz++)
            std::cout << final_list[zz] << std::endl;

    return 0;
}
