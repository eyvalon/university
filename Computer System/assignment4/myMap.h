// Student Name: Pongsakorn Surasarang
// Student Number: a1697114

#ifndef MYMAP_H
#define MYMAP_H
#include <map>
#include <sstream>
#include <bitset>
#include <stdio.h>

// Declaration of map variables - comp, dest, jump.
typedef std::pair<std::string, std::string> jump_map;
typedef std::pair<std::string, std::string> dest_map;
typedef std::pair<std::string, std::string> comp_map;
typedef std::pair<std::string, std::string> symbol_map;

// Jump lookup table pre-defined.
const jump_map jump_values[] = {
    jump_map("null", "000"),
    jump_map("JGT", "001"),
    jump_map("JEQ", "010"),
    jump_map("JGE", "011"),
    jump_map("JLT", "100"),
    jump_map("JNE", "101"),
    jump_map("JLE", "110"),
    jump_map("JMP", "111")
};

// Destination lookup table pre-defined.
const dest_map dest_values[] = {
    dest_map("null", "000"),
    dest_map("M", "001"),
    dest_map("D", "010"),
    dest_map("MD", "011"),
    dest_map("A", "100"),
    dest_map("AM", "101"),
    dest_map("AD", "110"),
    dest_map("AMD", "111")
};

// Comp lookup table pre-defined when a=0 and a=1.
const comp_map comp_values[] = {
    comp_map("0", "1110101010"),
    comp_map("1", "1110111111"),
    comp_map("-1", "1110111010"),
    comp_map("D", "1110001100"),
    comp_map("A", "1110110000"),
    comp_map("!D", "1110001101"),
    comp_map("!A", "1110110001"),
    comp_map("-D", "1110001111"),
    comp_map("-A", "1110110011"),
    comp_map("D+1", "1110011111"),
    comp_map("A+1", "1110110111"),
    comp_map("D-1", "1110001110"),
    comp_map("A-1", "1110110010"),
    comp_map("D+A", "1110000010"),
    comp_map("D-A", "1110010011"),
    comp_map("A-D", "1110000111"),
    comp_map("D&A", "1110000000"),
    comp_map("D|A", "1110010101"),

    comp_map("M", "1111110000"),
    comp_map("!M", "1111110001"),
    comp_map("-M", "1111110011"),
    comp_map("M+1", "1111110111"),
    comp_map("M-1", "1111110010"),
    comp_map("D+M", "1111000010"),
    comp_map("D-M", "1111010011"),
    comp_map("M-D", "1111000111"),
    comp_map("D&M", "1111000000"),
    comp_map("D|M", "1111010101")
};

// Symbol Lookup table.
const symbol_map symbol_values[] = {
    symbol_map("SP", " 0x0000"),
    symbol_map("LCL", "0x0001"),
    symbol_map("ARG", " 0x0002"),
    symbol_map("THIS", "0x0003"),
    symbol_map("THAT", " 0x0004"),
    symbol_map("R", "0x0000"),
    symbol_map("SCREEN", "0x4000"),
    symbol_map("KBD", " 0x6000")
};

// Declaration for map size, used for getting the size of map.
const int jump_values_size = sizeof(jump_values) / sizeof(jump_values[0]);
const int dest_values_size = sizeof(dest_values) / sizeof(dest_values[0]);
const int comp_values_size = sizeof(comp_values) / sizeof(comp_values[0]);
const int symbol_values_size = sizeof(symbol_values) / sizeof(symbol_values[0]);

// Declaration to be passed to extern.
extern std::map <std::string, std::string> jump;
extern std::map <std::string, std::string> dest;
extern std::map <std::string, std::string> comp;
extern std::map <std::string, std::string> symbol;

#endif //MYMAP_H
