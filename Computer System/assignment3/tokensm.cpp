// a skeleton of the tokens class implementation
#include <iostream>
#include "tokensm.h"

using namespace std ;

// create a new tokeniser - use readchar function to read the input
// readchar function returns -1 on error or end of input
tokensm::tokensm(int(*_readchar)(void))
{
	// save pointer to the function
	readchar = _readchar ;

	// token value initially ""
	value = "" ;

	// read first character to initialise the tokeniser
	ch = readchar() ;
}

// read next character in the input
void tokensm::nextch()
{
	ch = readchar() ;
}

// return the current token value
string tokensm::token_value()
{
	return value ;
}

// return the next token - "?" on error or end of input
string tokensm::next_token()
{
    // Declaration of variables
    string x;
    string y;
    string num="";
    string original;
    int length;

    while (ch != -1){
        
        // C-component Function that checks with operators, digit, and letters
        if((ch == '-') || (ch == '+') || (ch == '!') || (ch == '&') || (ch == '|') || isalpha(ch) || isdigit(ch) || ch == '$' || ch == '_' || ch == ':' || ch == '.')
        {
            while((ch == '-') || (ch == '+') || (ch == '!') || (ch == '&') || (ch == '|') || isalpha(ch) || isdigit(ch) || ch == '$' || ch == '_' || ch == ':' || ch == '.')
            {
                num += ch;
                ch = readchar();
            }
            
            value = num;
            return "C-component";
        }
        
        // C-component Function that checks for the address, digit and number as well as letters and name
        if (isdigit(ch) || ch == '@' || isalpha(ch) || isdigit(ch) || ch == '$' || ch == '_' || ch == ':' || ch == '.'){
            // This checks for address
            if(ch == '@'){
                while (isdigit(ch) || ch == '@' || isalpha(ch) || isdigit(ch) || ch == '$' || ch == '_' || ch == ':' || ch == '.'){
                    num += ch;
                    ch = readchar();
                }
                
                // This removes the @ at the start, so that when it returns the value, it will be displayed without @
                x = num;
                if(x.length() >= 1){
                    for(int k=1; k<x.length(); k++){
                        y+=x[k];
                    }
                    
                value=y;
                return "address";
                }
                
            // This checks for digit, which will be returned as C-component
            }else if(isdigit(ch)){
                while (isdigit(ch)){
                    num += ch;
                    ch = readchar();
                }
                
                value = num;
                return "C-component";

            }else if( isalpha(ch) || isdigit(ch) || ch == '$' || ch == '_' || ch == ':' || ch == '.' ){
                
                while( isalpha(ch) || isdigit(ch) || ch == '$' || ch == '_' || ch == ':' || ch == '.' ){
                    num += ch;
                    ch = readchar();
                }
                    
                value = num;
                if((value.length()==1))
                    return "C-component";
                }
        }
        
        // Check for label
        if( ch == '(' || ch == ')' ){
            while( ch == '(' || isalpha(ch) || isdigit(ch) || ch == '$' || ch == '_' || ch == ':' || ch == '.' || ch == ')' ){
                num += ch;
                ch = readchar();
            }    
            
            x = num;
            length = x.length();
            if(x.length() >= 4){
                for( int k=1; k<x.length(); k++ ){
                    if(x[k] != ')')
                        y+=x[k];
                    original+=x[k];
                }
                
                // Compare with the original value with the length to see that, if the end of string, contains ')', it will be a label
                value=y;
                if( original[length-2] == ')')
                    return "label";
            }
        }
        
        // Check for semi
        if((ch == ';'))
        {
            value = ch;
            ch = readchar();
            return "semi";
        }
        
        // Check for equal 
        if((ch == '='))
        {
            value = ch;
            ch = readchar();
            return "equals";
        }

        ch = readchar();
    }
    return "?";
}

