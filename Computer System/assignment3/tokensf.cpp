// a skeleton of the tokens class implementation

#include <iostream>
#include <cctype>
#include "tokensf.h"

using namespace std ;

// create a new tokeniser - use readchar function to read the input
// readchar function returns -1 on error or end of input
tokensf::tokensf(int(*_readchar)(void))
{
	// save pointer to the function
	readchar = _readchar ;

	// token value initially ""
	value = "" ;

	// read first character to initialise the tokeniser
	ch = readchar() ;
}

// read next character in the input
void tokensf::nextch()
{
	ch = readchar() ;
}

// return the current token value
string tokensf::token_value()
{
	return value ;
}

// return the next token - "?" on error or end of input
string tokensf::next_token()
{
    // Declaration of variables
    string num="";
    string x;
    string y;
    string original;
    string input_line;
    int length;
    int total;
    int total1;
    int total2;
    int count;
    
    while (ch != -1){
        
        // make the first character a upper case
        // define total, total1 and count to 0 so that it will be used for ignoring comments section later
        ch = toupper((char)ch);    
        total=0;
        total1=0;
        count=0;
        
        // Check for semi
        if((ch == ';')){
            value = ch;
            ch = readchar();
            return "semi";
        }
        
        // Check for equal 
        if((ch == '=')){
            value = ch;
            ch = readchar();
            return "equals";
        }
        
        // This is using ascii values to check for characters that are no listed in the token. If it doesn't match any of the token characters, it will return '?' causing it to stop the program
        if ( int(ch) < 10 || int(ch) > 10 && int(ch) < 32 || int(ch) > 33 && int(ch) < 36 || int(ch) == 37 || int(ch) > 38 && int(ch) < 40 || int(ch) == 44 || int(ch) == 60 || int(ch) == 62 || int(ch) == 63 || int(ch) > 90 && int(ch) < 97 || int(ch) == 123 || int(ch) > 124)
            return "?";
        
        // An IF statement that ignores the comments part. 
        // First it checks to see if it has '/', if it does, it will go into an IF statement checking for '*'. While checking for '*', it will use total1 as a temporary store value that acts as a bool to see that if it finds '*' then '/', it should break from the while loop, otherwise, keep on iterating.
        if ( ch == '/'  ){
            while ( total < 2 ){
                if ( ch == '*' ){
                    total1 = 1;
                    ch = readchar();
                    while(total1 == 1){
                        if ( ch== '/' ){
                            total++;
                        }else if ( ch!= '/' ){
                            total1=0;
                            ch = readchar();
                        }
                        break;
                    }
                }else if ( ch != '*' )
                    total1 = 0;
                
                if (ch == '/' || count == 0 || count == 1){
                    if (ch == '/' && count == 0 )
                        total++;
                }
            
                // This line is used to not make the program hangs
                if( total == 2 )
                    getline(cin, input_line);

                ch = readchar();
            }
            
        // For parts that doesn't start with '/', calculate what the token would be - Null, A|M|D, jump, address and label
        }else if ( ch != '/'  ){
            if(int(ch) == 10 || int(ch) == 32 || int(ch) == 33 || int(ch) == 36 || int(ch) == 38 || int(ch) >= 40 && int(ch) <= 43 || int(ch) >= 45 && int(ch) <= 59 || int(ch) == 61 || int(ch) >= 64 && int(ch) <= 90 || int(ch) >= 97 && int(ch) <= 122 || int(ch) == 124){
                
                // Checks for Null
                if((ch == 'N' || ch == 'U' || ch == 'L')){
                    while (isalpha(ch)){
                        ch = toupper((char)ch);  

                        if((ch == 'N' || ch == 'U' || ch == 'L')){
                            num += ch;
                            ch = readchar();
                        }else
                            return "?";
                    }
                    
                    length = num.length();
                    value=num;
                    
                    // Checks to see if the length of given letters that has N,U,L, if it equals to 4, return null, otherwise, return "?'
                    if(length==4)
                        return "null";
                    else
                        return "?";
                    
                // Checks for A, M and D letters as well as 0 and 1
                }else if( ch == '0' || ch == '1' || ch == '!' || ch == 'A' || ch == 'M' || ch == 'D' || ch == '-' || ch == '+' || ch == '&' || ch == '|' ){
                    while ( ch == '0' || ch == '1' || ch == '!' || isalpha(ch) || ch == '-' || ch == '+' || ch == '&' || ch == '|' ){
                        ch = toupper((char)ch);  
                        if ( ch == '0' || ch == '1' || ch == '!' || ch == 'A' || ch == 'M' || ch == 'D' || ch == '-' || ch == '+' || ch == '&' || ch == '|' ){
                            num += ch;
                            ch = readchar();
                        }
                    }
                    
                    length = num.length();
                    value=num;

                    // This first IF statements does the calculating that if the length is 1 and equals to operators, it should return '?' because there's no token definition
                    // Other IF statements checks to see what token values it should be displayed, corresponding to the length of the value given.
                    // This is done by seeing which of tje compared value matches with it and by matching that, return the corresponding token
                    if (length == 1 && (value[length-1] == '!' || length == 1 && value[length-1] == '-' || length == 1 && value[length-1] == '+' || value[length-1] == '&' || value[length-1] == '|'))
                        return "?";
                    
                    if(length = 1)
                        if ( (!value.compare("0")) || (!value.compare("1")) )
                            return "comp";
                        else if ( (!value.compare("MD")) || (!value.compare("AM")) || (!value.compare("AD")) )
                            return "dest";
                        else if ((!value.compare("A")) || (!value.compare("M")) || (!value.compare("D")))
                            return "dest-comp?";
                        
                    else if(length = 2)
                        if( isdigit(num[length-2]) )
                            return "?";
                        else
                            if ( (value[length-2] == '!' || value[length-2] == '-' || value[length-2] == '+' || value[length-2] == '&' || value[length-2] == '|'))
                                return "comp";
                            
                    else if(length = 3){
                        if (!value.compare("AMD"))
                            return "dest";
                        else if((!value.compare("D+1")) || (!value.compare("A+1")) || (!value.compare("D-1")) || (!value.compare("A-1")) || (!value.compare("D+A")) || (!value.compare("D-A")) || (!value.compare("A-D")) || (!value.compare("D&A")) || (!value.compare("D|A")) || (!value.compare("M+1")) || (!value.compare("M-1")) || (!value.compare("D+M")) || (!value.compare("D-M")) || (!value.compare("M-D")) || (!value.compare("D&M")) || (!value.compare("D|M")))
                            return "comp";}
                    else
                        return "?";

                // Checks for jump
                // This will compare with the values using IF statements to see which one matches, and if it does, return jump 
                }else if ( ch == 'J' || ch == 'L' || ch == 'T' || ch == 'E' || ch == 'M' || ch == 'P' || ch == 'G' || ch == 'Q' || ch == 'N' ){
                    while ( isalpha(ch) ){
                        ch = toupper((char)ch);    
                        if ( ch == 'J' || ch == 'L' || ch == 'T' || ch == 'E' || ch == 'M' || ch == 'P' || ch == 'G' || ch == 'Q' || ch == 'N' ){
                            num += ch;
                            ch = readchar();
                        }
                    }
                    
                    value = num;
                    if( (!num.compare("JMP")) || (!num.compare("JLT")) || (!num.compare("JLE")) || (!num.compare("JGT")) || (!num.compare("JGE")) || (!num.compare("JEQ")) || (!num.compare("JNE")) )
                        return "jump";
                    
                // Checks for address
                }else if( ch == '@' ){
                    while ( ch == '@' || isalpha(ch) || isdigit(ch) || ch == '$' || ch == '_' || ch == ':' || ch == '.'){
                        num += ch;
                        ch = readchar();
                    }
                    
                    x = num;
                    if(x.length() >= 1){
                        for( int k=1; k<x.length(); k++){
                            y+=x[k];
                        }
                        
                    value=y;
                    return "address";
                    }
                
                // Checks for label
                }else if( ch == '(' || ch == ')' ){
                    while( ch == '(' || isalpha(ch) || isdigit(ch) || ch == '$' || ch == '_' || ch == ':' || ch == '.' || ch == ')' ){
                        num += ch;
                        ch = readchar();
                    }    
                    
                    x = num;
                    length = x.length();
                    if(x.length() >= 4){
                        for( int k=1; k<x.length(); k++ ){
                            if(x[k] != ')')
                                y+=x[k];
                            original+=x[k];
                        }
                        
                        // Compare with the original value with the length to see that, if the end of string, contains ')', it will be a label
                        value=y;
                        if( original[length-2] == ')')
                            return "label";
                    }
                }
            }
            
            ch = readchar();
        }
    }
    return "?";
}
