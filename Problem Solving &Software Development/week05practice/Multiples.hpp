// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <bits/stdc++.h>
using namespace std;

class Multiples {
public:
  int number(int min, int max, int factor) {
    int current_number = min;
    int counter = 0;

    while (current_number % factor != 0) {
      current_number++;
    }

    while (current_number <= max) {
      counter++;
      current_number += factor;
    }

    return counter;
  }
};
