// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

class SimpleDuplicateRemover {
public:
  vector<int> process(vector<int> sequence) {

    vector<int> final_vector;

    // Iterates through the whole sequence and if found, and if the value
    // appears only once, add it to the end of the list
    for (int i = 0; i < sequence.size(); i++) {
      bool found = true;
      for (int j = i + 1; j < sequence.size(); j++) {

        // If the value occurs more than once, break
        if (sequence[j] == sequence[i]) {
          found = false;
          break;
        }
      }

      // Adds to the vector for output if value only appears once
      if (found)
        final_vector.push_back(sequence[i]);
    }
    return final_vector;
  }
};