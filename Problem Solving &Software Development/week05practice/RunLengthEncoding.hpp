// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <bits/stdc++.h>
using namespace std;

class RunLengthEncoding {
public:
  // Checks that convert string to int
  int string_to_int(string number) {
    int x;
    stringstream s;
    s << number;
    s >> x;
    return x;
  }

  // Main function to decode string
  string decode(string text) {

    string final_output = "";
    string character_store = "";
    string digits_store = "";

    bool is_characters = false;
    int total_number = 0;
    vector<char> array;

    for (int ii = 0; ii < text.length(); ii++) {
      array.push_back(text[ii]);
    }

    for (int i = 0; i < array.size(); i++) {
      if (isdigit(array[i]) || i == array.size()) {
        if (is_characters) {

          int length = string_to_int(digits_store);
          total_number += length;
          if (total_number > 50)
            return "TOO LONG";

          for (int k = 0; k < length; k++) {
            final_output += character_store[0];
          }

          if (character_store.length() > 1) {
            for (int k = 1; k < character_store.length(); k++) {
              final_output += character_store[k];
            }
          }

          digits_store = "";
          character_store = "";

          is_characters = false;
        }

        digits_store += array[i];
      }

      if (!isdigit(array[i])) {
        character_store += array[i];
        is_characters = true;
      }
    }

    int length = string_to_int(digits_store);
    total_number += length;
    if (total_number > 50)
      return "TOO LONG";

    for (int k = 0; k < length; k++) {
      final_output += character_store[0];
    }

    if (character_store.length() > 1) {
      for (int k = 1; k < character_store.length(); k++) {
        final_output += character_store[k];
      }
    }

    return final_output;
  }
};
