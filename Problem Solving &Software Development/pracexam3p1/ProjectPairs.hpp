#include <bits/stdc++.h>
#include <math.h>
#include <iostream>
#include <vector>

using namespace std;

struct myclass {
  bool operator() (int i,int j) { return (i>j);}
} sorter;

class ProjectPairs {
public:
  int minDiff(vector<int> practice) {

  	int current = practice[0];
  	int temp = 0;

  	for(int i=0; i<practice.size(); i++){
  		if(practice[i]>temp){
  			temp = practice[i];
  		}
  	}


  	if(current == temp){
  		return 0;
  	}

  	vector<int> range;

  	for(int i=0; i<practice.size(); i+=2){
  		range.push_back(practice[i] + practice[i+1]);
  	}

	sort (range.begin(), range.end(), sorter);	



	int lowest = range[0] - range[1];
	if(lowest < 0)
		lowest = range[1] - range[0];


	for(int i=0; i<range.size(); i+=2){
		if(((range[i] - range[i+1]) < lowest) && (range[i] - range[i+1]) > 0){
			lowest = range[i] - range[i+1];
		}
		// cout << range[i] - range[i+1] << endl;
	}

    return lowest;
  }
};