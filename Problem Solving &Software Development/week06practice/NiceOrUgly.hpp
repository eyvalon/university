// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <iostream>
#include <string>

using namespace std;
class NiceOrUgly {
public:
  string describe(string s) {
    int consonants = 0;
    int question = 0;
    int vowels = 0;
    int reset = 0;
    int i = 0;

    string res = "NICE";
    while (i <= s.length()) {
      if ((vowels == 3 || consonants == 5) && question == 0)
        return "UGLY";

      if (vowels + question == 3 || consonants + question == 5)
        res = "42";

      if (question >= 3)
        res = "42";

      if (i == s.length())
        break;

      if (s[i] == 'A' || s[i] == 'E' || s[i] == 'I' || s[i] == 'O' || s[i] == 'U') {
        if (reset < 0) {
          reset = 0;
          question = 0;
        }

        reset++;
        consonants = 0;
        vowels++;

      } else if (s[i] == '?') {
        if (vowels == 2) {
          res = "42";
          vowels = 0;
          question = 0;
          consonants = 1;
          reset = -1;

        } else if (consonants == 4) {
          reset = 1;
          question = 0;
          res = "42";
          consonants = 0;
          vowels = 1;

        } else {
          reset = 0;
          question++;
        }

      } else {
        if (reset > 0) {
          reset = 0;
          question = 0;
        }

        reset--;
        vowels = 0;
        consonants++;

      }
      i++;
    }

    return res;
  }
};