// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <iostream>

using namespace std;
class FracCount {
public:
  int position(int numerator, int denominator) {
    int res = 0;
    while (true) {
      if (checker(numerator, denominator) == 1) {
        res++;
        if (numerator == 1 && denominator == 2)
          break;
      }
      numerator--;
      if (numerator == 0) {
        denominator--;
        numerator = denominator - 1;
      }
    }

    return res;
  }

  int checker(int numerator, int denominator) {
    int c = 0;
    while (numerator != 0) {
      c = numerator;
      numerator = denominator % numerator;
      denominator = c;
    }

    return denominator;
  }
};