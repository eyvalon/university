// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <iostream>
#include <string>

using namespace std;

class BlackAndWhiteSolitaire {
public:
  int minimumTurns(string cardFront) {

    int blackOdd = 0;
    int blackEven = 0;
    int whiteOdd = 0;
    int whiteEven = 0;

    for (int i = 0; i < cardFront.length(); i++) {
      if (cardFront[i] == 'B') {
        if (i % 2 == 0)
          blackOdd++;
        else
          blackEven++;
      }
      if (cardFront[i] == 'W') {
        if (i % 2 == 0)
          whiteOdd++;
        else
          whiteEven++;
      }
    }

    // Checks that B and W doesn't add up to the card length || W is 0 && B is 0
    if ((whiteEven + blackOdd == cardFront.length() ||  whiteOdd + blackEven == cardFront.length()) &&
        blackOdd + blackEven != 0 && whiteOdd + whiteEven != 0)
      return 0;

    if (blackOdd + whiteEven > blackEven + whiteOdd)
      return blackEven + whiteOdd;
    else
      return blackOdd + whiteEven;
  }
};