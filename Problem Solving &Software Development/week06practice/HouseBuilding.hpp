// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <string>
#include <vector>

using namespace std;
class HouseBuilding {

public:
  int getMinimum(vector<string> area) {
    // Make it as high as possible (which in this case, the maximum an int can store)
    int result = 1e+9;

    // Run through an an iteration of 0 - 9 times
    for (int n = 0; n < 9; n++) {
      int temp = 0;
      for (int b = 0; b < area.size(); b++) {
        for (int k = 0; k < area[0].size(); k++) {
          temp += min(abs(area[b][k] - '0' - n), abs(area[b][k] - '0' - (n + 1)));
        }
      }
      result = min(result, temp);
    }

    return result;
  }
};