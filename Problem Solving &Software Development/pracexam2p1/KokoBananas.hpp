#include <bits/stdc++.h>

using namespace std;

class KokoBananas {
public:
  int lastHand(vector<int> hands, int K) {

  	int max = 0;
  	int position = 0;
  	int min = 0;

  	while(true){
	  	vector<int> myvector = hands;	
  		sort (myvector.begin(), myvector.begin()+hands.size()); 

  		if(myvector[0] == myvector[myvector.size()-1])
	    	break;

	  	max = -1;
	  	position = 0;
	  	min = 101;

  		for(int i=0; i<hands.size(); i++){
	    	if(hands[i] > max){
	    		max = hands[i];
	    		position = i;
	    	}

	    	if(hands[i] < min){
	    		min = hands[i];
	    	}
	    }

	    hands[position]--;
	    K--;

	    if(K <= 0)
	    	return position;
  	}

  	while(K!=0){
		for(int b=0; b<hands.size(); b++){
  			K--;
  			hands[b]--;

  			if(K==0)
  				return b;
  		}
  	}

    return 0;
  }
};