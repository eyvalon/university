#include <bits/stdc++.h>
using namespace std;
 
class Truckloads{
  public:

    int res = 0;

  	int numTrucks(int numCrates, int loadSize){
    
    if(loadSize >= numCrates) 
      return 1;

    int left = numCrates/2;
    int right = numCrates - left;

    return numTrucks(left, loadSize) + numTrucks(right, loadSize);
  }
};

  