// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <bits/stdc++.h>

using namespace std;

class SimpleCompressor {
public:
  // Char to String converter
  string convert_to_string(string data, int x) {
    string r;
    stringstream s;
    s << data[x];
    s >> r;
    return r;
  }

  // Checks that convert string to int
  int string_to_int(string number) {
    int x;
    stringstream s;
    s << number;
    s >> x;
    return x;
  }

  // Finds what the end answer is for calculating the [N x]
  string recursive_find(vector<string> data) {
    string r;

    int current_number = 0;
    int i = 0;

    string current_string = "";
    string final_string = "";

    // When there is [N x], it should be able to handle what's inside
    if (data[0] == "[" && data[3] == "]") {
      current_number = string_to_int(data[1]);

      for (int k = 0; k < current_number; k++) {
        final_string += data[2];
      }

      return final_string;
    }

    if (data[i] == "[" && data[i + 3] == "[") {

      current_number = string_to_int(data[i + 1]);

      i += 4;

      vector<string> new_data(data.begin() + i, data.end());
      string some_value = (data[2] + recursive_find(new_data));

      for (int k = 0; k < current_number; k++) {
        final_string += some_value;
      }
      return final_string;
    }

    // Grabs the [x[ ... value where this will split further more to keep
    // recursive happening
    if (data[i] == "[" && data[i + 2] == "[") {
      current_number = string_to_int(data[i + 1]);

      i += 3;

      vector<string> new_data(data.begin() + i, data.end());
      string some_value = recursive_find(new_data);

      for (int k = 0; k < current_number; k++) {
        final_string += some_value;
      }
      return final_string;
    }

    // When [x[ splits, the next part to follow is to grab what's within that
    // area and there may be a smaller breakdown, therefore get what's inside
    // and handle it like how [2[3AB]]
    if (string_to_int(data[i]) && data[i + 1].length() > 0 &&
        data[i + 2] == "]") {
      int num_of_times = string_to_int(data[i]);
      for (int b = 0; b < num_of_times; b++) {
        current_string += data[1];
      }
      r += current_string;
      i += 3;

      if (i == (data.size() - 1)) {
        return r;
      }
    }

    // This part handles the multiple closing bracket as well as checking for
    // opening brackets
    if (data[i] == "[" || data[i] == "]") {
      if (string_to_int(data[i + 1])) {
        vector<string> new_data(data.begin() + i, data.end());
        r += recursive_find(new_data);
      }
    }

    return r;
  }

  string uncompress(string data) {

    // Initialize of variable
    string current_string = "";
    string string_to_add = "";

    // Convert the inputs into vector arrays so that all the letters and numbers
    // are grouped together. This will help us with our ease of implementation
    // when using recursive call
    int i = 0;
    while (i < data.length()) {
      int x_times = 1;

      // Starts a check when '[' is first spotted
      if (data[i] == '[') {

        vector<string> inputs;
        vector<string> final_vector;

        if (current_string.length() > 0) {
          final_vector.push_back(current_string);
          current_string = "";
        }

        final_vector.push_back("");
        int left_times = 1;
        int counter = 0;
        int index = 0;
        inputs.push_back(convert_to_string(data, i));
        index++;
        i++;
        // While the '[' doesn't meet with the last ']', continue to input into
        // inputs vector array
        while (true) {
          if (data[i] == '[') {

            if (string_to_add.length() > 0) {
              inputs.push_back(string_to_add);
              string_to_add = "";
              index++;
            }

            inputs.push_back(convert_to_string(data, i));
            left_times++;
            index++;
          }

          if (data[i] == ']') {

            if (string_to_add.length() > 0) {
              inputs.push_back(string_to_add);
              string_to_add = "";
              index++;
            }

            inputs.push_back(convert_to_string(data, i));
            counter++;
            index++;
            // Break from loop after everything is successfull -> [ ____ ]
            if (left_times == counter)
              break;
          }

          if (isdigit(data[i])) {
            inputs.push_back(convert_to_string(data, i));
            index++;
          }

          if ((data[i] != '[') && (data[i] != ']') && !isdigit(data[i])) {
            string_to_add += data[i];
          }
          i++;
        }

        current_string +=
            (final_vector[0] + recursive_find(inputs) + current_string);

      } else {

        current_string += data[i];
      }
      i++;
    }

    // Return final answer
    return current_string;
  }
};