// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <bits/stdc++.h>

using namespace std;

class Arrows {
public:
  // Initialize Global Variables
  bool left = false;
  bool right = false;

  // Function that helps to check if a string exists within string
  int longestArrow(string s) {

    // Initialize variables
    vector<int> positions;
    int highest = 0;

    // Find all the positions that has > / <
    for (int i = 0; i < s.length(); i++) {
      if (s[i] == '<' || s[i] == '>') {
        positions.push_back(i);
      }
    }

    // Return -1 for error
    if (positions.size() == 0)
      return -1;

    for (int x = 0; x < positions.size(); x++) {

      // Local variables
      int total = 1;
      char current;

      // Right operations only
      if (positions[x] == 0) {
        left = true;
        current = s[positions[x] + 1];
        
        if (current == '=' || current == '-') {
          if (s[positions[x]] == '<' && (current == '-' || current == '='))
            total += Operations(positions[x] + 1, s, current);
        }
      }

      // Left operations only
      else if (positions[x] == s.length() - 1) {
        right = true;
        current = s[positions[x] - 1];

        if (current == '=' || current == '-') {
          if (s[positions[x]] == '>' && (current == '-' || current == '='))
            total += Operations(positions[x] - 1, s, current);
        }
      }

      // Both operations
      else {
        current = s[positions[x] + 1];
        if (current == '=' || current == '-') {
          left = true;

          if (s[positions[x]] == '<')
            total += Operations(positions[x] + 1, s, current);

          left = false;
        }
        highest = check_highest(total, highest);
        total = 1;

        current = s[positions[x] - 1];
        if (current == '=' || current == '-') {
          right = true;
          current = s[positions[x] - 1];

          if (s[positions[x]] == '>')
            total += Operations(positions[x] - 1, s, current);

          right = false;
          highest = check_highest(total, highest);
        }
      }

      // Check for highest and store into
      if (left || right)
        highest = check_highest(total, highest);
      left = false;
      right = false;
    }

    return highest;
  }

  // Function that checks to determine if highest should be replaced
  int check_highest(int x, int y) {
    if (x > y)
      return x;

    return y;
  }

  // Operation that does the incrementing / decrementing
  int Operations(int x, string s, char c) {
    if (left) {
      if (s[x] == c)
        return Operations(x + 1, s, c) + 1;
    }

    if (right) {
      if (s[x] == c)
        return Operations(x - 1, s, c) + 1;
    }
    return 0;
  }
};