// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <iostream>
using namespace std;

class Inchworm {
public:
  int lunchtime(int branch, int rest, int leaf) {

    bool breaknow = false;
    int res = 0;
    int current = 0;
    while (!breaknow) {

      if (current < branch) {
        if (current % leaf == 0)
          res++;
        current += rest;
      }

      if (current >= branch) {
        if (current % leaf == 0 && current == branch)
          res++;
        breaknow = true;
      }
    }
    return res;
  }
};
