// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

class AzimuthMonitoring {
public:
  int getAzimuth(vector<string> instructions) {

    // Initialise variables
    int angle_count = 0;

    for (int i = 0; i < instructions.size(); i++) {

      std::istringstream buf(instructions[i]);
      std::istream_iterator<std::string> beg(buf), end;
      std::vector<std::string> tokens(beg, end);

      // Checkings where size = 1
      if (tokens.size() == 1) {
        if (instructions[i] == "HALT") {
          break;
        }

        if (instructions[i] == "LEFT") {
          angle_count -= 90;
        }

        if (instructions[i] == "RIGHT") {
          angle_count += 90;
        }
      }

      // Checkings where size = 2
      if (tokens.size() == 2) {
        if (instructions[i] == "TURN AROUND") {
          angle_count -= 180;
        }

        if (tokens[0] == "LEFT") {
          angle_count -= stoi(tokens[1]);
        }

        if (tokens[0] == "RIGHT") {
          angle_count += stoi(tokens[1]);
        }
      }
    }

    while (angle_count < 0) {
      angle_count += 360;
    }

    while (angle_count >= 360){
        angle_count -= 360;
    }

    return angle_count;
  }
};
