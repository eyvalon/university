// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <iostream>
#include <vector>
using namespace std;

class EggCartons {
public:
  int minCartons(int n) {

    // Initialise variables
    int num = n;
    int count = 0;

    // Checks if it is divisible by 2
    if (num % 2 != 0)
      return -1;

    // Checks if input value is equal to 8 or 6
    if (n == 8 || n == 6) {
      return 1;
    }

    // While the num > 16, keep on subtracting 8 (to get minimal count)
    while (num > 16) {
      count++;
      num -= 8;
    }

    // Checks for values that is 14 or higher
    if (num > 13) {
      if (num = 16) {
        num -= 16;
        count += 2;
      } else {
        num -= 8;
        count++;
      }
    }

    // Checks for whether it is equal to 10
    if (n != 10 && num == 10) {
      num += 8;
      count--;
    }

    // Final Check that determine if it is possible to keep on subtracting by 6
    while (num > 0) {
      num -= 6;
      count++;

      if (num < 0)
        return -1;
    }

    return count;
  }
};
