// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <bits/stdc++.h>

using namespace std;

class DownloadingFiles {
public:
  double actualTime(vector<string> tasks) {

  // Initialize variables
  bool first = true;
  double total_time = 0.0;
  double speed_store = 0;

  vector<pair<double, double>> each_files;

  // The first loop will be putting the input time and speed into the vector
  // each_files so that it can be sorted in the next loop
  for (int i = 0; i < tasks.size(); i++) {
    istringstream buf(tasks[i]);
    istream_iterator<string> beg(buf), end;
    vector<string> tokens(beg, end);

    // Make a vector pair of the two numbers
    each_files.push_back(make_pair(stoi(tokens[1]), stoi(tokens[0])));
  }

  // Use built in sort to sort the whole array
  sort(each_files.begin(), each_files.end());

  for (int i = 0; i < tasks.size() + 1; i++) {
    speed_store += each_files[i].second;

    if (!first) {
      // Re-calculates the time that it needs to take for new speed.
      each_files[i].first =
          each_files[i].first * (each_files[i].second / speed_store);
    }

    if (first)
      first = false;

    // Loops through the whole list and update each and individuals to the new
    // time
    for (int m = i + 1; m < tasks.size(); m++) {
      each_files[m].first -= each_files[i].first;
    }

    // Add the current time it takes to the total_time for final output
    total_time += each_files[i].first;
  }

  return total_time;
  }
};
