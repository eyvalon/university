// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <iostream>
#include <vector>
using namespace std;

class ThrowTheBall {
public:
  int timesThrown(int N, int M, int L) {

    // Initialise a vector of 0's for N times
    std::vector<int> position_count;
    position_count.insert(position_count.end(), N, 0);

    // Initialise variables
    int res = 0;
    int current_position = 1;
    int counter = 0;
    bool first = true;

    while (counter < M) {
      // Checks if it's first time
      if (first) {
        current_position = N - L + 1;
        position_count[0] = 1;
        counter = position_count[0];
        first = false;
      } else {
        res++;

        // IF checks for passing to the left or to the right
        if (counter % 2 == 0) {
          current_position += L;
        } else {
          current_position -= L;
        }

        // Updates position count and number of passes
        current_position = current_position % N;

        if (current_position < 0)
          current_position += N;

        if (current_position == 0)
          current_position = N;

        position_count[current_position - 1]++;
        counter = position_count[current_position - 1];
      }
    }

    if (res != 0)
      res += 1;
    return res;
  }
};
