
#include <climits>
#include <iostream>
#include <math.h>
#include <string>

using namespace std;

class DifferentStrings {
public:
  int countDiffs(int index, string str1, string str2) {
    int result = 0;

    for (int i = 0; i < str1.length(); i++) {
      if (str1[i] != str2[i + index]) {
        result++;
      }
    }

    return result;
  }

  int minimize(string A, string B) {
    int answer = INT_MAX;
    for (int i = 0; i < (B.length() - A.length() + 1); i++) {
      answer = min(answer, countDiffs(i, A, B));
    }
    return answer;
  }
};