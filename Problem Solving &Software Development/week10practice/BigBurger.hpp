
#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

class BigBurger {
public:
  int maxWait(vector<int> arrival, vector<int> service) {
    int waitTime = 0;
    int currentWait = 0;

    for (int i = 0; i < service.size(); i++) {
      if (currentWait > arrival[i]) {
        waitTime = max(waitTime, currentWait - arrival[i]);
      } else
        currentWait = arrival[i];

      currentWait += service[i];
    }

    return waitTime;
  }
};