#include <bits/stdc++.h>
#include <math.h>
#include <iostream>
#include <vector>

using namespace std;

class RelatedWords {
public:
  int minRemoval(string A, string B) {
    vector<int> A_List;
    vector<int> B_List;

    for (int i=0; i<A.length(); i++){
    	A_List.push_back(A[i]);
    	B_List.push_back(B[i]);
    }

    int temp = A_List[0] + B_List[0];
    int temp1;
    int counter = 0;

	for (int i=0; i<A.length(); i++){
    	temp1 = A_List[i] + B_List[i];
    	if(temp != temp1){
    		counter++;
    	}
    }

	if(temp == temp1){
		return 0;
	}

    return counter;
  }
};