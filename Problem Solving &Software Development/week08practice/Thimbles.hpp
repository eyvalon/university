#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class Thimbles {

public:
  int thimbleWithBall(vector<string> swaps) {
    int currentIndex = 1;

    for (int i = 0; i < swaps.size(); i++) {
      if (currentIndex == (swaps[i][0] - '0')) {
        currentIndex = (swaps[i][2] - '0');
      } else if (currentIndex == (swaps[i][2] - '0')) {
        currentIndex = (swaps[i][0] - '0');
      }
    }

    return currentIndex;
  }
};
