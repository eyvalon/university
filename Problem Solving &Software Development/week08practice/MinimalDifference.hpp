#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class MinimalDifference {
public:
  // Stores the largest number by comparing the three numbers
  int max(int x, int y, int z) {
    int r;
    r = ((x > y) && (x > z) ? x : (y > x) && (y > z) ? y : z);

    return r;
  }

  // Converts the input digit to a sum of digits
  int returnDigitSum(int n) {
    int value = 0;

    while (n > 0) {
      value += n % 10;
      n /= 10;
    }

    return value;
  }

  // Find minimum difference function
  int findNumber(int A, int B, int C) {
    int cTotal = returnDigitSum(C);
    int min = max(A, B, C);
    int value = 0;

    for (int i = A; i <= B; i++) {
      int currentSum = returnDigitSum(i);
      if (min > abs(currentSum - cTotal)) {
        min = abs(currentSum - cTotal);
        value = i;
      }

      if (min == 0)
        break;
    }

    if(A == B){
      return A;
    }

    return value;
  }
};