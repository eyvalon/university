#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class RockStar {
public:
  int getNumSongs(int ff, int fs, int sf, int ss) {
    if (fs > sf)
      return ff + 2 * sf + ss + 1;
    else if (fs == 0) {
      if (ff > 0)
        return ff;
      else
        return ss + min(sf, 1);
    } else
      return ff + 2 * fs + ss;
  }
};
