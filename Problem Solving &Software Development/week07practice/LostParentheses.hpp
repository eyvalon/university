#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

class LostParentheses {

public:
  int minResult(string e) {

    int output = 0;
    int number = 0;
    int isNegative = 0;

    char currentSign;

    stringstream in(e);
    in >> output;

    while (in >> currentSign) {

      in >> number;

      if (currentSign == '-') {
        isNegative = 1;
      }

      if (isNegative) {
        output -= number;
      } else {
        output += number;
      }
    }

    return output;
  }
};