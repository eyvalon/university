#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class LotteryTicket {

public:
  string buy(int price, int b1, int b2, int b3, int b4) {

    vector<int> inputs = {b1, b2, b3, b4};
    sort(inputs.begin(), inputs.end(), greater<int>());

    for (int current = 0; current < inputs.size(); current++) {
      int total = price;

      for (int b = current; b < inputs.size(); b++) {
        if (inputs[b] <= total) {
          total -= inputs[b];
        }

        if (total == 0)
          return "POSSIBLE";
      }
    }

    return "IMPOSSIBLE";
  }
};
