#include <iostream>
#include <string>

using namespace std;
class ExerciseMachine {
public:
  int getChar(char c) { return c - '0'; }

  int convertToSeconds(string t) {
    int hour = getChar(t[7]) + getChar(t[6]) * 10;
    int minutes = 60 * (getChar(t[4]) + 10 * getChar(t[3]));
    int seconds = 3600 * (getChar(t[1]) + 10 * getChar(t[0]));
    int total_time = hour + minutes + seconds;
    return total_time;
  }

  int getPercentages(string time) {
    int secs = convertToSeconds(time);
    int totalTimes = 0;

    for (int i = 1; i < secs; i++) {
      int q = (i * 100) / secs;
      if (q * secs == i * 100)
        totalTimes++;
    }

    return totalTimes;
  }
};