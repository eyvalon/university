#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class TaliluluCoffee {

public:
  int maxTip(vector<int> tips) {
    int totalTip = 0;
    sort(tips.begin(), tips.end(), greater<int>());

    for (int i = 0; i < tips.size(); i++) {
      int temp = tips[i] - i;

      if (temp > 0)
        totalTip += temp;
    }

    return totalTip;
  }
};
