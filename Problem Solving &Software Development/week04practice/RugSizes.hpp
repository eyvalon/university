// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class RugSizes {
public:
  int rugCount(int area) {

    vector<int> all_possible_values;
    int counter = 1;

    if (area < 1) {
      return 1;
    }

    for (int i = 2; i < area / 2; i++) {
      int result = i;
      int temp = i;

      while (temp < area) {
        if ((result * temp) == area || (i * i == area)) {

          if (!(result % 2 == 0 && temp % 2 == 0) || temp == result) {
            all_possible_values.push_back(temp);
            all_possible_values.push_back(result);
          }

          break;
        } else {
          temp++;
        }
      }
    }

    return (all_possible_values.size() / 2) + 1;
  }
};