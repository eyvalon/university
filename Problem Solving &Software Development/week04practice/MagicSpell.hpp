// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class MagicSpell {
public:
  string fixTheSpell(string spell) {
    vector<char> current;
    vector<int> positions;

    string final_output;

    for (int i = 0; i < spell.length(); i++) {
      current.push_back(spell[i]);
      if (spell[i] == 'A' || spell[i] == 'Z') {
        positions.push_back(i);
      }
    }

    int find_length = (positions.size() / 2);

    for (int ii = 0; ii < find_length; ii++) {
      char previous = current[positions[ii]];
      char next = current[positions[positions.size() - 1 - ii]];

      current[positions[ii]] = next;
      current[positions[positions.size() - 1 - ii]] = previous;
    }

    for(int iii=0; iii<spell.length(); iii++){
      final_output += current[iii];
    }

    return final_output;
  }
};