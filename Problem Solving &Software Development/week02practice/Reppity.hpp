// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <bits/stdc++.h>

using namespace std;

class Reppity {
public:
  // Function that helps to check if a string exists within string
  int check(string::size_type n, string const &s) {
    if (n == string::npos) {
      return 0;
    } else {
      return 1;
    }
  }

  int longestRep(string input) {

    // Initialize variables
    string::size_type n;
    string temp = "";

    int temp_highest = 0;
    int highest = 0;
    int counter = 0;
    int k = 0;

    // Add new letters to existing temp string and then do the check function
    while (k < input.length()) {

      temp += input[k];
      n = input.find(temp, k + 1);

      // Resets the string to empty if there isn't any letters matching
      // Does highest checking where approriate and store them for later use
      if (check(n, input) == 0) {
        if (temp_highest > highest)
          highest = temp_highest;

        temp_highest = 0;
        temp = "";

      } else if (check(n, input) == 1) {
        temp_highest += 1;
      }

      k++;
      if (k == input.length() && counter < counter < input.length()) {
        if (counter < input.length())
          k = counter;

        counter++;
      }
    }

    // Checks one more time before the program exits so that highest is still
    // highest
    if (temp_highest > highest)
      highest = temp_highest;

    return highest;
  }
};