// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <bits/stdc++.h>

using namespace std;

class FibonacciDiv2 {
public:
  // Standard Recursive fib function
  int fib(int x) {
    if (x == 0)
      return 0;

    if (x == 1)
      return 1;

    return fib(x - 1) + fib(x - 2);
  }

  int find(int x) {

    // Initialise of variables
    int i = 0;
    int upper_bound = 0;
    int lower_bound = 0;
    int current = 0;

    while (true) {
      current = fib(i);
      if (current >= x) {

        if (current == x)
          return 0;

        // Calculates upper_bound and lower_bound
        lower_bound = x - upper_bound;
        upper_bound = current - x;

        // return the smaller value
        if (lower_bound < upper_bound)
          return lower_bound;
        else
          return upper_bound;
      }
      upper_bound = current;

      i++;
    }

    return 0;
  }
};
