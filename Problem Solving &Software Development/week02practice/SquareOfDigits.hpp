// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <bits/stdc++.h>

using namespace std;

class SquareOfDigits {
public:
  int getMax(vector<string> data) {

    // Initialise variables to be calculated
    int count_x = data[0].size();
    int count_y = data.size();
    int width_size = data[0].size();
    int counter = 0;

    // base case
    if (width_size == 1)
      return width_size;

    // Determine what the maximum sqaure it can be formed
    if (count_x > count_y) {
      counter = count_y;
    } else {
      counter = count_x;
    }

    // Loop that checks for the largest square it can find,
    // If it cannot find, return 1
    while (true) {
      for (int i = 0; i < count_x - counter + 1; i++) {
        for (int j = 0; j < count_y - counter + 1; j++) {
          if (data[j][i] == data[j][i + counter - 1] &&
              data[j][i] == data[j + counter - 1][i] &&
              data[j][i] == data[j + counter - 1][i + counter - 1]) {

            return counter * counter;
          }
        }
      }

      counter--;

      if (counter == 1)
        break;
    }

    return counter * counter;
  }
};