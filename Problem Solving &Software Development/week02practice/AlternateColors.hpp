// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <bits/stdc++.h>

using namespace std;

class AlternateColors {
public:
  string getColor(long r, long g, long b, long k) {

    // Initialise of variables
    vector<pair<long, string>> colours;
    int counter = 3;

    // Initialise of pair values
    colours.push_back(make_pair(r, "RED"));
    colours.push_back(make_pair(g, "GREEN"));
    colours.push_back(make_pair(b, "BLUE"));

    while (true) {

      // Find minimum value
      long min = 0;
      for (int ii = 0; ii < counter; ii++) {
        if (ii == 0) {
          min = colours[ii].first;
        } else {
          if (colours[ii].first < min)
            min = colours[ii].first;
        }
      }

      // JUMPS that delete the smallest large chunk of numbers instead of
      // looping individually
      if (counter * min <= k) {

        int i = 0;
        while (i < counter) {
          colours[i].first -= min;
          k -= min;
          if (colours[i].first == 0) {
            if (k == 0) {
              return colours[i].second;
            }

            colours.erase(colours.begin() + i);
            counter--;
          } else {
            i++;
          }

          if (colours.size() == 1) {
            return colours[0].second;
          }
        }
      } else {

        long temp_k = k;

        // Checks to see if it is a modular of the counter value. If it is,
        // return the position of that value
        if (k % counter == 0) {
          return colours[counter - 1].second;
        }

        // Checks if k is less than 3
        if (k < 3) {
          return colours[k - 1].second;
        }

        // Just a normal iteration that finds and remove numbers for k
        int num = k / counter;
        for (int iii = 0; iii < counter; iii++) {
          colours[iii].first -= num;
          k -= num;

          if (colours[iii].first == 0) {
            if (k == 0) {
              return colours[iii].second;
            }

            colours.erase(colours.begin());
            counter--;
          }

          // Return if there's only one colour left
          if (colours.size() == 1) {
            return colours[0].second;
          }

          if (k == 0) {
            return colours[iii].second;
          }
        }

        if (temp_k == k)
          return colours[k % counter-1].second;
      }
    }
    return "---";
  }
};