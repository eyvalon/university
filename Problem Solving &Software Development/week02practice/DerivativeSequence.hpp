// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <bits/stdc++.h>

using namespace std;

class DerivativeSequence {
public:
  vector<int> derSeq(vector<int> a, int n) {

    // Base test case where a = 0
    if (n < 1) {
      return a;
    }

    // For 'a' times, do the operation where you store a temporary vector and
    // use that to subtract between position [i] and [i-1]
    for (int k = 0; k < n; k++) {
      vector<int> new_vect = a;

      for (int i = 1; i < a.size(); i++) {
        a[i] -= new_vect[i - 1];
      }
      a.erase(a.begin());
    }

    return a;
  }
};