#include <bits/stdc++.h>

using namespace std;

class GameTable {
public:
  int MaxArea(vector<string> board) {

    int total = 0;

    for(int i=0; i<board.size(); i++){
      
      // acounts for array[index] where size is equal to 1
      if(board[i].size() == 1){
        total++;
      }

      if(board.size() == 1){

        int start = -1;
        // total++;
        for(int k=0; k<board[i].size(); k++){

          start = board[i][k];
          int count = 0;

          for(int n=0; n<board[i].size(); n++){
            if(board[i][n] == start){
              count++;
            }else if(board[i][n] != start){
              total++;
              break;
            }
          }

          if(board[i].size() == count){
            return 1;
          }
        }
      }

    }

    return total;
  }
};