#include <bits/stdc++.h>
#include <iostream>

using namespace std;

class JohnGame{
public:
	int temp = 0;
	int temp_0 = 0;
	int answer = 0;
	int sum = 0;
	int sub = 0;

	int tempp = 0;

	int guessNumber(vector<int> guesses, vector<int> answers){

		for( int i =0; i < guesses.size(); i++){
			if(i == 0){
				temp = guesses[i] + answers[i];
				temp_0 = guesses[i] - answers[i];
			}else{

				if(guesses[i] == guesses[0])
					return -1;

				if(i == 1){
					if(guesses[i] + answers[i] == temp){
						answer = temp;
						sum = 1;
					}else if(guesses[i] + answers[i] == temp_0){
						answer = temp_0;
						sum = 1;
					}

					if(guesses[i] - answers[i] == temp){
						answer = temp;
						sub = 1;
					}
					else if(guesses[i] - answers[i] == temp_0){
						answer = temp_0;
						sub = 1;
					}
				}

				if(sum || sub){
					if(guesses[i] + answers[i] != answer)
						if(guesses[i] - answers[i] != answer)
							return -2;
				}
			}

			if(guesses[i] == answers[i]){
				return guesses[i] + answers[i];
			}
		}

		if(guesses.size() == 1){
			if(temp < 1000000000 && temp_0 > 1)
				return -1;

			if(temp > 1000000000 && temp_0 < 1)
				return -2;

			if(temp > 1000000000)
				return temp_0;
		}

		return answer;
	}	
};