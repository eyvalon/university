#include <iostream>
#include <string>
#include <vector>

using namespace std;

class RGBStreet {
public:
  int minimum;
  vector<string> home;

  void sum(int num, int pos, int cost) {
    int rgb[3];

    if (pos == home.size()) {
      minimum = min(minimum, cost);
      return;
    }

    sscanf(home[pos].c_str(), "%d %d %d", &rgb[0], &rgb[1], &rgb[2]);
    for (int i = 0; i < 3; i++) {
      if (i != num) {
        if (cost < minimum)
          sum(i, pos + 1, rgb[i] + cost);
      }
    }
    return;
  }

  int estimateCost(vector<string> houses) {
    minimum = 1000 * 20;
    home = houses;
    sum(4, 0, 0);

    return minimum;
  }
};