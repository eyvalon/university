// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class TimeTravellingCellar {

public:
  int determineProfit(vector<int> profit, vector<int> decay) {
    int result = -100;

    for (int i = 0; i < profit.size(); ++i) {
      for (int j = 0; j < profit.size(); ++j) {
        if (i == j)
          continue;

        result = max(result, profit[i] - decay[j]);
      }
    }

    return result;
  }
};