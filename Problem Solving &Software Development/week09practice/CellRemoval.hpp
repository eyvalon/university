// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class CellRemoval {
public:
  int cellsLeft(vector<int> parent, int deletedCell) {
    vector<vector<int>> child(parent.size());

    int z = -1;
    for (int c = 0; c < parent.size(); c++) {
      int p = parent[c];
      if (p == -1) {
        z = c; // zygote
      } else {
        child[p].push_back(c);
      }
    }

    int result = 0;
    for (int i = 0; i < parent.size(); i++) {
      if (child[i].size() == 0) {
        int temp = i;
        bool check = true;

        while (temp != -1) {
          if (temp == deletedCell) {
            check = false;
            break;
          }

          temp = parent[temp];
        }

        if (check)
          result++;
      }
    }

    return result;
  }
};