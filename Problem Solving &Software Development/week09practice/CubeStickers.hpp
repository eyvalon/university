// Student Name: Pongsakorn Surasarang
// Student ID: a1697114

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class CubeStickers {

public:
  string isPossible(vector<string> sticker) {
    vector<string> colours;
    vector<int> numOfColours;

    for (int i = 0; i < sticker.size(); i++) {
      string temp = sticker[i];
      bool check = true;

      for (int j = 0; j < colours.size(); j++) {
        if (temp == colours[j]) {
          numOfColours[j]++;
          check = false;
          break;
        }
      }
      
      if (check) {
        colours.push_back(temp);
        numOfColours.push_back(1);
      }
    }

    int count = 0;
    for (int i = 0; i < numOfColours.size(); i++) {
      if (numOfColours[i] >= 2)
        count++;
    }

    if (count >= 3 || (count == 1 && numOfColours.size() >= 5) || (count == 2 && numOfColours.size() >= 4) || numOfColours.size() >= 6)
      return "YES";
    else
      return "NO";
  } 
};