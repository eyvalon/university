/* Filename: cp2.c
 * Student Name: Pongsakorn Surasarang
 * Student Number: a1697114
 * Description: Modified CP for question: 2
*/

// Defines the external headers to be used. 
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>             // Strtol
#include <unistd.h>             // Getopt
#include <sys/stat.h>
#include <libgen.h>
#include <memory.h>
#include <stdbool.h>

#define COPYMODE 0644

// Delcaring gloabl variables, to be used for parsing from main to copyFile function.
char *name;
char *base;
char *dir;
char *slash;

int length;
int current_i;
    
bool is_directory = false;
bool is_file = false;
bool is_link = false;
bool is_break = false;

bool interactive = false;

void errExit(char*, char*);
    
main(int argC, char* argV[]){

    struct stat sb;
    int index_start= 1;
    int start;
    int charCnt;
    int buffersize = 500;
    char buf[buffersize];

    /* Check args */
    if( argC<3 ){
        fprintf( stderr, "usage: %s source destination\n", argV[0]);
        exit(1);
    }
        
    int err = stat(argV[argC-1], &sb);
    
    int ii;
    for(ii=1; ii<3; ii++){
        if(strcmp(argV[ii], "-i") == 0){
            index_start++;
            interactive = true;
        }
    }
    
    if(stat(argV[argC-1],&sb) != 0)     /* Is not a directory or file */
        is_file = true;
    else{
        if(S_ISDIR(sb.st_mode))         /* Is a directory */
            is_directory = true;
        else if(S_ISLNK(sb.st_mode))    /* Is a symlink */
            is_link = true;
        else if(S_ISREG(sb.st_mode))    /* Is a regular file */
            is_file = true;
    }
    
    // For every input index, parse it into the copyFile function.
    for(start=index_start; start<argC-1; start++){
        char *p;
        char input_letter[1];
        
        int pass = 1;

        if(interactive == true){
            pass = 0;
            if(pass == 0){
                printf("copy $%s$? (y/n [n]) ", argV[start]);
                fgets(input_letter, 5, stdin);
                //Remove `\n` from the name.
                if ((p=strchr(input_letter, '\n')) != NULL)
                    *p = '\0';
                if(strcmp(input_letter, "y") == 0 || strcmp(input_letter, "Y") == 0)
                    pass = 1;                   // Copy
            }
        }
        if(pass == 1){
            if(is_directory == true){
                // Get's the basename of the sub folder.
                name = argV[argC-1];
                base = basename(name);
                dir = dirname(name);
                slash = "/";
                length = strlen(dir);
                
                char *directory_name;

                if (length == 1){
                    directory_name = (char *) malloc (1 + (strlen(base) + (strlen(slash) + strlen(base))));
                    strcpy(directory_name, base);
                    strcat(directory_name, slash);
                    strcat(directory_name, basename(argV[start]));

                }else {
                    directory_name = (char *) malloc (1 + (strlen(dir) + (strlen(slash)) + strlen(base) + (strlen(slash) + strlen(basename(argV[start])))));
                    strcpy(directory_name, dir);
                    strcat(directory_name, slash);        
                    strcat(directory_name, base);
                    strcat(directory_name, slash);
                    strcat(directory_name, basename(argV[start]));
                }
                
                copyFile(argV[start], directory_name);
            }
            
            if(is_file == true){ 
                
                current_i = argC - start;
                
                copyFile(argV[start], argV[argC-1]);
                
                if(is_break==true){
                    return 1;
                }
                
            }
        }
    }
}

void errExit(char* s1, char* s2){
    fprintf(stderr,"Error: %s ", s1);
    perror(s2);
    exit(1);
}

int copyFile(char* fromFile, char* toPath){

    int srcFd;
    int dstFd;
    int charCnt;    
    

    int buffersize = 500;
    char buf[buffersize];

    if(is_file == true){
        
        if((current_i > 2 && interactive==false) || (current_i > 2 && interactive==true)){
            is_break = true;
            return 0;
        }
        /*Open the files*/
        srcFd= open(fromFile,O_RDONLY);
        if( srcFd==-1 ){
            errExit("Cannot open ", fromFile);
        }
        
        dstFd= creat(toPath,COPYMODE);
        if( dstFd==-1 ){
            errExit( "Cannot create ", toPath);
        }
        /*Copy the data*/
        while( (charCnt= read(srcFd,buf,buffersize)) > 0 ){
            if( write(dstFd,buf,charCnt ) != charCnt ){
                errExit("Write error to ", toPath);
            }
        }
        
        if( charCnt==-1 ){
            errExit("Read error from ", fromFile);
        }
        
        /*Close files*/
        if ( close(srcFd) == -1 || close(dstFd) == -1 ){
            errExit("Error closing files","");
        }
    }

    if(is_directory == true){ 
        
        /*Open the files*/
        srcFd= open(fromFile,O_RDONLY);
        if( srcFd==-1 ){
            errExit("Cannot open ", fromFile);
        }
        
        dstFd= creat(toPath,COPYMODE);
        if( dstFd==-1 ){
            errExit( "Cannot create ", toPath);
        }

        /*Copy the data*/
        while( (charCnt= read(srcFd,buf,buffersize)) > 0 ){
            if( write(dstFd,buf,charCnt ) != charCnt ){
                errExit("Write error to ", toPath);
            }
        }
        
        if( charCnt==-1 ){
            errExit("Read error from ", fromFile);
        }
        
        /*Close files*/
        if ( close(srcFd) == -1 || close(dstFd) == -1 ){
            errExit("Error closing files","");
        }
    }
}
