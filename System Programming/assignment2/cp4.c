/* Filename: cp5.c
 * Student Name: Pongsakorn Surasarang
 * Student Number: a1697114
 * Description: Modified CP for question: 5
*/

// Defines the external headers to be used. 
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>             // Strtol
#include <unistd.h>             // Getopt
#include <libgen.h>
#include <memory.h>
#include <stdbool.h>
#include <dirent.h>

#include <sys/types.h>
#include <sys/stat.h>

#define COPYMODE        0644
#define MAX_CHAR_SIZE   1000

// Delcaring gloabl variables, to be used for parsing from main to copyFile function.
char current_folder[MAX_CHAR_SIZE];
char *name;
char *base;
char *dir;
char *slash;

int length;
int current_i;
int err;

bool is_directory = false;
bool is_file = false;
bool is_link = false;
bool is_break = false;
bool permission = false;

bool interactive = false;
bool recursive = false;
bool debug = false;
bool file_directory = false;

void errExit(char*, char*);
    
main(int argC, char* argV[]){

    struct stat sb, sbb;
    
    int buffersize = 500;
    int index_start= 1;
    int charCnt;   
    int start; 
    int c;
    
    char buf[buffersize];
    
    /* Check args */
    if( argC<3 ){
        fprintf( stderr, "usage: %s source destination\n", argV[0] );
        exit(1);
    }
    
    int ii;
    for( ii=1; ii<3; ii++ ){
        if( strcmp(argV[ii], "-i") == 0 ){
            index_start++;
            interactive = true;
        }
        
        if( strcmp(argV[ii], "-r") == 0 ){
            index_start++;
            recursive = true;
        }

        if( strcmp(argV[ii], "-d") == 0 ){
            index_start++;
            debug = true;
        }
    }
        
    err = stat( argV[argC-1], &sb );
    
    if( stat(argV[argC-1],&sb) != 0 )     /* Is not a directory or file */
        is_file = true;
    else{
        if(S_ISDIR(sb.st_mode))         /* Is a directory */
            is_directory = true;
        if(S_ISLNK(sb.st_mode))    /* Is a symlink */
            is_link = true;
        if(S_ISREG(sb.st_mode))    /* Is a regular file */
            is_file = true;
    }
    
    // For every input index, parse it into the copyFile function.
    for( start=index_start; start<argC-1; start++ ){
        file_directory = false;
        err = stat(argV[start], &sbb);        
        if( S_ISDIR(sbb.st_mode) )         /* Is a directory */
            file_directory = true;
        
        char *p;
        char input_letter[1];
        
        int pass = 1;
        
        if(!(file_directory))
            if(interactive){
                pass = 0;
                if(pass == 0){
                    printf("copy $%s$? (y/n [n]) ", argV[start]);
                    fgets(input_letter, 5, stdin);
                    //Remove `\n` from the name.
                    if ((p=strchr(input_letter, '\n')) != NULL)
                        *p = '\0';
                    if(strcmp(input_letter, "y") == 0 || strcmp(input_letter, "Y") == 0)
                        pass = 1;                   // Copy
                }
            }
            

        if(( file_directory ) && ( !(recursive) ))
            printf("Skipping directory %s\n", argV[start]);
        else{
            if( !(file_directory) ){
                if(pass == 1){
                    if( is_directory ){
                        // Get's the basename of the sub folder.
                        name = argV[argC-1];
                        base = basename(name);
                        dir = dirname(name);
                        slash = "/";
                        length = strlen(dir);
                        
                        char *directory_name;

                        if ( debug ) {
                            printf("DEBUG name: %s\n", argV[start]);
                            printf("DEBUG len: %d\n", length);
                        }

                        if ( length == 1 ){
                            directory_name = (char *) malloc (1 + (strlen(base) + (strlen(slash) + strlen(base))));
                            // strcat(directory_name, base);
                            strcpy(directory_name, base);
                            strcat(directory_name, slash);
                            strcat(directory_name, basename(argV[start]));

                        }else {
                            directory_name = (char *) malloc (1 + (strlen(dir) + (strlen(slash)) + strlen(base) + (strlen(slash) + strlen(basename(argV[start])))));
                            strcpy(directory_name, dir);
                            strcat(directory_name, slash);        
                            strcat(directory_name, base);
                            strcat(directory_name, slash);
                            strcat(directory_name, basename(argV[start]));
                        }
                        
                        if ( debug )
                            printf("DEBUG target_dir: %s\n", directory_name);

                        copyFile(argV[start], directory_name);
                        free(directory_name);
                    }

                    if(is_file){
                        
                        current_i = argC - start;
                        copyFile(argV[start], argV[argC-1]);
                        
                        if(is_break){
                            return 1;
                        }
                    }
                }
            }else if( file_directory ){
                // Get's the basename of the sub folder.
                name = argV[argC-1];
                base = basename(name);
                dir = dirname(name);
                slash = "/";
                length = strlen(dir);
                
                char *directory_name;

                if ( debug ) {
                    printf("DEBUG name: %s\n", argV[start]);
                    printf("DEBUG len: %d\n", length);
                }
                
                if ( length == 1 ){
                    directory_name = (char *) malloc (1 + (strlen(base) + (strlen(slash) + strlen(base))));
                    // strcat(directory_name, base);
                    strcpy(directory_name, base);
                    strcat(directory_name, slash);
                    strcat(directory_name, basename(argV[start]));

                }else {
                    directory_name = (char *) malloc (1 + (strlen(dir) + (strlen(slash)) + strlen(base) + (strlen(slash) + strlen(basename(argV[start])))));
                    strcpy(directory_name, dir);
                    strcat(directory_name, slash);        
                    strcat(directory_name, base);
                    strcat(directory_name, slash);
                    strcat(directory_name, basename(argV[start]));
                }

                // current_folder = argV[start];
                strcpy(current_folder, argV[start]);
                
                if ( debug ) {
                    printf("DEBUG current_dir: %s\n", current_folder);
                    printf("DEBUG target_dir: %s\n", directory_name);
                }
                
                if( recursive ){
                    copyDir(argV[start], directory_name);
                    copyItem(argV[start], directory_name);
                    free(directory_name);
                }
                
                if(permission)
                    return 1;

                if ( debug )
                    printf("DEBUG done\n");
            }
        }
    }
}

void errExit(char* s1, char* s2){
    fprintf(stderr,"Error: %s ", s1);
    perror(s2);
    exit(1);
}

int copyFile(char* fromFile, char* toPath){

    int srcFd;
    int dstFd;
    int charCnt;    
    int buffersize = 500;
    char buf[buffersize];

    if (debug) printf("DEBUG CopyFile from %s to %s\n", fromFile, toPath);

    if( is_file ){        /* Does not allow for more than 1 files to be copied for f1 f2 .. f */
        
        if(( current_i > 2 )){
            is_break = true;
            return 0;
        }
        /*Open the files*/
        srcFd= open(fromFile,O_RDONLY);
        if( srcFd==-1 )
            errExit("Cannot open ", fromFile);
        
        dstFd= creat(toPath,COPYMODE);
        if( dstFd==-1 )
            errExit( "Cannot create ", toPath);

        /*Copy the data*/
        while( (charCnt= read(srcFd,buf,buffersize)) > 0 )
            if( write(dstFd,buf,charCnt ) != charCnt )
                errExit("Write error to ", toPath);
        
        if( charCnt==-1 )
            errExit("Read error from ", fromFile);
        
        /*Close files*/
        if ( close(srcFd) == -1 || close(dstFd) == -1 )
            errExit("Error closing files","");
        
    }

    if( is_directory ){
            
        /*Open the files*/
        srcFd= open(fromFile,O_RDONLY);
        if( srcFd==-1 )
            errExit("Cannot open ", fromFile);
        
        dstFd= creat(toPath,COPYMODE);
        if( dstFd==-1 )
            errExit( "Cannot create ", toPath);

        /*Copy the data*/
        while( (charCnt= read(srcFd,buf,buffersize)) > 0 )
            if( write(dstFd,buf,charCnt ) != charCnt )
                errExit("Write error to ", toPath);
        
        if( charCnt==-1 )
            errExit("Read error from ", fromFile);
        
        /*Close files*/
        if ( close(srcFd) == -1 || close(dstFd) == -1 )
            errExit("Error closing files","");
    }
}

int copyItem(char* fromPath, char* toPath){

    int srcFd;
    int dstFd;
    int charCnt;    
    int buffersize = 500;
    
    char buf[buffersize];
    
    struct dirent *dp;
    struct stat sb;
    
    DIR *dir;
    dir = opendir(fromPath);
    
    if ( debug ) printf("DEBUG dir: %s\n", fromPath);

    while (( dp=readdir(dir) ) != NULL) {
        if ( debug ) printf("\tDEBUG d_name %s\n", dp->d_name);
        
        if ( !strcmp(dp->d_name, "." ) || !strcmp( dp->d_name, ".." )){ } 
        else {
            
            file_directory = false;
            char *file_name = dp->d_name;
            
            if ( debug ) printf("DEBUG current_folder: %s\n", current_folder);

            char* directory_name = (char *) malloc (1 + (strlen(toPath) + (strlen(slash) + (strlen(file_name)))));
            strcpy(directory_name, toPath);
            strcat(directory_name, slash);
            strcat(directory_name, file_name);
            
            if ( debug ) printf("DEBUG toPath: %s\n", toPath);
            if ( debug ) printf("DEBUG directory_name: %s\n", directory_name);
            
            char *current = (char *) malloc (1 + (strlen(current_folder) + (strlen(slash) + (strlen(file_name)))));
            strcpy(current, current_folder);
            strcat(current, slash);
            strcat(current, file_name);
            
            if ( debug ) printf("\tDEBUG current: %s\n", current);

            err = stat(current, &sb);        
            if( S_ISDIR(sb.st_mode) )         /* Is a directory */
                file_directory = true;
            
            if( file_directory ){
                char i[MAX_CHAR_SIZE];
                strcpy(i, current_folder);
                strcpy(current_folder, current);
                
                printf("Copying %s recursively\n", current_folder);
                copyDir(current, directory_name);
                copyItem(current, directory_name);
                strcpy(current_folder, i);
                
                if(permission)
                    break;
                
            }else if(!(file_directory)){
                if ( debug ) printf("--DEBUG: a file\n");
                if ( debug ) printf("Calling copyFile: %s  -->  %s\n", current, directory_name);
                
                copyFile(current, directory_name);
            }

            free( directory_name );
            free( current );
        }
    }
    closedir(dir);
    return 0; 
}

int copyDir(char* fromDir, char* toDir){
    int status;
    status = mkdir( toDir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

    if (status != 0) {
        permission = true;
        return 1;
    }
    
    if ( debug ) printf("DEBUG copyDir status: %d\n", status);
}
