/* Filename: cp2.c
 * Student Name: Pongsakorn Surasarang
 * Student Number: a1697114
 * Description: Modified CP for question: 2
*/

// Defines the external headers to be used. 
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>             // Strtol
#include <unistd.h>             // Getopt
#include <sys/stat.h>
#include <libgen.h>
#include <memory.h>

#define COPYMODE 0644

// Delcaring gloabl variables, to be used for parsing from main to copyFile function.
char *name;
char *base;
char *dir;
char *slash;
int length;
int decision;

void errExit(char*, char*);
    
main(int argC, char* argV[]){

    struct stat sb;
    int index_start= 1;
    int start;
    int charCnt;
    int buffersize = 500;
    char buf[buffersize];

    /* Check args */
    if( argC<3 ){
        fprintf( stderr, "usage: %s source destination\n", argV[0]);
        exit(1);
    }
        
    int err = stat(argV[argC-1], &sb);
    
    if(stat(argV[argC-1],&sb) != 0)     /* Is not a directory or file */
        decision=3;
    else{
        if(S_ISDIR(sb.st_mode))         /* Is a directory */
            decision=1;
        else if(S_ISLNK(sb.st_mode))    /* Is a symlink */
            decision=2;
        else if(S_ISREG(sb.st_mode))    /* Is a regular file */
            decision=3;
    }
    
    if(decision==1){
        // For every input index, parse it into the copyFile function.
        for(start=index_start; start<argC-1; start++){
            char *directory_name;
            // Get's the basename of the sub folder.
            name = argV[argC-1];
            base = basename(name);
            dir = dirname(name);
            slash = "/";
            length = strlen(dir);
            
            if (length == 1){
                directory_name = (char *) malloc (1 + (strlen(base) + (strlen(slash) + strlen(base))));
                strcat(directory_name, base);
                strcat(directory_name, slash);
                strcat(directory_name, basename(argV[start]));
            }else {
                directory_name = (char *) malloc (1 + (strlen(dir) + (strlen(slash)) + strlen(base) + (strlen(slash) + strlen(basename(argV[start])))));
                strcpy(directory_name, dir);
                strcat(directory_name, slash);        
                strcat(directory_name, base);
                strcat(directory_name, slash);
                strcat(directory_name, basename(argV[start]));
            }
            
            copyFile(argV[start], directory_name);
        }
    }
    
    if(decision==3){
        if(argC > 3){
            return 1;
        }
        
        copyFile(argV[1], argV[2]);
    }
}

void errExit(char* s1, char* s2){
    fprintf(stderr,"Error: %s ", s1);
    perror(s2);
    exit(1);
}

int copyFile(char* fromFile, char* toPath){

    int srcFd;
    int dstFd;
    int charCnt;    
    int buffersize = 500;
    char buf[buffersize];

    if(decision==3){
        /*Open the files*/
        srcFd= open(fromFile,O_RDONLY);
        if( srcFd==-1 ){
            errExit("Cannot open ", fromFile);
        }
        
        dstFd= creat(toPath,COPYMODE);
        if( dstFd==-1 ){
            errExit( "Cannot create ", toPath);
        }

        /*Copy the data*/
        while( (charCnt= read(srcFd,buf,buffersize)) > 0 ){
            if( write(dstFd,buf,charCnt ) != charCnt ){
                errExit("Write error to ", toPath);
            }
        }
        
        if( charCnt==-1 ){
            errExit("Read error from ", fromFile);
        }
        
        /*Close files*/
        if ( close(srcFd) == -1 || close(dstFd) == -1 ){
            errExit("Error closing files","");
        }
    }

    if(decision==1){    
        /*Open the files*/
        srcFd= open(fromFile,O_RDONLY);
        if( srcFd==-1 ){
            errExit("Cannot open ", fromFile);
        }
        
        dstFd= creat(toPath,COPYMODE);
        if( dstFd==-1 ){
            errExit( "Cannot create ", toPath);
        }

        /*Copy the data*/
        while( (charCnt= read(srcFd,buf,buffersize)) > 0 ){
            if( write(dstFd,buf,charCnt ) != charCnt ){
                errExit("Write error to ", toPath);
            }
        }
        
        if( charCnt==-1 ){
            errExit("Read error from ", fromFile);
        }
        
        /*Close files*/
        if ( close(srcFd) == -1 || close(dstFd) == -1 ){
            errExit("Error closing files","");
        }
    }
}
