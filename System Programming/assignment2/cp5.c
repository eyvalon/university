/* Filename: cp5.c
 * Student Name: Pongsakorn Surasarang
 * Student Number: a1697114
 * Description: Modified CP for question: 5
*/

// Defines the external headers to be used. 
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>             // Strtol
#include <unistd.h>             // Getopt
#include <libgen.h>
#include <memory.h>
#include <stdbool.h>
#include <dirent.h>

#include <sys/types.h>
#include <sys/stat.h>

#define COPYMODE        0644
#define MAX_CHAR_SIZE   1000

// Delcaring gloabl variables, to be used for parsing from main to copyFile function.
char current_folder[MAX_CHAR_SIZE];
char *slash;

int current_i;

bool target_is_directory = false;
bool target_is_file = false;

bool interactive = false;
bool recursive = false;
bool debug = false;
bool sym_link = false;
bool file_directory = false;

void errExit(char*, char*);
    
int main(int argC, char* argV[]){

    struct stat sb, sbb;
    
    int buffersize = 500;
    int index_start= 1;
    int charCnt;   
    int start; 
    int c;
    int err;
    
    char buf[buffersize];
    
    /* Check args */
    if( argC<3 ){
        fprintf( stderr, "usage: %s source destination\n", argV[0] );
        exit(1);
    }
    
    int ii;
    for( ii=1; ii<3; ii++ ){
        // interactive
        if( strcmp(argV[ii], "-i") == 0 ){
            index_start++;
            interactive = true;
        }
        // recursive
        if( strcmp(argV[ii], "-r") == 0 ){
            index_start++;
            recursive = true;
        }
        // debug (optional)
        if( strcmp(argV[ii], "-d") == 0 ){
            index_start++;
            debug = true;
        }
        // symlink
        if( strcmp(argV[ii], "-s") == 0 ){
            index_start++;
            sym_link = true;
        }
    }
    
    char *cp_target = argV[argC-1];

    err = stat( cp_target, &sb );
    
    // file invalid
    if (err < 0) {
        if (debug) {
            printf("Target destination may not existed!\n");
            printf("Assuming it's a file\n");
        }
        target_is_file = true;
    }


    ////////////////////////////////////////////////////
    // if it gets pass this point then files are good
    ////////////////////////////////////////////////////

    bool pass;
    int length;

    char input_letter[1];
    char *p;
    char *base;
    char *dir;

    if(S_ISDIR(sb.st_mode)){         /* Is a directory */
        target_is_directory = true;
    }else if(S_ISREG(sb.st_mode))    /* Is a regular file */
        target_is_file = true;

    if (debug) {
        if (target_is_directory)
            printf("Target is a directory\n");
        if (target_is_file)
            printf("Target is a file\n");
    }

    // For every input index, parse it into the copyFile function.
    for( start=index_start; start<(argC-1); start++ ){
        file_directory = false;
        err = stat(argV[start], &sbb);        
        if( S_ISDIR(sbb.st_mode) )         /* Is a directory */
            file_directory = true;

        pass = true;

        if(interactive){
            pass = false;
            if(!pass){
                printf("copy $%s$? (y/n [n]) ", argV[start]);
                fgets(input_letter, 5, stdin);
                //Remove `\n` from the name.
                if ((p=strchr(input_letter, '\n')) != NULL)
                    *p = '\0';
                if(strcmp(input_letter, "y") == 0 || strcmp(input_letter, "Y") == 0)
                    pass = true;                   // Copy
            }
        }

        /////////////////
        // copyItem()
        /////////////////

        // regular non-directory file only
        if (!recursive && pass){
            if (file_directory)
                printf("Skipping directory %s\n", argV[start]);
            else {
                if (target_is_file){
                    current_i = argC - start;
                    copyItem(argV[start], cp_target);

                }
                else if (target_is_directory){
                    // Get's the basename of the sub folder.
                    base = basename(cp_target);
                    dir = dirname(cp_target);
                    slash = "/";
                    length = strlen(dir);
                    
                    char *directory_name;

                    if ( debug ) {
                        printf("DEBUG name: %s\n", argV[start]);
                        printf("DEBUG len: %d\n", length);
                    }

                    // cp target is current directory
                    if ( length == 1 ){
                        directory_name = (char *) malloc (1 + (strlen(base) + (strlen(slash) + strlen(base))));
                        strcpy(directory_name, base);
                        strcat(directory_name, slash);
                        strcat(directory_name, basename(argV[start]));

                    }else {
                        directory_name = (char *) malloc (1 + (strlen(dir) + (strlen(slash)) + strlen(base) + (strlen(slash) + strlen(basename(argV[start])))));
                        strcpy(directory_name, dir);
                        strcat(directory_name, slash);        
                        strcat(directory_name, base);
                        strcat(directory_name, slash);
                        strcat(directory_name, basename(argV[start]));
                    }
                    
                    if ( debug )
                        printf("DEBUG target_dir: %s\n", directory_name);

                    copyFile(argV[start], directory_name);
                    free(directory_name);
                }
            }
        }
        // recursive copy
        else if(recursive && pass){
            // Get's the basename of the sub folder.
            base = basename(cp_target);
            dir = dirname(cp_target);
            slash = "/";
            length = strlen(dir);
            
            char *directory_name;

            if ( debug ) {
                printf("DEBUG name: %s\n", argV[start]);
                printf("DEBUG len: %d\n", length);
            }
            
            // cp target is current directory
            if ( length == 1 ){
                directory_name = (char *) malloc (1 + (strlen(base) + (strlen(slash) + strlen(base))));
                strcpy(directory_name, base);
                strcat(directory_name, slash);
                strcat(directory_name, basename(argV[start]));

            }else {
                directory_name = (char *) malloc (1 + (strlen(dir) + (strlen(slash)) + strlen(base) + (strlen(slash) + strlen(basename(argV[start])))));
                strcpy(directory_name, dir);
                strcat(directory_name, slash);        
                strcat(directory_name, base);
                strcat(directory_name, slash);
                strcat(directory_name, basename(argV[start]));
            }

            strcpy(current_folder, argV[start]);
            
            if ( debug ) {
                printf("DEBUG current_dir: %s\n", current_folder);
                printf("DEBUG target_dir: %s\n", directory_name);
            }
            
            copyItem(argV[start], directory_name);
            free(directory_name);

            if ( debug )
                printf("DEBUG done\n");
        }
    }
    return 0;
}

void errExit(char* s1, char* s2){
    fprintf(stderr,"Error: %s ", s1);
    perror(s2);
    exit(1);
}

int copyFile(char* fromFile, char* toPath){

    int srcFd;
    int dstFd;
    int charCnt;    
    int buffersize = 500;
    char buf[buffersize];

    if (debug) printf("DEBUG CopyFile from %s to %s\n", fromFile, toPath);

    if( target_is_file ){        /* Does not allow for more than 1 files to be copied for f1 f2 .. f */
        
        if( current_i > 2 )
            exit(1);
            
        /*Open the files*/
        srcFd= open(fromFile,O_RDONLY);
        if( srcFd==-1 )
            errExit("Cannot open ", fromFile);
        
        dstFd= creat(toPath,COPYMODE);
        if( dstFd==-1 )
            errExit( "Cannot create ", toPath);

        /*Copy the data*/
        while( (charCnt= read(srcFd,buf,buffersize)) > 0 )
            if( write(dstFd,buf,charCnt ) != charCnt )
                errExit("Write error to ", toPath);
        
        if( charCnt==-1 )
            errExit("Read error from ", fromFile);
        
        /*Close files*/
        if ( close(srcFd) == -1 || close(dstFd) == -1 )
            errExit("Error closing files","");
        
    }

    if( target_is_directory ){
            
        /*Open the files*/
        srcFd= open(fromFile,O_RDONLY);
        if( srcFd==-1 )
            errExit("Cannot open ", fromFile);
        
        dstFd= creat(toPath,COPYMODE);
        if( dstFd==-1 )
            errExit( "Cannot create ", toPath);

        /*Copy the data*/
        while( (charCnt= read(srcFd,buf,buffersize)) > 0 )
            if( write(dstFd,buf,charCnt ) != charCnt )
                errExit("Write error to ", toPath);
        
        if( charCnt==-1 )
            errExit("Read error from ", fromFile);
        
        /*Close files*/
        if ( close(srcFd) == -1 || close(dstFd) == -1 )
            errExit("Error closing files","");
    }
}

int copyDir(char* fromDir, char* toDir){

    int srcFd;
    int dstFd;
    int charCnt;    
    int buffersize = 500;
    char buf[buffersize];
    
    struct dirent *dp;
    struct stat sb;
    
    DIR *dir;
    dir = opendir(fromDir);

    if ( debug ) printf("DEBUG dir: %s\n", fromDir);

    while (( dp=readdir(dir) ) != NULL) {
        if ( debug ) printf("\tDEBUG d_name %s\n", dp->d_name);
        
        if ( !strcmp(dp->d_name, "." ) || !strcmp( dp->d_name, ".." )){ } 
        else {
            
            file_directory = false;
            char *file_name = dp->d_name;
            
            if ( debug ) printf("DEBUG current_folder: %s\n", current_folder);

            char* directory_name = (char *) malloc (1 + (strlen(toDir) + (strlen(slash) + (strlen(file_name)))));
            strcpy(directory_name, toDir);
            strcat(directory_name, slash);
            strcat(directory_name, file_name);
            
            if ( debug ) printf("DEBUG toDir: %s\n", toDir);
            if ( debug ) printf("DEBUG directory_name: %s\n", directory_name);
            
            char *current = (char *) malloc (1 + (strlen(current_folder) + (strlen(slash) + (strlen(file_name)))));
            strcpy(current, current_folder);
            strcat(current, slash);
            strcat(current, file_name);
            
            if ( debug ) printf("\tDEBUG current: %s\n", current);

            int err = stat(current, &sb);        
            if( S_ISDIR(sb.st_mode) )         /* Is a directory */
                file_directory = true;
            
            if( file_directory ){
                char i[MAX_CHAR_SIZE];
                strcpy(i, current_folder);
                strcpy(current_folder, current);
                
                printf("Copying %s recursively\n", current_folder);
                copyItem(current, directory_name);
                strcpy(current_folder, i);
                
            }else if(!(file_directory)){
                if ( debug ) printf("--DEBUG: a file\n");
                if ( debug ) printf("Calling copyFile: %s  -->  %s\n", current, directory_name);
                
                copyFile(current, directory_name);
            }

            free( directory_name );
            free( current );
        }
    }
    closedir(dir);
    return 0; 
}

int copyItem(char* fromPath, char* toPath){
    
    struct stat sb, sbb;
    int err = stat( fromPath, &sb );
    int status=0;
    int link_status=0;
    int errr = stat( toPath, &sbb );
    
    if (sym_link) {
        if (debug) printf("DEBUG: Reading symlink ...\n");
        if(S_ISDIR(sbb.st_mode))
            exit(1);
        
        link_status = copyLink(fromPath, toPath);
        
        if (link_status < 0){
            printf("Cannot create symbolic link\n");
            exit(1);
        }
    }else{
        
        if(S_ISREG(sb.st_mode))
            copyFile(fromPath, toPath);
        
        if(S_ISDIR(sb.st_mode)){
            status = mkdir( toPath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

            if (status != 0) {
                exit (1);
            }else
                copyDir(fromPath, toPath);
        }
    }
    
    if ( debug ) printf("DEBUG copyDir status: %d\n", status);    
}

int copyLink (char* fromDir, char* toDir){
    int srcFd;
    int dstFd;
    int charCnt;    
    int buffersize = 500;
    char buf[buffersize];
    int status=0;

    if (0 == access(toDir, 0))
        status = -1;
    else
        status = symlink(fromDir, toDir);

    if ( debug ) printf("DEBUG copyDir status: %d\n", status);    
    
    return status;
}
