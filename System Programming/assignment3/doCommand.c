/* Filename: modifiedTime.c
 * Student Name: Pongsakorn Surasarang
 * Student Number: a1697114
 * Description: Checking to see if files need to be regenerated
*/

// Defines the external headers to be used. 

#include "modifiedTime.h"
#include <string.h>

#define MAX_CHAR_SIZE   1000

bool doCommand(char* current_line){

    char* arg[MAX_CHAR_SIZE];
    char* s = strtok(current_line, "\t");    
    int countt = 0;
    
    bool isgcc = false;
    bool isc = false;
    char* copy;
    copy = (char *) malloc (1 + strlen(current_line));
    strcpy(copy, s);
   
    char* first_token = strtok(copy, " ");
    
    if(strcmp(first_token, "gcc") == 0)
        isgcc = true;
   
    int current_count = 0;
    
    pid_t parent = getpid();
    pid_t pid;
    
   /* walk through other tokens */
    while(first_token ){
        
        if(strcmp(first_token, "-o") == 0){
            current_count = countt+1;
        }
        
        if(isgcc && strcmp(first_token, "-c") != 0 && (strcmp(first_token, "-o") != 0) && strcmp(first_token, "gcc") != 0){
            
        if(countt > current_count){

            char const* const fileName = first_token; /* should check that argc > 1 */
            
            FILE* fp = fopen(fileName,"a+");
            fprintf(fp, "%s", " ");
            fclose(fp);
        }
        }

        
        arg[countt] = s;
        first_token = strtok(NULL, " \n");
        countt++;
    }
    
    pid = fork();
    
    if(pid < 0) {
        printf("Error");
        exit(1);
    } else if (pid == 0) {
        if(isV)
            printf("%s", arg[0]);
        system(arg[0]);
        exit(0); 
    } else  {
        wait(NULL);
    }

    return true;
    
//     printf("%s", arg[0]);
    
//     printf("\n");
    
//     printf("%s", arg);
//         execvp(arg[0],arg);
    
//     char* file_name;
//     file_name = (char *) malloc (1 + strlen(current_line));
//     strcpy(file_name, arg[0]);
// //     
//     int k;
//     for(k=1; k<countt-1; k++){
//         
// //         printf("%s\n", arg[k]);
//         
// //         strcat(file_name, arg[k]);
// //         if(k!=countt-2)
// //             strcat(file_name, " ");
//         
// 
//         
//        
// 
//         
//         
//     }
    
//     printf("%d\n", isgcc);

    
//     system(file_name);
//     system(arg[0]);
//     printf("%s", file_name);

//     printf("%s\n", arg[countt-1]);

//     arg[countt] = "";
// 
//     pid_t parent = getpid();
//     pid_t pid = fork();

//     if (pid == -1){
//         // error, failed to fork()
//     }else if (pid > 0){
//         int status;
//         waitpid(pid, &status, 0);
//     }else {
//         system(current_line);
//         exit(0);
//     }
//     
//     execvp(arg[0],arg);

//     system(arg[0]);
    
//     printf("%s", arg[0]);
}
