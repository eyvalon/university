/* Filename: modifiedTime.c
 * Student Name: Pongsakorn Surasarang
 * Student Number: a1697114
 * Description: Checking to see if files need to be regenerated
*/

// Defines the external headers to be used. 

#include "modifiedTime.h"
#include <string.h>


bool hasChanged(char* current_line){    
       
    char* s = strtok(current_line, " :");
   
//     printf("%s", s);
    
    time_t file_original;
    time_t file_new;
    
    int counter = 0;
    
    
    if (0 != access(current_line, 0))
        return true;
    
    /* walk through other tokens */    
    while( s ){
        if(counter == 0){
            file_original=get_mtime(s);
//             if (0 != access(s, 0))
//                 return true;
            counter++;
            continue;
        }else if(counter != 0){
            if(strstr((&s[strlen(s)-2]), "c") || strstr((&s[strlen(s)-2]), "h") || strstr((&s[strlen(s)-2]), "o")){
                file_new=get_mtime(s);
//                 if (file_original != file_new)
//                     return true;
                
                s = strtok(NULL, " \t\n");
                if(!s)
                    break;
                else if(strstr((&s[strlen(s)-2]), "c") || strstr((&s[strlen(s)-2]), "h") || strstr((&s[strlen(s)-2]), "o")){
                    continue;
                }
            }
        }
        s = strtok(NULL, " \t\n");
    }
    
   return false;
}
