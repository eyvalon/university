/* Filename: modifiedTime.h
 * Student Name: Pongsakorn Surasarang
 * Student Number: a1697114
 * Description: Getting timestamp
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>

#include <unistd.h>

#include <stdbool.h>

#ifndef _TIME_H_
#define _TIME_H_

time_t get_mtime(const char *path);
bool hasChanged(char* current_line);
bool doCommand(char* current_line);

extern bool isV;

#endif
