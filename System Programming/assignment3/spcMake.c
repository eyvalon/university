/* Filename: modifiedTime.c
 * Student Name: Pongsakorn Surasarang
 * Student Number: a1697114
 * Description: Getting timestamp
*/

// Defines the external headers to be used. 

#include "modifiedTime.h"
#include <string.h>

bool isV = false;

int main(int argc, char* argv[]){
    
    bool regenerate = false;
    bool return_output = false;
    
    if(argv[1] == "-v")
        isV = true;
    
    char* first = argv[1];
    char* compare_first = "-v";
    
    if(first != NULL)
        if(strcmp(first, compare_first) == 0)
            isV = true;
        else
            exit(1);
        
//     if(argv[2] != NULL)
//         exit(1);
    
    char const* const fileName = "makefile"; /* should check that argc > 1 */
    FILE* file = fopen(fileName, "r"); /* should check the result */
    char line[256];
    int count=0;
    
    pid_t parent = getpid();
    pid_t pid = fork();
    
    while (fgets(line, sizeof(line), file)) {
//         if (pid == 0){
//             exit(0);
//         }else{
            // Checks for new line and if it does equal to a new line, skips that line
            if(strlen(line)==1){
                count = 0;
                continue;
            }else{
                // Parse the arguements that has "echo" as well as "gcc"
                if(count==0){
                    regenerate = hasChanged(line);
                    
//                     printf("%d", regenerate);
                    
                }else{
                    if(count>=1 && regenerate){
                        return_output = doCommand(line);
                    }
                }
                count++;
//             }
        }
    }
    fclose(file);
    
    return return_output;
}
