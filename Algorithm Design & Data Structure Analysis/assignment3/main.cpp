// Student Name: Pongsakorn Surasarang
// Student Number: a1697114
// Description: Linear Programming

#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <tuple>
#include <vector>

// To simplify the code
using namespace std;

bool debug = false;

#define TABLE_SIZE 26
struct Node {
  int data;
  char x;
  string name;
  string status;

  Node *next;
};

// Initialially initialise 26 blank slots of never used
Node *add(Node *head, int val) {
  Node *newNode = new Node;

  newNode->data = TABLE_SIZE - val;
  newNode->x = 'z' - val;
  newNode->next = head;
  newNode->status = "never used";

  return newNode;
}

// Prints all the value from the node
void print_all(Node *head) {
  Node *current = head;

  for (int ii = 0; ii < 26; ii++) {

    if (debug == true) {
      cout << current->data << " " << current->x << " " << current->name << " "
           << current->status << endl;
    } else if (debug == false) {
      if (current->status == "occupied")
        cout << current->name << " ";
    }

    current = current->next;
  }
  cout << endl;
}

// Determine at which position it should place the word inside the Node Linked
// List of a hash table
void insert_new(Node *head, string g) {
  Node *current = head;
  bool isInsert = true;

  for (int k = 0; k < TABLE_SIZE; k++) {
    if (current->name == g) {
      if (current->status == "tombstone") {
        current->status = "occupied";
      }
      return;
    }
    current = current->next;
  }

  current = head;
  for (int i = 0; i < TABLE_SIZE; i++) {

    // If the last digit equals to the hash table letter, do the operation of
    // either adding, or running a while loop to check where it should add
    if (current->x == g.back()) {
      if (current->x == 'z') {
        if (current->status == "occupied") {
          current = head;
          while (current->status != "never used")
            current = current->next;
          current->status = "occupied";
          current->name = g;
          break;
        } else if (current->x == g.back()) {

          current->status = "occupied";
          current->x = g.back();
          current->name = g;
          break;
        }
      }

      if (current->status == "occupied") {
        if (current->name == g)
          break;

        current = current->next;

        // Resets current node to be the head node if the last letter ('z') is
        // occupied and then iterate to find where to put the value to
        while (current->status != "never used") {
          if (current->x == 'z' && current->status == "occupied") {
            current = head;
          } else {
            current = current->next;
          }
        }
        current->status = "occupied";
        current->name = g;

        break;

        // Normal adding to the node list for hash table
      } else if (current->x == g.back()) {

        current->status = "occupied";
        current->x = g.back();
        current->name = g;
        break;
      }
    }
    current = current->next;
  }
}

void insert_remove(Node *head, char a, string g) {
  Node *current = head;

  for (int i = 0; i < TABLE_SIZE; i++) {
    if (current->name == g) {
      current->status = "tombstone";
      break;
    }

    current = current->next;
  }
}

// Function to delete certain node in the list.
void deleteNode(Node *current_node) {
  Node *temp = current_node->next;
  current_node->data = temp->data;
  current_node->next = temp->next;
  delete temp;
}

int main(int argC, char *argV[]) {

  Node *head = NULL;
  for (int i = 0; i < TABLE_SIZE; i++) {
    head = add(head, i);
  }

  // Declaration of inputs storing
  string buf;
  vector<string> stringInputs;

  // Reads from a file and determine whether it the current word starts with A /
  // D, and then doing the operation of adding or deleting respectively
  while (getline(cin, buf)) {

    istringstream ssin(buf);
    string current_line;
    vector<string> current_line1;

    while (ssin >> current_line) {
      current_line1.push_back(current_line);
      if (current_line[0] == 'A' || current_line[0] == 'D') {
        if (current_line[0] == 'A') {
          current_line1[0].erase(0, 1);
          insert_new(head, current_line1[0]);
        } else if (current_line[0] == 'D') {
          current_line1[0].erase(0, 1);
          insert_remove(head, current_line.back(), current_line1[0]);
        }
      }
      current_line1.erase(current_line1.begin());
    }

    current_line.erase(0, 1);
  }
  print_all(head);

  // free all nodes
  Node *tmp;
  while (head) {
    tmp = head;
    head = head->next;
    delete tmp;
  }

  return 0;
}
