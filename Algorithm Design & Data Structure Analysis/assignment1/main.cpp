// Student Name: Pongsakorn Surasarang
// Student ID  : a1697114

#include <bits/stdc++.h>
#include <iostream>

using namespace std;

/* DESCRIPTION
When a string size match each other, we add a '0' to the beginning of the
string: i.e. 2987 ==> 2987 19   ==> 0019

From this, we can then start calculating for addition, subtraction and
multiplication; using karatsuba
*/

// Obtaining the max length of the two strings
size_t max_length(string left, string right) {
  size_t length;

  if (left.size() > right.size()) {
    length = left.size();
  } else {
    length = right.size();
  }

  return length;
}

// Dynamic allocation empty slot with 0
string fill_zero(string s, size_t length) {
  while (s.size() < length) {
    s.insert(0, "0");
  }

  return s;
}

// School Method - Addition function that is used for adding 2 numbers
string schoolMethodAddition(string left, string right, int B) {

  // Declaration of local variables inside School Method - Additions
  int carry = 0;
  string sum = "";

  // Fill left and right so that they have the same length size
  size_t length = max_length(left, right);
  left = fill_zero(left, length);
  right = fill_zero(right, length);

  // Calculates sum
  int i = length - 1;
  while (i >= 0) {
    int result = (left[i] - '0') + (right[i] - '0');
    int temp = result + carry;

    // If the sum > B, carry == 1 otherwise 0
    // Add resulting sum to the string output
    if (temp >= B) {
      temp = temp - B;
      carry = 1;
    } else {
      carry = 0;
    }

    sum.insert(0, to_string(temp));
    i--;
  }

  // Add if there is carry
  if (carry) {
    sum.insert(0, to_string(carry));
  }

  return sum;
}

// School Method - Subtraction function that is used for subtracting 2 numbers
string schoolMethodSubtract(string left, string right, int B) {

  // Declaration of variables
  string subtraction = "";

  // Fill left and right so that they have the same length size
  size_t length = max_length(left, right);
  left = fill_zero(left, length);
  right = fill_zero(right, length);

  // Calculates subtraction
  int i = length - 1;
  while (i >= 0) {
    int difference = (left[i] - '0') - (right[i] - '0');

    // Using modular to subtract, get new values and adding to string output
    if (difference < 0) {
      for (int j = i - 1; j >= 0; j--) {
        left[j] = ((left[j] - '0') - 1) % B + '0';

        if (left[j] != ((B - 1) + '0')) {
          break;
        }
      }

      int new_value = difference + B;
      subtraction.insert(0, to_string(new_value));

    } else {
      subtraction.insert(0, to_string(difference));
    }

    i--;
  }

  return subtraction;
}

// Karatsuba Method for Multiplication
string karatsubaMultiply(string left, string right, int B) {

  // Fill left and right so that they have the same length size
  size_t length = max_length(left, right);
  left = fill_zero(left, length);
  right = fill_zero(right, length);

  // Declaration of variables
  unsigned x = 0;
  int new_length = length / 2;

  /*  base case for the recursion (if the length is 1) // */
  if (length == 1) {
    int current_result = (left[0] - '0') * (right[0] - '0');
    if (current_result >= B) {
      string new_answer =
          to_string(current_result / B) + to_string(current_result % B);
      return new_answer;
    } else {
      return to_string(current_result);
    }
  }

  // Split into 2 halves for a
  string a0 = left.substr(0, new_length);
  string a1 = left.substr(new_length, length - new_length);

  // Split into 2 halves for b
  string b0 = right.substr(0, new_length);
  string b1 = right.substr(new_length, length - new_length);

  // Recursive calls
  string p0 = karatsubaMultiply(a0, b0, B); // p0 = a0 * b0
  string p1 = karatsubaMultiply(a1, b1, B); // p1 = a1 * b1
  string p2 = karatsubaMultiply(schoolMethodAddition(a0, a1, B),
                                schoolMethodAddition(b0, b1, B),
                                B); // p2 = (a1 + a0) * (b1 + b0)

  // p3 = p2 - (p0 + p1)
  string p_subtract = schoolMethodAddition(p0, p1, B);
  string p3 = schoolMethodSubtract(p2, p_subtract, B);

  // Restore positions for p0 (B^2k step)
  for (size_t i = 0; i < 2 * (length - new_length); i++) {
    p0.push_back('0');
  }

  // Restore positions for p3 (B^k step)
  for (size_t i = 0; i < (length - new_length); i++) {
    p3.push_back('0');
  }

  // result = (p0 + p1) + p3 ---> p3*B^2k + p1*B^2k + p0
  string p_final_0 = schoolMethodAddition(p0, p1, B);
  string result = schoolMethodAddition(p_final_0, p3, B);

  while (true) {
    if (result[x] == '0' && x != result.size() - 1) {
      x++;
    } else {
      result.erase(0, x);
      break;
    }
  }

  return result;
}

// Get input numbers argv
int main() {

  srand(time(NULL));

  // Initialising variables for inputs.
  string I1, I2;
  int B;

  // cin the users input
  cin >> I1 >> I2 >> B;

  // Put the input string into addition and multiplication function
  string additionResult = schoolMethodAddition(I1, I2, B);
  string multiplayResult = karatsubaMultiply(I1, I2, B);

  // Returning answer to websubmission
  cout << additionResult << " " << multiplayResult << " "
       << "0" << endl;

  return 0;
}