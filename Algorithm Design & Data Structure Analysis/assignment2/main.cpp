#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>

using namespace std;

// AVL tree struct
struct avlTree {
  int data;
  avlTree *left;
  avlTree *right;
};

// Checks that convert string to int
int string_to_int(string number) {
  int inputs;
  stringstream str;
  str << number;
  str >> inputs;
  return inputs;
}

// Initialise global variables
avlTree *root = NULL;
int minimum = 0;

// Function to get the height of the tree
int treeHeight(avlTree *root) {
  int height = 0;

  if (root != NULL) {
    int leftHeight = treeHeight(root->left);
    int rightHeight = treeHeight(root->right);
    int maxHeight = max(leftHeight, rightHeight);
    height = maxHeight + 1;
    return height;
  }

  return height;
}

// Finds the height of left and right then subtract it
int heightDiff(avlTree *root) {
  int leftHeight = treeHeight(root->left);
  int rightHeight = treeHeight(root->right);

  int diffence = leftHeight - rightHeight;
  return diffence;
}

// Right Case Rotation
avlTree *rotateRight(avlTree *root) {
  avlTree *temp = root->right;
  root->right = temp->left;
  temp->left = root;
  return temp;
}

// Left Case Rotation
avlTree *rotateLeft(avlTree *root) {
  avlTree *temp = root->left;
  root->left = temp->right;
  temp->right = root;
  return temp;
}

// Function to balance the AVL tree
avlTree *balanceTree(avlTree *root) {

  int diffence = heightDiff(root);
  if (diffence > 1) {
    if (heightDiff(root->left) >= 0) {
      root = rotateLeft(root);
    } else {
      avlTree *temp = root->left;
      root->left = rotateRight(temp);
      return rotateLeft(root);
    }
  } else if (diffence < -1) {
    if (heightDiff(root->right) > 0) {
      avlTree *temp;
      temp = root->right;
      root->right = rotateLeft(temp);
      return rotateRight(root);
    } else {
      root = rotateRight(root);
    }
  }

  return root;
}

// Function used to insert a Node into the AVL tree
avlTree *insertNode(avlTree *root, int value) {

  if (root == NULL) {
    root = new avlTree;
    root->data = value;
    root->left = NULL;
    root->right = NULL;
    return root;

    // If input value is less than root, insert left
  } else if (value < root->data) {
    root->left = insertNode(root->left, value);

    // If input value is greater than root, insert right
  } else if (value > root->data) {
    root->right = insertNode(root->right, value);

    // If doesn't equal to any of the above
  } else {
    return root;
  }

  root = balanceTree(root);
  return root;
}

// Function used to delete a Node from the AVL tree
avlTree *deleteNode(avlTree *root, int value) {

  if (root == NULL)
    return root;

  // Search left
  if (value < root->data)
    root->left = deleteNode(root->left, value);

  // Search right
  else if (value > root->data)
    root->right = deleteNode(root->right, value);

  // If doesn't equal to any of the above
  else {
    if ((root->left == NULL) && (root->right == NULL)) {
      root = NULL;
      return root;
    } else if (root->left == NULL) {
      *root = *(root->right);
    } else if (root->right == NULL) {
      *root = *(root->left);
    } else {
      avlTree *store = root->left;
      while (store->right != NULL) {
        store = store->right;
      }
      root->data = store->data;
      root->left = deleteNode(root->left, store->data);
    }
  }

  root = balanceTree(root);
  return root;
}

void minValue(avlTree *tree) {
  avlTree *current = tree;
  while (current->left != NULL) {
    current = current->left;
  }
  minimum = current->data;
}

// sorts out PRE-order or POST-order or IN-order
void orderType(string type, avlTree *tree) {
  if (tree != NULL) {
    if (type == "PRE") {
      cout << " " << tree->data;
    }

    if (type == "POST") {
      orderType("POST", tree->left);
      orderType("POST", tree->right);
    } else if (type == "PRE") {
      orderType("PRE", tree->left);
      orderType("PRE", tree->right);
    } else if (type == "IN") {
      orderType("IN", tree->left);
      if (tree->data == minimum) {
        cout << tree->data;
      } else {
        cout << " " << tree->data;
      }
      orderType("IN", tree->right);
    }

    if (type == "POST") {
      cout << tree->data << " ";
    }
  }
}

// prints PRE-order or POST-order or IN-order
void printOrder(string type, avlTree *tree) {
  if (tree != NULL) {
    if (type == "PRE") {
      cout << tree->data;
    }

    if (type == "POST") {
      orderType("POST", tree->left);
      orderType("POST", tree->right);

    } else if (type == "PRE") {
      orderType("PRE", tree->left);
      orderType("PRE", tree->right);

    } else if (type == "IN") {
      orderType("IN", tree->left);
      if (tree->data == minimum) {
        cout << tree->data;
      } else {
        cout << " " << tree->data;
      }
      orderType("IN", tree->right);
    }

    if (type == "POST") {
      cout << tree->data;
    }
  }
}

// Main Function
int main(int argc, char **argv) {

  // Defines what the input tag is
  bool isIN = false;
  bool isPRE = false;
  bool isPOST = false;

  // Declaration of inputs storing
  string buf;
  vector<string> stringInputs;

  while (getline(cin, buf)) {
    istringstream ssin(buf);
    string input;
    while (ssin >> input) {
      stringInputs.push_back(input);
    }

    if (stringInputs[stringInputs.size() - 1] == "PRE" ||
        stringInputs[stringInputs.size() - 1] == "POST" ||
        stringInputs[stringInputs.size() - 1] == "IN")
      break;
  }

  // Returns ERROR where input is NULL or equal to 0
  if (stringInputs.size() == 0) {
    cout << "ERROR" << endl;
    return 0;
  }

  // Check printing method and flag it
  if (stringInputs[stringInputs.size() - 1] == "POST") {
    isPOST = true;
  } else if (stringInputs[stringInputs.size() - 1] == "PRE") {
    isPRE = true;
  } else if (stringInputs[stringInputs.size() - 1] == "IN") {
    isIN = true;
  }

  // Checks whether to delete or insert.
  // When trying to insert or delete, delete the first index (A or D) and add to
  // the AVL tree.
  // For these cases, it checks to see if it can find the.
  // corresponding values If not, it will not do anything.
  for (int i = 0; i < stringInputs.size() - 1; i++) {
    if (stringInputs[i][0] == 'A') {
      stringInputs[i].erase(0, 1);
      int num = string_to_int(stringInputs[i]);
      root = insertNode(root, num);
    } else if (stringInputs[i][0] == 'D') {
      stringInputs[i].erase(0, 1);
      int num = string_to_int(stringInputs[i]);
      root = deleteNode(root, num);
    }
  }

  // Is the AVL tree empty?
  if (root == NULL) {
    cout << "EMPTY" << endl;
    return 0;
  }

  // Depending on the last word, print that type
  if (isPOST) {
    printOrder("POST", root);
  } else if (isPRE) {
    printOrder("PRE", root);
  } else if (isIN) {
    minValue(root);
    printOrder("IN", root);
  }
  cout << endl;

  return 0;
}